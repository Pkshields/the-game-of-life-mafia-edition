# The Game of Life: Mafia Edition
 
The Game of Life: Mafia Edition is an XNA game that takes the rules of The Game of Life(tm) and flips them on their head. Capture the Flag!

The Game of Life: Mafia Edition is licensed under the New BSD License.