﻿#region FILE DESCRIPTION
//-----------------------------------------------------------------------------
// MuScreenManager
// 
// MenuScreen.cs
//-----------------------------------------------------------------------------
#endregion

#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion USING

namespace MuScreenManager
{
    /// <summary>
    /// Provides an API for generating a Menu Screen using the Screen from MuScreenManager
    /// </summary>
    public abstract class MenuScreen : Screen
    {
                                                #region PROPERTIES

        /// <summary>
        /// List of selectable menu items, selectible by mouse or gamepad
        /// </summary>
        protected List<MenuObject> menuItem = new List<MenuObject>();

        /// <summary>
        /// Current selected menu item
        /// </summary>
        protected int currentItem = 0;

        /// <summary>
        /// Font used by this screen - can be ScreenManager font or custom
        /// </summary>
        protected SpriteFont font;

        /// <summary>
        /// Header for this title on menu screen
        /// </summary>
        protected DrawableString title;

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize the MenuScreen components of this object - No built in header, no custom font
        /// </summary>
        public MenuScreen()
        { }

        /// <summary>
        /// Initialize the MenuScreen components of this object - No built in header, custom font
        /// </summary>
        /// <param name="font">Font used to draw menu items on screen</param>
        public MenuScreen(SpriteFont font)
        {
            //Store a custom font for this screen
            this.font = font;
        }

        /// <summary>
        /// Initialize the MenuScreen components of this object - Header included, no custom font included
        /// </summary>
        /// <param name="title">Text header for the screen</param>
        public MenuScreen(string title)
        {
            //Save the header for the screen
            this.title = title;
        }

        /// <summary>
        /// Initialize the MenuScreen components of this object - Header included, custom font included
        /// </summary>
        /// <param name="title">Text header for the screen</param>
        /// <param name="font">Font used to draw menu items on screen</param>
        public MenuScreen(string title, SpriteFont font)
        {
            //Save the header for the screen
            this.title = title;

            //Store a custom font for this screen
            this.font = font;
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Run checks on font, check if one has already been initialized or if we can 
        /// take one from ScreenManager
        /// </summary>
        public override void LoadContent()
        {
            //If no custom font has been set
            if (font == null)
            {
                //Take the font from ScreenManager
                font = screenManager.Font;

                //If that is still null then throw an error
                if (font == null)
                    throw new ContentLoadException("Font not loaded in ScreenManager");
            }

            //Fully initialize the title
            if (title != null)
                title = new DrawableString(title, new Vector2(screenManager.ScreenDimensions.X / 2, 30), font);
        }

        /// <summary>
        /// Update the current menu selection and check if a button has been hit
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        public override void Update(GameTime gameTime)
        {
            //PART 1: Check if menu selection has been changed
            //Check all gamepads for input
            for (int i = 0; i < screenManager.Input.NumOfGamepad; i++)
            {
                //1st. Check sticks
                //If the stick is over 50% pushed in one direction then change menu item
                //But only once! not a hold
                if (screenManager.Input.IsStickPressed(Stick.Left, Direction.Up, (PlayerIndex)i))
                    MoveItem(false);
                else if (screenManager.Input.IsStickPressed(Stick.Left, Direction.Down, (PlayerIndex)i))
                    MoveItem(true);

                //2nd. check gamepad buttons
                //If up or down is pressed on the Dpad then change accordingly
                if (screenManager.Input.IsButtonPressed(Buttons.DPadUp, (PlayerIndex)i))
                    MoveItem(false);
                else if (screenManager.Input.IsButtonPressed(Buttons.DPadDown, (PlayerIndex)i))
                    MoveItem(true);
            }

            //3rd. Check keyboard keys
            //If up or down is pressed on the Dpad OR is W or D is pressed then change accordingly
            if (screenManager.Input.IsKeyPressed(Keys.Up) || screenManager.Input.IsKeyPressed(Keys.W))
                MoveItem(false);
            else if (screenManager.Input.IsKeyPressed(Keys.Down) || screenManager.Input.IsKeyPressed(Keys.S))
                MoveItem(true);

            //4th. Check mouse
            //Check if the mouse collides with any of the menuitems and if it does, then set that as the current item
            for (int i = 0; i < menuItem.Count; i++)
                if (menuItem[i].IsMouseIntersect() && menuItem[i].State == MenuObjectState.Enabled)
                {
                    currentItem = i;
                    break;
                };

            //TODO: Change the cursor depending on if it is on a menu item or not.

            //PART 2: Check if a menu item has been selected
            //I.E. Clicked on, etc.
            //Check all input devices at once
            if (screenManager.Input.IsButtonReleased(Buttons.A, PlayerIndex.One) ||
                screenManager.Input.IsButtonReleased(Buttons.Start, PlayerIndex.One) ||
                screenManager.Input.IsKeyReleased(Keys.Enter) ||
                (screenManager.Input.IsLeftMouseReleased() && screenManager.Input.IsMouseIntersect(menuItem[currentItem].Hitbox)))
                menuItem[currentItem].Update(gameTime, true);
            else
                menuItem[currentItem].Update(gameTime, false);
        }

        /// <summary>
        /// Method to move the current menuItem around, ensuring the cursor does not land on a disabled item
        /// </summary>
        /// <param name="isDown">Is the cursor moving down? or Up?</param>
        private void MoveItem(bool isDown)
        {
            //If the cursor is moving down
            if (isDown)
            {
                do
                {
                    //Move the cursor down 1, checking for overflow
                    currentItem++;
                    if (currentItem > menuItem.Count - 1)
                        currentItem = 0;
                }
                //If this menuItem is disabled, then move down and try again
                while (menuItem[currentItem].State != MenuObjectState.Enabled);
            }
            //Else the cursor is moving up
            else
            {
                do
                {
                    //Move the cursor up 1, checking for overflow
                    currentItem--;
                    if (currentItem < 0)
                        currentItem = menuItem.Count - 1;
                }
                //If this menuItem is disabled, then move up and try again
                while (menuItem[currentItem].State != MenuObjectState.Enabled);
            }
        }

        /// <summary>
        /// Draw the text header and all the menu items on screen
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw these objects on screen</param>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            //If the header is valid, then draw the header
            if (title != null)
            {
                //Draw the header on screen
                title.Draw(spriteBatch);
            }

            //Draw all the menu components stored in MenuList
            for(int i = 0; i < menuItem.Count; i++)
            {
                //Use i == currentItem to display if this item is currently selected or not
                menuItem[i].Draw(spriteBatch, gameTime, (i == currentItem));
            }
        }

                                                #endregion METHODS

                                                #region PUBLIC METHODS

        /// <summary>
        /// Add an item to the menu item list
        /// </summary>
        /// <param name="item">MenuObject to add to the current list</param>
        protected void AddItem(MenuObject item)
        {
            //Add item to list
            menuItem.Add(item);
        }

        /// <summary>
        /// Remove an item from the menu item list
        /// </summary>
        /// <param name="item">MenuObject to remove from the current list</param>
        protected void RemoveItem(MenuObject item)
        {
            //Remove item from list
            menuItem.Remove(item);
        }

                                                #endregion PUBLIC METHODS
    }
}
