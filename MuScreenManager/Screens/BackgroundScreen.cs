﻿#region FILE DESCRIPTION
//-----------------------------------------------------------------------------
// MuScreenManager
// 
// BackgroundScreen.cs
//-----------------------------------------------------------------------------
#endregion

#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion USING

namespace MuScreenManager
{
    /// <summary>
    /// Simple screen to draw an image on the background. Allows, for example, menu screens to be added and removed around it with no 
    /// need for unloading and reloading images in the graphics card
    /// </summary>
    public class BackgroundScreen : Screen
    {
                                                #region PROPERTIES

        /// <summary>
        /// Image to show in background
        /// </summary>
        private Texture2D bgImage;

        /// <summary>
        /// Name and locaion of the sprite in Content
        /// </summary>
        private string bgImageLocation;

        /// <summary>
        /// Stretch or shrink the image to fill the screem
        /// </summary>
        private bool screenFill;

                                                #endregion PROPERTIES

                                                #region METHODS

        /// <summary>
        /// Initialize the BackgroundScreen components - default screen stretch false
        /// </summary>
        /// <param name="bgImageLocation">Name and locaion of the sprite to show in the background in Content</param>
        /// <param name="screenFill">Stretch or shrink the image to fill the screem</param>
        public BackgroundScreen(string bgImageLocation, bool screenFill = false)
            : base(ScreenState.Paused)
        {
            this.bgImageLocation = bgImageLocation;
            this.screenFill = screenFill;
        }

        /// <summary>
        /// Load the image from content into the graphics card for showing
        /// </summary>
        public override void Initialize()
        {
            bgImage = content.Load<Texture2D>(bgImageLocation);
        }

        /// <summary>
        /// Draw the sprite, either stretched or not
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw these objects on screen</param>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (!screenFill)
                spriteBatch.Draw(bgImage, Vector2.Zero, Color.White);
            else
                spriteBatch.Draw(bgImage, new Rectangle(0, 0, (int)screenManager.ScreenDimensions.X, (int)screenManager.ScreenDimensions.Y), bgImage.Bounds, Color.White);
        }

                                                #endregion METHODS
    }
}