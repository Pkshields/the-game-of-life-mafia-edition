﻿#region FILE DESCRIPTION
//-----------------------------------------------------------------------------
// MuScreenManager
// VERSION: Nightly-05-04-2012
// 
// ScreenManager.cs
//-----------------------------------------------------------------------------
#endregion

#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion USING

namespace MuScreenManager
{
    /// <summary>
    /// Core ScreenManager engine, used to control, manage and display multiple GameScreens for an XNA game
    /// </summary>
    public class ScreenManager : DrawableGameComponent
    {
                                                #region PROPERTIES

        /// <summary>
        /// List of current screens
        /// </summary>
        private List<Screen> screenList = new List<Screen>();

        /// <summary>
        /// Reference to the initial game that created it all
        /// </summary>
        private Game game;
        public new Game Game
        {
            get { return game; }
        }

        /// <summary>
        /// Global Spritebatch, allows for drawing of all elements under the same spriteBatch
        /// </summary>
        private SpriteBatch spriteBatch;

        /// <summary>
        /// Provided by the Game, the device currently used to manage the graphics
        /// </summary>
        private GraphicsDevice graphicsDevice;
        public new GraphicsDevice GraphicsDevice
        {
            get { return graphicsDevice; }
        }

        /// <summary>
        /// Provides global access to an instance fo the Input APIs, automatically updated by ScreenManager
        /// </summary>
        private Input input;
        public Input Input
        {
            get { return input; }
        }

        /// <summary>
        /// Provides global access to an instance of AudioManager, if generated using settigns provided by the user 
        /// upon initialization
        /// </summary>
        private Audio audio;
        public Audio Audio
        {
            get { return audio; }
        }

        /// <summary>
        /// Relative URI to the content directory of this game
        /// </summary>
        private string contentDir;
        public string ContentDir
        {
            get { return contentDir; }
        }

        /// <summary>
        /// Global font that can be used anywhere in the ScreenManager
        /// </summary>
        private SpriteFont font;
        public SpriteFont Font
        {
            get { return font; }
        }

        /// <summary>
        /// Get the full screen dimensions for this game
        /// </summary>
        public Vector2 ScreenDimensions
        {
            get { return new Vector2(graphicsDevice.Viewport.Bounds.Width, graphicsDevice.Viewport.Bounds.Height); }
        }


                                                #endregion PROPERTIES

                                                #region METHODS

        /// <summary>
        /// Initialize an instance of the ScreenManager
        /// </summary>
        /// <param name="game">Reference to the initial game that created it all</param>
        /// <param name="graphicsDevice">Game's generated GraphicsDevice</param>
        /// <param name="contentDir">Directory where the content from the Content project</param>
        /// <param name="font">Global font used throughout the ScreenManager and menus</param>
        /// <param name="audioSettings">Settings to generate a SoundManager using</param>
        public ScreenManager(Game game, GraphicsDevice graphicsDevice, string contentDir, SpriteFont font, AudioSettings audioSettings)
            : base(game)
        {
            //Store the required references
            this.game = game;
            this.graphicsDevice = graphicsDevice;
            spriteBatch = new SpriteBatch(graphicsDevice);
            this.contentDir = contentDir;

            //Initialize the Input object
            input = new Input();

            //Initialize the Audio Manager
            audio = new Audio(audioSettings);

            //Store the global font
            this.font = font;
        }

        /// <summary>
        /// Initialize an instance of the ScreenManager
        /// </summary>
        /// <param name="game">Reference to the initial game that created it all</param>
        /// <param name="graphicsDevice">Game's generated GraphicsDevice</param>
        /// <param name="contentDir">Directory where the content from the Content project</param>
        /// <param name="font">Global font used throughout the ScreenManager and menus</param>
        public ScreenManager(Game game, GraphicsDevice graphicsDevice, string contentDir, SpriteFont font)
            : base(game)
        {
            //Store the required references
            this.game = game;
            this.graphicsDevice = graphicsDevice;
            spriteBatch = new SpriteBatch(graphicsDevice);
            this.contentDir = contentDir;

            //Initialize the Input object
            input = new Input();

            //Store the global font
            this.font = font;
        }

        /// <summary>
        /// Initialize an instance of the ScreenManager
        /// </summary>
        /// <param name="game">Reference to the initial game that created it all</param>
        /// <param name="graphicsDevice">Game's generated GraphicsDevice</param>
        /// <param name="contentDir">Directory where the content from the Content project</param>
        /// <param name="audioSettings">Global font used throughout the ScreenManager and menus</param>
        public ScreenManager(Game game, GraphicsDevice graphicsDevice, string contentDir, AudioSettings audioSettings)
            : base(game)
        {
            //Store the required references
            this.game = game;
            spriteBatch = new SpriteBatch(graphicsDevice);
            this.contentDir = contentDir;

            //Initialize the Input object
            input = new Input();

            //Initialize the Audio Manager
            audio = new Audio(audioSettings);
        }

        /// <summary>
        /// Initialize an instance of the ScreenManager
        /// </summary>
        /// <param name="game">Reference to the initial game that created it all</param>
        /// <param name="graphicsDevice">Game's generated GraphicsDevice</param>
        /// <param name="contentDir">Directory where the content from the Content project</param>
        public ScreenManager(Game game, GraphicsDevice graphicsDevice, string contentDir)
            : base(game)
        {
            //Store the required references
            this.game = game;
            spriteBatch = new SpriteBatch(graphicsDevice);
            this.contentDir = contentDir;

            //Initialize the Input object
            input = new Input();
        }

        /// <summary>
        /// Update the ScreenManager and all screens within
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        public override void Update(GameTime gameTime)
        {
            //Update the input devices
            input.Update(gameTime);

            Console.WriteLine(input.GetMousePosition());

            //Update the audio manager
            if (audio != null)
                audio.Update();

            //Store a copy of the current screen list for updating
            Screen[] currentScreenList = screenList.ToArray();

            //Update all the screens in the current screenlist
            foreach (var screen in currentScreenList)
            {
                //Only update the screen if the screen is active and not paused
                if (screen.ScreenState == ScreenState.Active)
                    screen.Update(gameTime);
            }
        }

        /// <summary>
        /// Draw the screens within ScreenManager
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        public override void Draw(GameTime gameTime)
        {
            //Start the spritebatch
            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null);

            //Draw all the screens
            foreach (var screen in screenList)
            {
                screen.Draw(spriteBatch, gameTime);
            }

            //Everything on all screens has been drawn, end spritebatch
            spriteBatch.End();
        }

                                                #endregion METHODS

                                                #region PUBLIC METHODS

        /// <summary>
        /// Add a screen to this ScreenManager
        /// </summary>
        /// <param name="screen">Screen to add to rotation</param>
        public void AddScreen(Screen screen)
        {
            //Add screen to current list of screens
            screenList.Add(screen);

            screen.ScreenManager = this;    //Add this ScreenManager reference to the screen
            screen.OnInitialize();          //Initializes objects that couldn't be initialized in the constructor
            screen.LoadContent();           //Load the content for the screen
            screen.Initialize();            //Initialize anything that couldn't be initialized in the constructor
                                            //because of content needing loaded
        }

        /// <summary>
        /// Remove the screen from active ScreenManager
        /// </summary>
        /// <param name="screen">Screen to remove from rotation</param>
        public void RemoveScreen(Screen screen)
        {
            //Remove screen from list, won't appear next updte cycle
            screen.UnloadContent();
            screenList.Remove(screen);
        }

        /// <summary>
        /// Remove all screens from the current screen rotation
        /// </summary>
        public void RemoveAllScreens()
        {
            //Unload all the content from the current screens
            foreach (var screen in screenList)
                screen.UnloadContent();

            //Remove all the screens
            screenList.Clear();
        }

        /// <summary>
        /// Change the ScreenState in all currently active screens
        /// </summary>
        /// <param name="screenState">ScreenState to change all screens to</param>
        /// <param name="unchangedScreen">Screen to not change the ScreenState of</param>
        public void ChangeAllScreenStates(ScreenState screenState, Screen unchangedScreen = null)
        {
            foreach (var screen in screenList)
                if (screen != unchangedScreen)
                    screen.ScreenState = screenState;
        }
                                                #endregion PUBLIC METHODS
    }
}
