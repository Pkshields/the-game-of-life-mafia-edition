﻿#region FILE DESCRIPTION
//-----------------------------------------------------------------------------
// MuScreenManager
// 
// MenuObject.cs
//-----------------------------------------------------------------------------
#endregion

#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion USING

namespace MuScreenManager
{
    /// <summary>
    /// Item mainly used for a list style menu setup - specifically for MenuScreen
    /// </summary>
    public abstract class MenuObject
    {
                                                #region PROPERTIES

        /// <summary>
        /// Current ScreenManager controlling this menu and thus MenuItem
        /// </summary>
        protected ScreenManager screenManager;

        /// <summary>
        /// Current state of the menuObject
        /// </summary>
        protected MenuObjectState state = MenuObjectState.Enabled;
        public MenuObjectState State
        {
            get { return state; }
            set { state = value; }
        }

        /// <summary>
        /// Font used to draw menu items on screen
        /// </summary>
        protected SpriteFont font;

        /// <summary>
        /// Text to display for this button
        /// </summary>
        protected string text;
        public string Text
        {
            get { return text; }
        }

        /// <summary>
        /// Position of the text on screen
        /// </summary>
        protected Vector2 position;
        public Vector2 Position
        {
            get { return position; }
        }

        /// <summary>
        /// Origin of that text (center)
        /// </summary>
        protected Vector2 stringOrigin;

        /// <summary>
        /// Return the hitbox that the menuItem is occupying on screen
        /// </summary>
        protected Rectangle hitbox;
        public Rectangle Hitbox
        {
            get { return hitbox; }
        }

                                                #endregion PROPERTIES

                                                #region METHODS

        /// <summary>
        /// Initialize the MenuObject components of this object
        /// </summary>
        /// <param name="text">Text to display for this button</param>
        /// <param name="position">Position of the button on screen</param>
        /// <param name="screenManager">Current ScreenManager controlling this menu</param>
        public MenuObject(string text, Vector2 position, ScreenManager screenManager)
        {
            //Store the ScreenManager immediately
            this.screenManager = screenManager;

            //Check if there is a general font in ScreenManager
            if (screenManager.Font != null)
            {
                //No errors, store all required information
                this.text = text;
                this.position = position;
                this.font = screenManager.Font;
            }
            //No font loaded in ScreenManager, let peoples know
            else
                throw new ContentLoadException("Font not loaded in ScreenManager");

            //Measure the string and set up the hitbox for the menuitem on screen
            Vector2 size = font.MeasureString(text);
            hitbox = new Rectangle((int)(position.X - (size.X / 2)), (int)(position.Y - (size.Y / 2)), (int)size.X, (int)size.Y);

            //Calculate the string origin
            this.stringOrigin = size / 2;
        }

        /// <summary>
        /// Initialize the MenuObject components of this object
        /// </summary>
        /// <param name="text">Text to display for this button</param>
        /// <param name="position">Position of the button on screen</param>
        /// <param name="font">Font used to draw menu items on screen</param>
        public MenuObject(string text, Vector2 position, SpriteFont font)
        {
            //No error check required, store all required information
            this.text = text;
            this.position = position;
            this.font = font;

            //Measure the string and set up the hitbox for the menuitem on screen
            Vector2 size = font.MeasureString(text);
            hitbox = new Rectangle((int)(position.X - (size.X / 2)), (int)(position.Y - (size.Y / 2)), (int)size.X, (int)size.Y);

            //Calculate the string origin
            this.stringOrigin = size / 2;
        }

                                                #endregion METHODS

                                                #region VIRTUAL METHODS

        /// <summary>
        /// Update method to override to check for inputs etc.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        /// <param name="isSelected">Choose if this menu item is already selected</param>
        public virtual void Update(GameTime gameTime, bool isSelected)
        { }

        /// <summary>
        /// Draw method to overload to draw objects on screen
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw these objects on screen</param>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        /// <param name="isSelected">Choose if this menu item is already selected</param>
        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime, bool isSelected)
        { }

        /// <summary>
        /// Check if the mouse intersects this menuitem at all
        /// </summary>
        /// <returns>Result of mouse intersect check</returns>
        public virtual bool IsMouseIntersect()
        {
            return screenManager.Input.IsMouseIntersect(hitbox);
        }

                                                #endregion VIRTUAL METHODS
    }

    /// <summary>
    /// Placeholder - Used to pass data from this to event method - currently unneeded but good just in case
    /// </summary>
    public class MenuObjectEventArgs : EventArgs
    { }

    /// <summary>
    /// Enum to show what state the MenuObject currently is in
    /// </summary>
    public enum MenuObjectState
    {
        Enabled = 0,
        DrawOnly = 1,
        Disabled = 2
    };
}