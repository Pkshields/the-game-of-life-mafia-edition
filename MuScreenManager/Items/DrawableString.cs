﻿#region FILE DESCRIPTION
//-----------------------------------------------------------------------------
// MuScreenManager
// 
// DrawableString.cs
//-----------------------------------------------------------------------------
#endregion

#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion USING

namespace MuScreenManager
{
    /// <summary>
    /// Simple DrawableString class to represent a string that has a position on screen and that can be drawn
    /// </summary>
    public class DrawableString
    {
                                                #region PREFERENCES

        /// <summary>
        /// Text to draw on screen
        /// </summary>
        private string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        /// <summary>
        /// Position of that text on screen
        /// </summary>
        private Vector2 position;

        /// <summary>
        /// Origin of that text (center)
        /// </summary>
        private Vector2 stringOrigin;

        /// <summary>
        /// Font used to draw the text
        /// </summary>
        private SpriteFont font;


                                                #endregion PREFERENCES

                                                #region METHODS

        /// <summary>
        /// Initialize an instance of DrawableString
        /// </summary>
        /// <param name="text">Text to draw on screen</param>
        /// <param name="position">Position of that text on screen</param>
        /// <param name="font">Font used to draw the text</param>
        public DrawableString(string text, Vector2 position, SpriteFont font)
        {
            this.text = text;
            this.position = position;
            this.font = font;

            this.stringOrigin = font.MeasureString(this.text) / 2;
        }

        /// <summary>
        /// Initialize an instance of DrawableString
        /// </summary>
        /// <param name="text">Text to draw on screen</param>
        /// <param name="position">Position of that text on screen</param>
        /// <param name="font">Font used to draw the text</param>
        public DrawableString(DrawableString text, Vector2 position, SpriteFont font)
        {
            this.text = text.text;
            this.position = position;
            this.font = font;

            this.stringOrigin = font.MeasureString(this.text) / 2;
        }

        /// <summary>
        /// Initialize an instance of DrawableString with just text to use in String form
        /// </summary>
        /// <param name="text">Text to draw on screen</param>
        private DrawableString(string text)
        {
            this.text = text;
        }

        /// <summary>
        /// Draw the contained text on screen
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw these objects on screen</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, text, position, Color.White, 0f, stringOrigin, 1, SpriteEffects.None, 0);
        }

        /// <summary>
        /// Draw the contained text on screen
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw these objects on screen</param>
        /// <param name="color">Color to colour the text</param>
        public void Draw(SpriteBatch spriteBatch, Color color)
        {
            spriteBatch.DrawString(font, text, position, color, 0f, stringOrigin, 1, SpriteEffects.None, 0);
        }

                                                #endregion METHODS

                                                #region IMPLICIT METHODS

        /// <summary>
        /// Implicitly force a string into a DrawableString
        /// </summary>
        /// <param name="value">String value to force into a string</param>
        /// <returns>Drawable string with just text value</returns>
        public static implicit operator DrawableString(string value)
        {
            return new DrawableString(value);
        }

        /// <summary>
        /// Implicitly allow a DrawableString to be referenced as a String value
        /// </summary>
        /// <param name="value">DrawableString to be converted to a String</param>
        /// <returns>String value</returns>
        public static implicit operator string(DrawableString value)
        {
            return value.text;
        }

                                                #endregion IMPLICIT METHODS
    }
}
