﻿#region FILE DESCRIPTION
//-----------------------------------------------------------------------------
// MuScreenManager
// 
// MenuItem.cs
//-----------------------------------------------------------------------------
#endregion

#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion USING

namespace MuScreenManager
{
    /// <summary>
    /// Item mainly used for a list style menu setup - specifically for MenuScreen
    /// </summary>
    public class MenuItem : MenuObject
    {
                                                #region EVENTS

        /// <summary>
        /// Event for use on button hit
        /// </summary>
        public event EventHandler<MenuObjectEventArgs> OnButtonSelect;

                                                #endregion EVENTS

                                                #region METHODS

        /// <summary>
        /// Initialize the MenuItem components of this object
        /// </summary>
        /// <param name="text">Text to display for this button</param>
        /// <param name="position">Position of the button on screen</param>
        /// <param name="screenManager">Current ScreenManager controlling this menu</param>
        public MenuItem(string text, Vector2 position, ScreenManager screenManager)
            : base(text, position, screenManager)
        { }

        /// <summary>
        /// Initialize the MenuItem components of this object
        /// </summary>
        /// <param name="text">Text to display for this button</param>
        /// <param name="position">Position of the button on screen</param>
        /// <param name="font">Font used to draw menu items on screen</param>
        public MenuItem(string text, Vector2 position, SpriteFont font)
            : base(text, position, font)
        { }

        /// <summary>
        /// Method used to execute the OnButtonSelect event if this button is selected
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        /// <param name="isSelected">Choose if this menu item is already selected</param>
        public override void Update(GameTime gameTime, bool isSelected)
        {
            if (isSelected && OnButtonSelect != null)
                OnButtonSelect(this, new MenuObjectEventArgs());
        }

        /// <summary>
        /// Draw this menu item on screen following parameters
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw these objects on screen</param>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        /// <param name="isSelected">Choose if this menu item is already selected</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, bool isSelected)
        {
            //Only draw if the MenuItem is enabled in some way
            if (state != MenuObjectState.Disabled)
            {
                if (isSelected)
                {
                    //Make the menu item pulse. Calculate a rate of pulsation using Sin
                    float pulse = (float)((Math.Sin(gameTime.TotalGameTime.TotalSeconds * 4) * 0.15) + 0.15);
                    spriteBatch.DrawString(font, text, position, Color.Yellow, 0, stringOrigin, 1 + pulse, SpriteEffects.None, 0);
                }
                else
                {
                    //If Item is enabled, then draw normally
                    //If it is disabled, then fade it out
                    if (state == MenuObjectState.Enabled)
                        spriteBatch.DrawString(font, text, position, Color.White, 0, stringOrigin, 1, SpriteEffects.None, 0);
                    else
                        spriteBatch.DrawString(font, text, position, Color.White * 0.4f, 0, stringOrigin, 1, SpriteEffects.None, 0);
                }
            }
        }

                                                #endregion METHODS
    }
}
