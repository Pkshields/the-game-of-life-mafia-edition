﻿#region FILE DESCRIPTION
//-----------------------------------------------------------------------------
// MuScreenManager
// 
// MenuOption.cs
//-----------------------------------------------------------------------------
#endregion

#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion USING

namespace MuScreenManager
{
    public class MenuOption : MenuObject
    {
        /// <summary>
        /// List of all the options available for this MenuOption
        /// </summary>
        private string[] menuOptions;

        /// <summary>
        /// Positions and hitboxes for all things on screen
        /// </summary>
        Vector2 leftButtonPosition, leftButtonOrigin;
        Vector2 rightButtonPosition, rightButtonOrigin;
        Vector2 menuOptionPosition, menuOptionOrigin;
        Rectangle leftButtonRect, rightButtonRect;
        
        /// <summary>
        /// Currently selected menu option
        /// </summary>
        private int currentOption = 0;
        public int SelectedID
        {
            get { return currentOption; }
        }

        /// <summary>
        /// Returns the curretly selected value in string form
        /// </summary>
        public string SelectedValue
        {
            get { return menuOptions[currentOption]; }
        }

        /// <summary>
        /// Initialize an instance of MenuObject
        /// </summary>
        /// <param name="text">Name of this MenuOption to show</param>
        /// <param name="position">Position of the text</param>
        /// <param name="menuOptions">List of selectable options</param>
        /// <param name="menuPosition">Position of the selectable options on screen</param>
        /// <param name="screenManager">Current ScreenManager controlling the menu</param>
        public MenuOption(string text, Vector2 position, string[] menuOptions, Vector2 menuPosition, ScreenManager screenManager)
            : base(text, position, screenManager)
        {
            //Store the menuOptions
            this.menuOptions = menuOptions;

            //Go through the menuOptions and find the largest width string, 
            //so we can know how wide the menuOptions need to be on screen
            Vector2 largestString = Vector2.Zero, tempVector;
            foreach (var menuOption in menuOptions)
            {
                //Get the size of the string the compare it to the current largest
                tempVector = font.MeasureString(menuOption);
                if (tempVector.X > largestString.X)
                    largestString = tempVector;
            }

            //Get the origin positions of the strings we are showing on screen
            leftButtonOrigin = rightButtonOrigin = font.MeasureString("<") / 2;
            menuOptionOrigin = largestString / 2;

            //Calculate the positions of the strings on screen
            leftButtonPosition = menuPosition;
            menuOptionPosition = leftButtonPosition + new Vector2(8 + menuOptionOrigin.X, 0);
            rightButtonPosition = menuOptionPosition + new Vector2(10 + menuOptionOrigin.X, 0);

            //Calculate the hitboxes for the two left and right buttons for switching options through mouse
            leftButtonRect = new Rectangle((int)(leftButtonPosition.X - leftButtonOrigin.X),
                                           (int)(leftButtonPosition.Y - leftButtonOrigin.Y),
                                           (int)(leftButtonOrigin.X * 2),
                                           (int)(leftButtonOrigin.Y * 2));
            rightButtonRect = new Rectangle((int)(rightButtonPosition.X - rightButtonOrigin.X),
                                            (int)(rightButtonPosition.Y - rightButtonOrigin.Y),
                                            (int)(rightButtonOrigin.X * 2),
                                            (int)(rightButtonOrigin.Y * 2));
        }

        /// <summary>
        /// Initialize an instance of MenuObject
        /// </summary>
        /// <param name="text">Name of this MenuOption to show</param>
        /// <param name="position">Position of the text</param>
        /// <param name="menuOptions">List of selectable options</param>
        /// <param name="menuPosition">Position of the selectable options on screen</param>>
        /// <param name="font">Get the font used to draw the strings</param>
        public MenuOption(string text, Vector2 position, string[] menuOptions, Vector2 menuPosition, SpriteFont font)
            : base(text, position, font)
        {
            //Store the menuOptions
            this.menuOptions = menuOptions;

            //Go through the menuOptions and find the largest width string, 
            //so we can know how wide the menuOptions need to be on screen
            Vector2 largestString = Vector2.Zero, tempVector;
            foreach (var menuOption in menuOptions)
            {
                //Get the size of the string the compare it to the current largest
                tempVector = font.MeasureString(menuOption);
                if (tempVector.X > largestString.X)
                    largestString = tempVector;
            }

            //Get the origin positions of the strings we are showing on screen
            leftButtonOrigin = rightButtonOrigin = font.MeasureString("<") / 2;
            menuOptionOrigin = largestString / 2;

            //Calculate the positions of the strings on screen
            leftButtonPosition = menuPosition;
            menuOptionPosition = leftButtonPosition + new Vector2(8 + menuOptionOrigin.X, 0);
            rightButtonPosition = menuOptionPosition + new Vector2(10 + menuOptionOrigin.X, 0);

            //Calculate the hitboxes for the two left and right buttons for switching options through mouse
            leftButtonRect = new Rectangle((int)(leftButtonPosition.X - leftButtonOrigin.X),
                                           (int)(leftButtonPosition.Y - leftButtonOrigin.Y), 
                                           (int)(leftButtonOrigin.X * 2), 
                                           (int)(leftButtonOrigin.Y * 2));
            rightButtonRect = new Rectangle((int)(rightButtonPosition.X - rightButtonOrigin.X),
                                            (int)(rightButtonPosition.Y - rightButtonOrigin.Y), 
                                            (int)(rightButtonOrigin.X * 2), 
                                            (int)(rightButtonOrigin.Y * 2));
        }

        /// <summary>
        /// Check the input devices to see if the current option needs changed
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        /// <param name="isSelected">Choose if this menu item is already selected</param>
        public override void Update(GameTime gameTime, bool isSelected)
        {
            //Check all gamepads for input
            for (int i = 0; i < screenManager.Input.NumOfGamepad; i++)
            {
                //1st. Check sticks
                //If the stick is over 50% pushed in one direction then change menu item
                //But only once! not a hold
                if (screenManager.Input.IsStickPressed(Stick.Left, Direction.Left, (PlayerIndex)i))
                    currentOption--;
                else if (screenManager.Input.IsStickPressed(Stick.Left, Direction.Right, (PlayerIndex)i))
                    currentOption++;

                //2nd. check gamepad buttons
                //If left or right is pressed on the Dpad then change accordingly
                if (screenManager.Input.IsButtonPressed(Buttons.DPadLeft, (PlayerIndex)i))
                    currentOption--;
                else if (screenManager.Input.IsButtonPressed(Buttons.DPadRight, (PlayerIndex)i))
                    currentOption++;
            }

            //3rd. Check keyboard keys
            //If left or right is pressed on the Dpad OR is A or D is pressed then change accordingly
            if (screenManager.Input.IsKeyPressed(Keys.Left) || screenManager.Input.IsKeyPressed(Keys.A))
                currentOption--;
            else if (screenManager.Input.IsKeyPressed(Keys.Right) || screenManager.Input.IsKeyPressed(Keys.D))
                currentOption++;

            //4th. Check mouse
            //Check if the mouse collides with any of the menuitems and if it does, then set that as the current item
            if (screenManager.Input.IsMouseIntersect(leftButtonRect) && screenManager.Input.IsLeftMousePressed())
                currentOption--;
            else if (screenManager.Input.IsMouseIntersect(rightButtonRect) && screenManager.Input.IsLeftMousePressed())
                currentOption++;

            //Now that all the menuitem movement is done, check if we are still within range for the menuOptions
            //If it is less than 0 or more than max item, then loop around
            if (currentOption < 0)
                currentOption = menuOptions.Length - 1;
            else if (currentOption > menuOptions.Length - 1)
                currentOption = 0;
        }

        /// <summary>
        /// Draw this MenuOption on screen following parameters
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw these objects on screen</param>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        /// <param name="isSelected">Choose if this menu item is already selected</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, bool isSelected)
        {
            //Only draw if the MenuItem is enabled in some way
            if (state != MenuObjectState.Disabled)
            {
                //If the menu option is disabled, then it is faded out
                float fadeNum = (state == MenuObjectState.Enabled ? 1 : 0.4f);

                //Draw the main item name on screen depending on the isSelected parameter
                if (isSelected)
                {
                    //Make the menu item pulse. Calculate a rate of pulsation using Sin
                    float pulse = (float)((Math.Sin(gameTime.TotalGameTime.TotalSeconds * 4) * 0.15) + 0.15);
                    spriteBatch.DrawString(font, text, position, Color.Yellow, 0, stringOrigin, 1 + pulse, SpriteEffects.None, 0);
                }
                else
                {
                    spriteBatch.DrawString(font, text, position, Color.White * fadeNum, 0, stringOrigin, 1, SpriteEffects.None, 0);
                }

                //Draw the buttons and current object
                spriteBatch.DrawString(font, "<", leftButtonPosition, Color.Yellow * fadeNum, 0, leftButtonOrigin, 1, SpriteEffects.None, 0);
                spriteBatch.DrawString(font, ">", rightButtonPosition, Color.Yellow * fadeNum, 0, rightButtonOrigin, 1, SpriteEffects.None, 0);
                spriteBatch.DrawString(font, menuOptions[currentOption], menuOptionPosition, Color.White * fadeNum, 0, menuOptionOrigin, 1, SpriteEffects.None, 0);
            }
        }

        /// <summary>
        /// Check if the mouse intersects this menuitem at all 
        /// Needs overridden tio check for more collision boxes than normal
        /// </summary>
        /// <returns>Result of mouse intersect check</returns>
        public override bool IsMouseIntersect()
        {
            return screenManager.Input.IsMouseIntersect(hitbox) ||
                   screenManager.Input.IsMouseIntersect(leftButtonRect) ||
                   screenManager.Input.IsMouseIntersect(rightButtonRect);
        }
    }
}
