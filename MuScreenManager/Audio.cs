﻿#region FILE DESCRIPTION
//-----------------------------------------------------------------------------
// MuScreenManager
// 
// Sound.cs
//-----------------------------------------------------------------------------
#endregion

#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion USING

namespace MuScreenManager
{
    /// <summary>
    /// Official Sound Manager of MuScreenManager! Probably be worthwhile to read over the code so you can know how 
    /// to set up your XACT project to take full advantage of the manager, or use the provided example project
    /// </summary>
    public class Audio
    {
                                                #region PROPERTIES

        /// <summary>
        /// Audio Engine running all audio in this ScreenManager
        /// </summary>
        private AudioEngine engine;

        /// <summary>
        /// List of soundbanks loaded with unique IDs
        /// </summary>
        private Dictionary<string, SoundBank> soundBanks;

        /// <summary>
        /// List of wavebanks loaded with unique IDs
        /// </summary>
        private Dictionary<string, WaveBank> waveBanks;

        /// <summary>
        /// List of categories loaded with unique IDs
        /// </summary>
        private Dictionary<string, AudioCategory> categories;

                                                #endregion PROPERTIES

                                                #region METHODS

        /// <summary>
        /// Generate an instance of the Sound manager
        /// </summary>
        /// <param name="settings">Settings to initialize Sound manager with</param>
        public Audio(AudioSettings settings)
        {
            //Generate the AudioEngine
            this.engine = new AudioEngine(settings.Location + "/" + settings.AudioEngine + ".xgs");

            //Initialize the banks
            soundBanks = new Dictionary<string, SoundBank>();
            waveBanks = new Dictionary<string, WaveBank>();
            categories = new Dictionary<string, AudioCategory>();

            //Initialize the banks with the settings we were given
            for (int i = 0; i < settings.BankNames.Count; i++)
            {
                soundBanks.Add(settings.BankNames[i], new SoundBank(engine, settings.Location + "/" + settings.SoundBanks[i] + ".xsb"));
                waveBanks.Add(settings.BankNames[i], new WaveBank(engine, settings.Location + "/" + settings.SoundBanks[i] + ".xwb"));
            }

            //Initialize the categories with the same settings
            for (int i = 0; i < settings.Categories.Count; i++)
            {
                categories.Add(settings.Categories[i], engine.GetCategory(settings.Categories[i]));
            }
        }

        /// <summary>
        /// Update the Sound manager
        /// </summary>
        public void Update()
        {
            //Update the audio engine
            engine.Update();
        }

                                                #endregion METHODS

                                                #region PUBLIC METHODS

        /// <summary>
        /// Get a sound cue from a certain sound bank
        /// </summary>
        /// <param name="soundBank">Sound bank to get the audio cue from</param>
        /// <param name="cueName">Cue to get from said sound bank</param>
        /// <returns>Cue in Music form</returns>
        public Music GetMusic(string soundBank, string cueName)
        {
            return new Music(soundBanks[soundBank].GetCue(cueName));
        }

        /// <summary>
        /// Play a sound cue. One off, plays till it's done!
        /// </summary>
        /// <param name="soundBank">Sound bank to get the audio cue from</param>
        /// <param name="cueName">Cue to get from said sound bank</param>
        public void PlayMusic(string soundBank, string cueName)
        {
            soundBanks[soundBank].GetCue(cueName).Play();
        }

        /// <summary>
        /// Change the volume of a certain category
        /// </summary>
        /// <param name="category">Category to change the volume to</param>
        /// <param name="volume">Volume between 0f and 1f to change the volume to</param>
        public void ChangeVolume(string category, float volume)
        {
            //Check that the volume is in range
            if (volume >= 0 && volume <= 1)
                categories[category].SetVolume(volume);
        }

                                                #endregion PUBLIC METHODS
    }

    /// <summary>
    /// Wrapper class to control the Cue provided by SoundManager
    /// </summary>
    public class Music
    {
                                                #region PROPERTIES

        /// <summary>
        /// Cue being played by this Music object
        /// </summary>
        private Cue cue;

        /// <summary>
        /// Get the playing state for the cue
        /// </summary>
        public bool IsPlaying
        {
            get { return cue.IsPlaying; }
        }

        /// <summary>
        /// Get the paused state for the cue
        /// </summary>
        public bool IsPaused
        {
            get { return cue.IsPaused; }
        }

        /// <summary>
        /// Get the stopped state for the cue
        /// </summary>
        public bool IsStopped
        {
            get { return cue.IsStopped; }
        }

                                                #endregion PROPERTIES

                                                #region METHODS

        /// <summary>
        /// Generate an instance of the Sound manager
        /// </summary>
        /// <param name="cue">Cue to play</param>
        public Music(Cue cue)
        {
            //Check the cue is created
            if (cue.IsPrepared)
                this.cue = cue;
            else
                //throw an exception, Cue is not ready or is incorrect for some reason
                throw new InvalidOperationException("Cue generated is invalid.");

        }

                                                #endregion METHODS

                                                #region PUBLIC METHODS

        /// <summary>
        /// Play this cue, as long that this cue isn't already playing
        /// </summary>
        public void Play()
        {
            //As long as the cue is ready and isn't already playing
            if (cue.IsPrepared && !cue.IsPlaying)
                cue.Play();
            //Else check is the cue is paused and needs to be unpaused
            else if (cue.IsPaused)
                cue.Resume();
        }

        /// <summary>
        /// Pause the cue, as long as the cue is already playing
        /// </summary>
        public void Pause()
        {
            //As long as the cue is already playing
            if (cue.IsPlaying)
                cue.Pause();
        }

        /// <summary>
        /// Completely stop the cue under the authored stop style
        /// </summary>
        public void Stop()
        {
            //As long as the cue is already playing
            if (cue.IsPlaying)
                cue.Stop(AudioStopOptions.AsAuthored);
        }

        /// <summary>
        /// Change the volume of the cue 
        /// NOTE: Only supported by cues set up with this setup (using variable "volume") - 
        /// http://www.luminance.org/blog/code/2009/07/23/simple-dynamic-music-with-xact
        /// </summary>
        /// <param name="volume">Volume between 0f and 1f to change the volume to</param>
        public void ChangeVolume(float volume)
        {
            //Check that the volume is in range
            if (volume >= 0 && volume <= 1)
                cue.SetVariable("Volume", volume);
        }

                                                #endregion PUBLIC METHODS
    }

    /// <summary>
    /// Object to provide when initializing audio manager
    /// </summary>
    public class AudioSettings
    {
                                                #region PROPERTIES

        /// <summary>
        /// Directory location for the XACT files
        /// </summary>
        public string Location;

        /// <summary>
        /// Filename for the Audio Engine
        /// </summary>
        public string AudioEngine;

        /// <summary>
        /// List of bank name IDs to assign to the SoundBanks
        /// </summary>
        public List<string> BankNames;

        /// <summary>
        /// List of SoundBanks to generate for the above bank name IDs
        /// </summary>
        public List<string> SoundBanks;

        /// <summary>
        /// List of WaveBanks to generate for the above bank name IDs
        /// </summary>
        public List<string> WaveBanks;

        /// <summary>
        /// List of Categories to generate
        /// </summary>
        public List<string> Categories;

                                                #endregion PROPERTIES

                                                #region METHODS

        /// <summary>
        /// Initialize the AudioSettings object 
        /// Add Banks and Categories through methods!
        /// </summary>
        /// <param name="Location">Directory location for the XACT files</param>
        /// <param name="AudioEngine">Filename for the Audio Engine</param>
        public AudioSettings(string Location, string AudioEngine)
        {
            this.Location = Location;
            this.AudioEngine = AudioEngine;
            BankNames = new List<string>();
            SoundBanks = new List<string>();
            WaveBanks = new List<string>();
            Categories = new List<string>();
        }

                                                #endregion METHODS

                                                #region PUBLIC METHODS

        /// <summary>
        /// Add a SoundBank and WaveBank to the AudioManager
        /// </summary>
        /// <param name="bankName">ID to give to these banks</param>
        /// <param name="bankFileName">File name (without extension) for the banks</param>
        public void AddBank(string bankName, string bankFileName)
        {
            BankNames.Add(bankName);
            SoundBanks.Add(bankFileName);
            WaveBanks.Add(bankFileName);
        }

        /// <summary>
        /// Add a SoundBank and WaveBank to the AudioManager
        /// </summary>
        /// <param name="bankName">ID to give to these banks</param>
        /// <param name="soundBankFileName">File name (without extension) for the SoundBank</param>
        /// <param name="waveBankFileName">File name (without extension) for the WaveBank</param>
        public void AddBank(string bankName, string soundBankFileName, string waveBankFileName)
        {
            BankNames.Add(bankName);
            SoundBanks.Add(soundBankFileName);
            WaveBanks.Add(waveBankFileName);
        }

        /// <summary>
        /// Add a category to generate to the settings
        /// </summary>
        /// <param name="category">Name of the category in the XACT manager</param>
        public void AddCategory(string category)
        {
            Categories.Add(category);
        }

                                                #endregion PUBLIC METHODS
    }
}
