﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace GameOfLifeMafia
{
    /// <summary>
    /// This class checks the position of the mouse and wheter or not the mouse button is pressed.
    /// </summary>
    public abstract class Input
    {
        /// <summary>
        /// This checks if the left mouse button has been pressed this frame
        /// </summary>
        public static Boolean LeftMouseButtonPressed;

        /// <summary>
        /// This checks if the left mouse button is being held down
        /// </summary>
        public static Boolean LeftMouseButtonDown;

        /// <summary>
        /// This checks if the left mouse button has been released this frame
        /// </summary>
        public static Boolean LeftMouseButtonReleased;

        /// <summary>
        /// Returns the current position of the mouse
        /// </summary>
        public static Vector2 MousePosition;

        /// <summary>
        /// The current state of the mouse.
        /// </summary>
        private static MouseState _currentMouseState = Mouse.GetState();

        /// <summary>
        /// The previous MouseState used for the purposes of comparisson.
        /// </summary>
        private static MouseState _previousMouseState = Mouse.GetState();

        /// <summary>
        /// Updates all the variables of Input.
        /// </summary>
        public static void Update()
        {
            //Updates the GameState
            _previousMouseState = _currentMouseState;
            _currentMouseState = Mouse.GetState();

            //Updates the static variables
            LeftMouseButtonReleased = ((_previousMouseState.LeftButton == ButtonState.Pressed) && (_currentMouseState.LeftButton == ButtonState.Released));
            LeftMouseButtonDown = (_currentMouseState.LeftButton == ButtonState.Pressed);
            LeftMouseButtonPressed = ((_previousMouseState.LeftButton == ButtonState.Released) && (_currentMouseState.LeftButton == ButtonState.Pressed));

            //Sets the Position of the Mouse
            MousePosition = new Vector2(_currentMouseState.X, _currentMouseState.Y);
        }
    }
}
