﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MuScreenManager;

namespace GameOfLifeMafia
{
    class CareerMenuScreen : MenuScreen
    {
        private Player[] playerList;
        private Career[] careerList;
        private int currentPlayer;
        private BoardGame boardGame;

        Texture2D whitePixel;
        Rectangle drawRect;

        /// <summary>
        /// Initialise an instance of a StartGameMenu using MenuScreen
        /// </summary>
        public CareerMenuScreen(Player[] playerList, Career[] careerList, int currentPlayer, BoardGame boardGame)
            : base("Select Career - Player " + (currentPlayer+1))
        {
            this.playerList = playerList;
            this.careerList = careerList;
            this.currentPlayer = currentPlayer;
            this.boardGame = boardGame;
        }

        /// <summary>
        /// Initialise the MenuItems components of the menu
        /// </summary>
        public override void Initialize()
        {
            Vector2 startingPosition = new Vector2(screenManager.ScreenDimensions.X / 2, 100);
            int offsetPosition = 100;

            MuScreenManager.MenuItem tempItem;
            for (int i = 0; i < careerList.Length; i++)
            {
                tempItem = new MuScreenManager.MenuItem(careerList[i].Title, startingPosition + new Vector2(0, offsetPosition * i), screenManager);
                tempItem.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(tempItem_OnButtonSelect);
                AddItem(tempItem);
            }

        }

        public override void LoadContent()
        {
            base.LoadContent();

            drawRect = new Rectangle(0, 0, (int)screenManager.ScreenDimensions.X, (int)screenManager.ScreenDimensions.Y);

            whitePixel = new Texture2D(screenManager.GraphicsDevice, 1, 1);
            whitePixel.SetData(new[] { Color.White });
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            //Draw the translucent background
            spriteBatch.Draw(whitePixel, drawRect, Color.Black * 0.6f);

            //Draw the rest of MenuItem
            base.Draw(spriteBatch, gameTime);
        }

        void tempItem_OnButtonSelect(object sender, MenuObjectEventArgs e)
        {
            MuScreenManager.MenuItem currentItem = (MuScreenManager.MenuItem)sender;

            for (int i = 0; i < careerList.Length; i++)
            {
                if (currentItem.Text.Equals(careerList[i].Title))
                {
                    playerList[currentPlayer].AddCareer(careerList[i]);

                    if (currentPlayer < playerList.Length - 1)
                    {
                        screenManager.RemoveScreen(this);
                        screenManager.AddScreen(new CareerMenuScreen(playerList, careerList, currentPlayer+1, boardGame));
                    }
                    else
                    {
                        screenManager.RemoveScreen(this);
                        screenManager.ChangeAllScreenStates(ScreenState.Active);
                        boardGame.AfterCareerSetup(playerList);
                    }
                }
            }
        }
    }
}