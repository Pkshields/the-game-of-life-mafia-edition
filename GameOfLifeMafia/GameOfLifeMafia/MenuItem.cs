﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameOfLifeMafia
{
    /// <summary>
    /// Menu Types
    /// </summary>
    public enum MenuItemType
    {
        ClickButton = 0,
        SelectText = 1
    };

    public class MenuItem
    {
        /// <summary>
        /// Image of the button for the UI
        /// </summary>
        private Texture2D _menuImage;

        /// <summary>
        /// Position of the button
        /// </summary>
        private Vector2 _mItemPos;

        /// <summary>
        /// Type of button in use
        /// </summary>
        private MenuItemType _mItemType;

        /// <summary>
        /// Is the button enabled?
        /// </summary>
        private bool buttonEnabled = true;

        /// <summary>
        /// Has the button been clicked in?
        /// </summary>
        private bool buttonClicked = false;

        /// <summary>
        /// 
        /// </summary>
        public Vector2 Position
        {
            get { return _mItemPos; }
        }

        /// <summary>
        /// Create an instance of a Menu button
        /// </summary>
        /// <param name="buttonImage">UI image of the button</param>
        /// <param name="pos">Position of the button on screen</param>
        /// <param name="miType">Type of button in use</param>
        public MenuItem(Texture2D buttonImage, Vector2 pos, MenuItemType miType)
        {
            _menuImage = buttonImage;
            _mItemPos = pos;
            _mItemType = miType;
        }

        /// <summary>
        /// Reset the button to unclicked
        /// </summary>
        public void ResetButtonClicked()
        { this.buttonClicked = false; }

        /// <summary>
        /// Set if the button is enabled
        /// </summary>
        public bool IsEnabled
        {
            set { this.buttonEnabled = value; }
        }

        /// <summary>
        /// Update and check if the button has been pressed or not
        /// </summary>
        /// <returns>Returns the state of the button, eurher pressed in or not</returns>
        public bool UpdateMenuItem()
        {
            //Only check if the button is enabled
            if (buttonEnabled)
            {
                //If we are type 0?
                if (this._mItemType == 0)
                {
                    //Get the current position of the mouse
                    Vector2 mouse = Input.MousePosition;

                    //If the mouse is within the boundaries of the button and is the mouse is clicked down, then the button has been pressed
                    //if ((mpos.X < _menuImage.Bounds.Right) && (mpos.Y < _menuImage.Bounds.Bottom) && (mpos.X > _menuImage.Bounds.Left) && (mpos.Y > _menuImage.Bounds.Top) && (ms.LeftButton == ButtonState.Pressed))
                    if ((mouse.X < _mItemPos.X + _menuImage.Bounds.Right) && (mouse.Y < _mItemPos.Y + _menuImage.Bounds.Bottom) && (mouse.X > _mItemPos.X) && (mouse.Y > _mItemPos.Y) && (Input.LeftMouseButtonPressed == true))
                        //If yes then set the boolean to pressed
                        buttonClicked = true;

                }

                //Return the state of the button, pressed or not
                return buttonClicked;
            }
            else
                //If the button is disabled then just return no
                return false;
        }

        /// <summary>
        /// Draw the button on screen 
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void DrawMenuItem(SpriteBatch spriteBatch)
        {
            //Set the shading of the button, if the button is disabled then grey out the button slightly
            int color;
            if ((this._mItemType == 0 && buttonClicked) || (!buttonEnabled))
                color = 190;
            else
                color = 255;            

            //Draw the button (with the shading)
            spriteBatch.Draw(_menuImage, new Vector2(_mItemPos.X, _mItemPos.Y),
                new Color(color, color, color, (byte)MathHelper.Clamp(255, 0, 255)));
        }

    }

}

