﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace GameOfLifeMafia
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Career
    {
        private string careerTitle;
        public string Title
        {
            get { return careerTitle; }
        }
        private CareerType careerType;
        public CareerType CareerType
        {
            get { return careerType; }
        }
        private int[] salary;
        private int promotionLevel;

        public Career()
        { }
        
        /// <summary>
        /// Initialise method for Career Objects
        /// </summary>
        /// <param name="ct">Name of the Career</param>
        /// <param name="cl">If it is Mafia or Not Mafia</param>
        /// <param name="sal">Array list of size 5 for salaries at promotion levels</param>
        /// <param name="prom">dictates which number from the array is chosen when player is paid</param>
        public Career(String careerTitle, CareerType careerType, int[] salary)
        {
            this.careerTitle = careerTitle;
            this.careerType = careerType;
            this.salary = salary;
            promotionLevel = 0;
        }

        /// <summary>
        /// this method is used to allow player access to their salary
        /// </summary>
        /// <returns>Returns their current salary</returns>
        public int Salary()
        { return salary[promotionLevel]; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int[] AllSalaries()
        { return salary; }
        
        /// <summary>
        /// Method that promotes player
        /// </summary>
        public void ChangeSalary(CareerLevel level)
        {
            if (level == CareerLevel.Promote)
            {
                if (promotionLevel < 4)
                    promotionLevel += 1;
            }
            else
            {
                if (promotionLevel > 0)
                    promotionLevel -= 1;
            }
        }
    }

    public enum CareerType
    {
        Mafia = 0, 
        Civilian = 1
    };

    public enum CareerLevel
    {
        Promote = 0,
        Demote = 1
    };
}
