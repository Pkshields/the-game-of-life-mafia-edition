﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MuScreenManager;

namespace GameOfLifeMafia
{
    public class BoardManager
    {
        /// <summary>
        /// Current rotation of the board
        /// </summary>
        private int boardState;

        /// <summary>
        /// Max rotation of the board
        /// </summary>
        private int maxBoardState;

        /// <summary>
        /// List holding all static squares on the board
        /// </summary>
        private Dictionary<int, Square> staticSquareBoard;

        /// <summary>
        /// List holding all dynamic rotating squares on the board for each location
        /// </summary>
        private Dictionary<int, Square[]> rotatingSquareBoard;

        /// <summary>
        /// Initialize the board manager and generate all possible squares
        /// </summary>
        /// <param name="boardImage">Image to show for the square</param>
        /// <param name="cardDeck">Deck of Story Squares</param>
        /// <param name="listOHouses">List of houses</param>
        /// <param name="listOfPlayers">List of players</param>
        /// <param name="blankButton">Blank UI button</param>
        /// <param name="font">Font to draw text with</param>
        public BoardManager(Texture2D boardImage, StoryEvents cardDeck, House[] listOHouses, Player[] listOfPlayers, CareerManager careerManager, Texture2D blankButton, SpriteFont font, ScreenManager screenManager)
        {
            //Set the initial and max rotations of the board
            boardState = 0;
            maxBoardState = 3;

            //Initialize the two lists
            staticSquareBoard = new Dictionary<int, Square>();
            rotatingSquareBoard = new Dictionary<int, Square[]>();

            //Initialize all the static squares with all necessary information, including linking squares
            //Centre board
            staticSquareBoard.Add(0, new RegularSquare(this, new Vector2(650, 255), 1, boardImage));
            staticSquareBoard.Add(1, new PayDaySquare(this, new Vector2(650, 200), 2, boardImage));
            staticSquareBoard.Add(2, new RegularSquare(this, new Vector2(630, 155), 3, boardImage));
            staticSquareBoard.Add(3, new StorySquare(this, new Vector2(590, 125), cardDeck, 4, boardImage));
            staticSquareBoard.Add(4, new RegularSquare(this, new Vector2(555, 95), 5, boardImage));
            staticSquareBoard.Add(5, new StorySquare(this, new Vector2(500, 90), cardDeck, 6, boardImage));
            staticSquareBoard.Add(6, new StorySquare(this, new Vector2(450, 125), cardDeck, 7, boardImage));
            staticSquareBoard.Add(7, new SpinToWinSquare(this, new Vector2(420, 165), 8, boardImage, null, "Spin to Win Square! :D", listOfPlayers));
            staticSquareBoard.Add(8, new RegularSquare(this, new Vector2(405, 215), 9, boardImage));
            staticSquareBoard.Add(9, new RegularSquare(this, new Vector2(415, 265), 10, boardImage));
            staticSquareBoard.Add(10, new PayDaySquare(this, new Vector2(440, 305), 11, boardImage));
            staticSquareBoard.Add(11, new RegularSquare(this, new Vector2(485, 345), 12, boardImage));
            staticSquareBoard.Add(12, new TravelSquare(this, new Vector2(513, 383), 23, new int[] { 0 }, boardImage, IslandID.Land1));/////port need to assign destinations!!
            staticSquareBoard.Add(13, new RegularSquare(this, new Vector2(630, 295), 14, boardImage));
            staticSquareBoard.Add(14, new SetGoodGuyCareerSquare(this, new Vector2(580, 315), 15, boardImage, screenManager, careerManager)); //Pay loan
            staticSquareBoard.Add(15, new RegularSquare(this, new Vector2(550, 355), 16, boardImage));
            staticSquareBoard.Add(16, new RegularSquare(this, new Vector2(515, 330), 17, boardImage));
            staticSquareBoard.Add(17, new RegularSquare(this, new Vector2(485, 300), 18, boardImage));
            staticSquareBoard.Add(18, new StorySquare(this, new Vector2(453, 265), cardDeck, 19, boardImage));
            staticSquareBoard.Add(19, new RegularSquare(this, new Vector2(440, 225), 20, boardImage));
            staticSquareBoard.Add(20, new RegularSquare(this, new Vector2(445, 185), 21, boardImage));
            staticSquareBoard.Add(21, new RegularSquare(this, new Vector2(465, 150), 22, boardImage));
            staticSquareBoard.Add(22, new UniSquare(this, new Vector2(508, 120), 5, boardImage)); //stop square to join main path

            //Island 1
            staticSquareBoard.Add(23, new TravelSquare(this, new Vector2(571, 540), 24, new int[] { 46, 67, 88, 119 }, boardImage, IslandID.Land2)); //boat
            staticSquareBoard.Add(24, new StorySquare(this, new Vector2(580, 490), cardDeck, 25, boardImage));
            staticSquareBoard.Add(25, new RegularSquare(this, new Vector2(595, 445), 26, boardImage));
            staticSquareBoard.Add(26, new PayDaySquare(this, new Vector2(635, 430), 27, boardImage)); //payday
            staticSquareBoard.Add(27, new RegularSquare(this, new Vector2(675, 455), 28, boardImage));
            staticSquareBoard.Add(28, new RegularSquare(this, new Vector2(645, 485), 29, boardImage));
            staticSquareBoard.Add(29, new TravelSquare(this, new Vector2(643, 520), 30, new int[] { 36, 51, 70 }, boardImage, IslandID.Land2)); //plane
            staticSquareBoard.Add(30, new RegularSquare(this, new Vector2(635, 560), 31, boardImage));
            staticSquareBoard.Add(31, new HouseSquare(this, new Vector2(621, 595), 32, boardImage, listOHouses[0], blankButton, font)); //house
            staticSquareBoard.Add(32, new StorySquare(this, new Vector2(589, 628), cardDeck, 33, boardImage));
            staticSquareBoard.Add(33, new StorySquare(this, new Vector2(551, 611), cardDeck, 34, boardImage));
            staticSquareBoard.Add(34, new RegularSquare(this, new Vector2(498, 585), 35, boardImage)); //stop event
            staticSquareBoard.Add(35, new RegularSquare(this, new Vector2(482, 616), 36, boardImage));
            staticSquareBoard.Add(36, new TravelSquare(this, new Vector2(445, 631), 37, new int[] { 29, 51, 70 }, boardImage, IslandID.Land2)); //plane
            staticSquareBoard.Add(37, new StorySquare(this, new Vector2(415, 598), cardDeck, 38, boardImage));
            staticSquareBoard.Add(38, new RegularSquare(this, new Vector2(392, 554), 39, boardImage));
            staticSquareBoard.Add(39, new HouseSquare(this, new Vector2(386, 505), 40, boardImage, listOHouses[0], blankButton, font)); //house
            staticSquareBoard.Add(40, new RegularSquare(this, new Vector2(400, 460), 41, boardImage));
            staticSquareBoard.Add(41, new RegularSquare(this, new Vector2(439, 425), 42, boardImage));
            staticSquareBoard.Add(42, new SpinToWinSquare(this, new Vector2(487, 427), 43, boardImage, null, "Spin to Win Square! :D", listOfPlayers)); //spin to win
            staticSquareBoard.Add(43, new StorySquare(this, new Vector2(493, 478), cardDeck, 44, boardImage));
            staticSquareBoard.Add(44, new RegularSquare(this, new Vector2(486, 527), 45, boardImage));
            staticSquareBoard.Add(45, new RegularSquare(this, new Vector2(530, 553), 23, boardImage));

            //Island 2
            staticSquareBoard.Add(46, new TravelSquare(this, new Vector2(285, 571), 47, new int[] { 23, 67, 88, 119 }, boardImage, IslandID.Land3)); //boat
            staticSquareBoard.Add(47, new StorySquare(this, new Vector2(260, 607), cardDeck, 48, boardImage));
            staticSquareBoard.Add(48, new RegularSquare(this, new Vector2(223, 621), 49, boardImage));
            staticSquareBoard.Add(49, new StorySquare(this, new Vector2(185, 622), cardDeck, 50, boardImage));
            staticSquareBoard.Add(50, new StorySquare(this, new Vector2(140, 608), cardDeck, 51, boardImage));
            staticSquareBoard.Add(51, new TravelSquare(this, new Vector2(100, 581), 52, new int[] { 29, 36, 70 }, boardImage, IslandID.Land3)); //plane
            staticSquareBoard.Add(52, new RegularSquare(this, new Vector2(81, 544), 53, boardImage));
            staticSquareBoard.Add(53, new RegularSquare(this, new Vector2(85, 499), 54, boardImage));
            staticSquareBoard.Add(54, new HouseSquare(this, new Vector2(125, 482), 55, boardImage, listOHouses[1], blankButton, font)); //house
            staticSquareBoard.Add(55, new RegularSquare(this, new Vector2(148, 521), 56, boardImage));
            staticSquareBoard.Add(56, new RegularSquare(this, new Vector2(164, 558), 57, boardImage));
            staticSquareBoard.Add(57, new SpinToWinSquare(this, new Vector2(213, 544), 58, new int[] { 60 }, boardImage, null, "Spin to Win Square! :D", listOfPlayers)); //spin to win and junction
            staticSquareBoard.Add(58, new StorySquare(this, new Vector2(244, 550), cardDeck, 59, boardImage));
            staticSquareBoard.Add(59, new StorySquare(this, new Vector2(267, 556), cardDeck, 46, boardImage)); //back to boat
            staticSquareBoard.Add(60, new StorySquare(this, new Vector2(237, 502), cardDeck, 61, boardImage));
            staticSquareBoard.Add(61, new StorySquare(this, new Vector2(235, 458), cardDeck, 62, boardImage));
            staticSquareBoard.Add(62, new HouseSquare(this, new Vector2(264, 429), 63, boardImage, listOHouses[2], blankButton, font)); //house
            staticSquareBoard.Add(63, new RegularSquare(this, new Vector2(297, 415), 64, boardImage));
            staticSquareBoard.Add(64, new PayDaySquare(this, new Vector2(316, 452), 65, boardImage));
            staticSquareBoard.Add(65, new RegularSquare(this, new Vector2(317, 494), 66, boardImage));
            staticSquareBoard.Add(66, new RegularSquare(this, new Vector2(307, 532), 46, boardImage));

            //Island 3
            staticSquareBoard.Add(67, new TravelSquare(this, new Vector2(140, 125), 68, new int[] { 23, 46, 88, 119 }, boardImage, IslandID.Land4)); //boat
            staticSquareBoard.Add(68, new RegularSquare(this, new Vector2(180, 146), 69, new int[] { 85 }, boardImage)); //junction
            staticSquareBoard.Add(69, new RegularSquare(this, new Vector2(209, 180), 70, boardImage));
            staticSquareBoard.Add(70, new TravelSquare(this, new Vector2(226, 217), 71, new int[] { 29, 36, 51 }, boardImage, IslandID.Land4)); //plane
            staticSquareBoard.Add(71, new RegularSquare(this, new Vector2(240, 259), 72, boardImage));
            staticSquareBoard.Add(72, new StorySquare(this, new Vector2(240, 304), cardDeck, 73, boardImage));
            staticSquareBoard.Add(73, new StorySquare(this, new Vector2(210, 325), cardDeck, 74, boardImage));
            staticSquareBoard.Add(74, new RegularSquare(this, new Vector2(174, 342), 75, boardImage));
            staticSquareBoard.Add(75, new RegularSquare(this, new Vector2(156, 375), 76, boardImage));
            staticSquareBoard.Add(76, new RegularSquare(this, new Vector2(140, 412), 77, boardImage));
            staticSquareBoard.Add(77, new RegularSquare(this, new Vector2(94, 404), 78, boardImage));
            staticSquareBoard.Add(78, new HouseSquare(this, new Vector2(54, 376), 79, boardImage, listOHouses[3], blankButton, font)); //house
            staticSquareBoard.Add(79, new RegularSquare(this, new Vector2(32, 328), 80, boardImage));
            staticSquareBoard.Add(80, new PayDaySquare(this, new Vector2(47, 277), 81, boardImage)); //payday
            staticSquareBoard.Add(81, new SpinToWinSquare(this, new Vector2(49, 226), 82, boardImage, null, "Spin to Win Square! :D", listOfPlayers)); //Spin to win
            staticSquareBoard.Add(82, new StorySquare(this, new Vector2(25, 174), cardDeck, 83, boardImage));
            staticSquareBoard.Add(83, new StorySquare(this, new Vector2(43, 129), cardDeck, 84, boardImage));
            staticSquareBoard.Add(84, new RegularSquare(this, new Vector2(93, 116), 85, boardImage));
            staticSquareBoard.Add(85, new StorySquare(this, new Vector2(194, 111), cardDeck, 86, boardImage));
            staticSquareBoard.Add(86, new RegularSquare(this, new Vector2(217, 89), 87, boardImage));
            staticSquareBoard.Add(87, new RegularSquare(this, new Vector2(250, 93), 88, boardImage));
            staticSquareBoard.Add(88, new TravelSquare(this, new Vector2(270, 120), 89, new int[] { 23, 46, 67, 119 }, boardImage, IslandID.Land4)); //boat
            staticSquareBoard.Add(89, new RegularSquare(this, new Vector2(263, 155), 90, boardImage));
            staticSquareBoard.Add(90, new SpinToWinSquare(this, new Vector2(262, 189), 91, boardImage, null, "Spin to Win Square! :D", listOfPlayers)); //spin to win
            staticSquareBoard.Add(91, new RegularSquare(this, new Vector2(300, 174), 92, boardImage));
            staticSquareBoard.Add(92, new PromotionSquare(this, new Vector2(339, 182), 93, boardImage)); //promotion
            staticSquareBoard.Add(93, new RegularSquare(this, new Vector2(345, 228), 94, boardImage));
            staticSquareBoard.Add(94, new StorySquare(this, new Vector2(316, 259), cardDeck, 95, boardImage));
            staticSquareBoard.Add(95, new HouseSquare(this, new Vector2(278, 266), 71, boardImage, listOHouses[4], blankButton, font)); //house

            //Island 4
            staticSquareBoard.Add(96, new TravelSquare(this, new Vector2(914, 361), 97, new int[] { 23, 29, 36, 46, 51, 67, 70, 88 }, boardImage, IslandID.Land5)); //boat
            staticSquareBoard.Add(97, new RegularSquare(this, new Vector2(917, 411), 98, boardImage));
            staticSquareBoard.Add(98, new SpinToWinSquare(this, new Vector2(911, 463), 99, boardImage, null, "Spin to Win Square! :D", listOfPlayers)); //spin to win
            staticSquareBoard.Add(99, new RegularSquare(this, new Vector2(908, 513), 100, new int[] { 116 }, boardImage)); //to 2nd junction
            staticSquareBoard.Add(100, new StorySquare(this, new Vector2(870, 541), cardDeck, 101, boardImage));
            staticSquareBoard.Add(101, new HouseSquare(this, new Vector2(832, 552), 102, boardImage, listOHouses[5], blankButton, font)); //house
            staticSquareBoard.Add(102, new RegularSquare(this, new Vector2(797, 576), 103, boardImage));
            staticSquareBoard.Add(103, new RegularSquare(this, new Vector2(759, 581), 104, boardImage));
            staticSquareBoard.Add(104, new RegularSquare(this, new Vector2(727, 555), 105, boardImage));
            staticSquareBoard.Add(105, new StorySquare(this, new Vector2(734, 508), cardDeck, 106, boardImage));
            staticSquareBoard.Add(106, new StorySquare(this, new Vector2(759, 468), cardDeck, 107, boardImage));
            staticSquareBoard.Add(107, new RegularSquare(this, new Vector2(752, 419), 108, new int[] { 113 }, boardImage)); // to 1st junction
            staticSquareBoard.Add(108, new RegularSquare(this, new Vector2(724, 371), 109, boardImage)); //stop event
            staticSquareBoard.Add(109, new RegularSquare(this, new Vector2(742, 329), 110, boardImage));
            staticSquareBoard.Add(110, new PromotionSquare(this, new Vector2(778, 307), 111, boardImage)); //promotion square needs change!!!
            staticSquareBoard.Add(111, new RegularSquare(this, new Vector2(824, 299), 112, boardImage));
            staticSquareBoard.Add(112, new RegularSquare(this, new Vector2(876, 319), 96, boardImage));
            staticSquareBoard.Add(113, new StorySquare(this, new Vector2(792, 404), cardDeck, 114, boardImage)); //1st junction
            staticSquareBoard.Add(114, new HouseSquare(this, new Vector2(830, 388), 115, boardImage, listOHouses[6], blankButton, font)); //house
            staticSquareBoard.Add(115, new StorySquare(this, new Vector2(862, 359), cardDeck, 112, boardImage));
            staticSquareBoard.Add(116, new StorySquare(this, new Vector2(946, 527), cardDeck, 117, boardImage)); //2nd junction
            staticSquareBoard.Add(117, new StorySquare(this, new Vector2(981, 543), cardDeck, 118, boardImage));
            staticSquareBoard.Add(118, new RegularSquare(this, new Vector2(986, 587), 119, boardImage));
            staticSquareBoard.Add(119, new TravelSquare(this, new Vector2(938, 604), 120, new int[] { 23, 46, 67, 88 }, boardImage, IslandID.Land5)); //boat
            staticSquareBoard.Add(120, new RegularSquare(this, new Vector2(885, 632), 121, boardImage));
            staticSquareBoard.Add(121, new PayDaySquare(this, new Vector2(834, 634), 122, boardImage)); //payday
            staticSquareBoard.Add(122, new RegularSquare(this, new Vector2(802, 611), 102, boardImage));

            //prison
            staticSquareBoard.Add(123, new RegularSquare(this, new Vector2(980, 95), 124, boardImage));
            staticSquareBoard.Add(124, new RegularSquare(this, new Vector2(974, 163), 125, boardImage));
            staticSquareBoard.Add(125, new StorySquare(this, new Vector2(978, 212), cardDeck, 126, boardImage));
            staticSquareBoard.Add(126, new RegularSquare(this, new Vector2(971, 259), 127, boardImage));
            staticSquareBoard.Add(127, new RegularSquare(this, new Vector2(934, 266), 128, boardImage));
            staticSquareBoard.Add(128, new RegularSquare(this, new Vector2(925, 226), 129, boardImage));
            staticSquareBoard.Add(129, new RegularSquare(this, new Vector2(933, 180), 130, boardImage));
            staticSquareBoard.Add(130, new StorySquare(this, new Vector2(925, 134), cardDeck, 131, boardImage));
            staticSquareBoard.Add(131, new RegularSquare(this, new Vector2(889, 125), 132, boardImage));
            staticSquareBoard.Add(132, new StorySquare(this, new Vector2(883, 176), cardDeck, 133, boardImage));
            staticSquareBoard.Add(133, new RegularSquare(this, new Vector2(854, 202), 134, boardImage));
            staticSquareBoard.Add(134, new RegularSquare(this, new Vector2(845, 164), 135, boardImage));
            staticSquareBoard.Add(135, new TravelSquare(this, new Vector2(823, 138), 96, new int[] { 23, 46, 67, 88, 119 }, boardImage, IslandID.Prision)); //boat

            //Initialize all the dynamic rotating squares with all necessary information, including linking squares for all versions
     /*       rotatingSquareBoard.Add(6, new Square[] { new RegularSquare(this, new Vector2(250, 350), 14, boardImage), 
                                                      new RegularSquare(this, new Vector2(250, 350), 14, boardImage), 
                                                      new RegularSquare(this, new Vector2(250, 350), 14, boardImage) });
            rotatingSquareBoard.Add(28, new Square[] { new HouseSquare(this, new Vector2(500, 50), 29, boardImage, listOHouses[1], blankButton, font), 
                                                      new RegularSquare(this, new Vector2(500, 50), 29, boardImage), 
                                                      new PayDaySquare(this, new Vector2(500, 50), 29, boardImage) }); */

        }

        /// <summary>
        /// Draw all the squares on the board
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw these objects on screen</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            //Draw all the static squares
            foreach (var square in staticSquareBoard)
                square.Value.DrawSquare(spriteBatch, Color.DarkSalmon);
            
            //Draw all the dynamic squares for this current board rotation
            foreach (var square in rotatingSquareBoard)
                square.Value[boardState].DrawSquare(spriteBatch, (boardState % 2 == 0 ? Color.Blue : Color.Green));

            
        }

        /// <summary>
        /// Get the square for a certain ID, from either static or dynamic lists
        /// </summary>
        /// <param name="squareID">ID of the square to get</param>
        /// <returns>Square requested</returns>
        public Square GetNextSquare(int squareID)
        {
            //If the Static list contains the square, send that back
            if (staticSquareBoard.ContainsKey(squareID))
                return staticSquareBoard[squareID];

            //If the Dynamic list contains the square, send that back
            if (rotatingSquareBoard.ContainsKey(squareID))
                return rotatingSquareBoard[squareID][boardState];

            //Nothing sent back from either list, must be out of range, throw exception
            throw new IndexOutOfRangeException("SquareID is out of range");
        }

        /// <summary>
        /// Rotate the board
        /// </summary>
        public void RotateBoard()
        {
            //Add 1 to the boardState, then make sure it stays within the maximum board thing
            boardState++;
            boardState %= maxBoardState;
        }
    }

    public enum IslandID
    {
        Land1 = 0,
        Land2 = 1,
        Land3 = 2,
        Land4 = 3,
        Land5 = 4,

        Prision = -1,

        Max = 5,
    };
}
