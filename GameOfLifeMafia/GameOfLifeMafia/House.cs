﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace GameOfLifeMafia
{
    [Serializable]
    public class House
    {
        /// <summary>
        /// Monetary value of the house in Solid Shiny Doubloons
        /// </summary>
        private int value;

        /// <summary>
        /// Player currently owning the house
        /// </summary>
        private Player playerID = null;

        /// <summary>
        /// Square currently assigned to the house
        /// </summary>
        private HouseSquare houseSquareID = null;

        /// <summary>
        /// Name of the house
        /// </summary>
        private string name;

        [XmlElement]
        public int HouseValue
        {
            get { return value; }
            set { this.value = value; }
        }

        [XmlElement(IsNullable = true)]
        public Player PlayerID
        {
            get { return playerID; }
            set { this.playerID = value; }
        }

        [XmlElement(IsNullable = true)]
        public HouseSquare HouseSquareID
        {
            get { return houseSquareID; }
            set { this.houseSquareID = value; }
        }

        [XmlElement]
        public string HouseName
        {
            get { return name; }
            set { this.name = value; }
        }

        /// <summary>
        /// Generate a lsit of houses from the (hardcoded) XML file
        /// </summary>
        /// <returns>List of houses in House[] form</returns>
        public static House[] LoadHouses()
        {
            //Get the XML file
            string xmlFile = @"Content/HouseList.xml";

            //Deserialize the XML file into the House List
            XmlSerializer deserializer;
            try
            {
                deserializer = new XmlSerializer(typeof(List<House>));
            }
            catch (Exception ex)
            {
                throw ex.InnerException.InnerException.InnerException;
            }
            TextReader textReader = new StreamReader(xmlFile);
            List<House> newHouseList = (List<House>)deserializer.Deserialize(textReader);
            textReader.Close();

            //Send the house array back to caller
            return newHouseList.ToArray();
        }
    }
}
