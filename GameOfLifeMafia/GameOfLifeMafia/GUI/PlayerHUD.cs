﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace GameOfLifeMafia
{
    public class PlayerHUD
    {
        #region Initilise Variables

        /// <summary>
        /// The position of the HUD
        /// </summary>
        private Vector2 _position;

        /// <summary>
        /// The image from which the Hud Elements will be drawn
        /// </summary>
        private Texture2D _HUDImage;

        /// <summary>
        /// The player for which to draw the HUd for
        /// </summary>
        private Player _player;

        /// <summary>
        /// The hitboxes for the pay off loan and sell house
        /// </summary>
        //private Rectangle[] _buttonHitboxes;

        #endregion

        #region Initilising Object

        public PlayerHUD(Vector2 Position, Texture2D HUDimg, Player Player)
        {
            _position = Position;
            _HUDImage = HUDimg;
            _player = Player;
        }

        #endregion

        #region Update and Draw

        public int Update()
        {
            return 0;
        }

        public void Draw(SpriteBatch spriteBatch, int index)
        {
            spriteBatch.Draw(_HUDImage, new Vector2(-_HUDImage.Width / 2, 0) + _position, _player.Color);
            SpriteText.drawSpriteText("Player " + (index + 1), GlobalVariables.spriteFont, _position + new Vector2(0, 4), new Vector2(8, 8), 1, align.center, spriteBatch, _player.Color);
            SpriteText.drawSpriteText("$" + _player.Money, GlobalVariables.spriteFont, _position + new Vector2(0, 14), new Vector2(8, 8), 2, align.center, spriteBatch, _player.Color);
        }

        #endregion
    }
}
