﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameOfLifeMafia
{
    /// <summary>
    /// This class creates a Selection Box that will check if an option has been clicked.
    /// It will appear on the center of the screen by default
    /// </summary>
    public class SelectionBox
    {
        /// <summary>
        /// This image will be used to draw the selection box.
        /// </summary>
        private static Texture2D _GUIGraphics;

        /// <summary>
        /// This is the font used to write the text.
        /// </summary>
        private static SpriteFont _font;

        /// <summary>
        /// This sets the dimension of the box. The default is 700 by 400.
        /// </summary>
        private Vector2 _dimensions = new Vector2(700, 400);

        /// <summary>
        /// The size of each of the option boxes
        /// </summary>
        private Vector2 _optionDimensions = new Vector2(200, 75);

        /// <summary>
        /// The text to display in the mainbox
        /// Split into the different lines
        /// </summary>
        private string[] _text;

        /// <summary>
        /// The different options for the box.
        /// </summary>
        private string[] _options;

        /// <summary>
        /// The boxes for each of the options.
        /// </summary>
        private Rectangle[] _optionsRectangle;

        /// <summary>
        /// Intilisation of the box
        /// </summary>
        public SelectionBox(String text, String[] options)
        {
            _text = text.Split(new String[] { "\n" }, StringSplitOptions.None);
            _options = options;

            //Sets all the rectangle
            _optionsRectangle = new Rectangle[_options.Length];
            for (int i = 0; i < _options.Length; i++)
            {
                _optionsRectangle[i] = new Rectangle(

                    1024 / 2 - ((10 + (int)_optionDimensions.X) * _options.Length / 2) + (((int)_optionDimensions.X + 20) * i),
                    //1024 / 2 - 20 - (int)_optionDimensions.X + (((int)_optionDimensions.X + 40) * (i % 2)),

                    768 / 2 + (int)_dimensions.Y / 2 - 20 - (int)_optionDimensions.Y,
                    (int)_optionDimensions.X,
                    (int)_optionDimensions.Y);

                if (options.Length == 1)
                {
                    _optionsRectangle[i] = new Rectangle(
                        1024 / 2 - _options.Length / 2 - (int)_optionDimensions.X / 2,
                        768 / 2 + (int)_dimensions.Y / 2 - 20 - (int)_optionDimensions.Y,
                        (int)_optionDimensions.X,
                        (int)_optionDimensions.Y);
                }
            }
        }

        /// <summary>
        /// Checks if any option has been clicked
        /// </summary>
        /// <returns>The option clicked. Returns 0 if nothings clicked</returns>
        public int Update()
        {
            //Checks if any of the boxes are pressed
            for (int i = 0; i < _optionsRectangle.Length; i++)
            {
                if (Input.LeftMouseButtonReleased && _optionsRectangle[i].Contains((int)Input.MousePosition.X, (int)Input.MousePosition.Y))
                    return (i + 1);
            }

            return 0;
        }

        /// <summary>
        /// Draws the box on the screen
        /// </summary>
        /// <param name="spriteBatch">The spritebatch to draw too.</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            //Gets the box size of the SelectionBox
            Rectangle drawBox = new Rectangle(
                (int)(1024 - _dimensions.X) / 2,
                (int)(768 - _dimensions.Y) / 2,
                (int)_dimensions.X,
                (int)_dimensions.Y
                );

            //Draws the box
            spriteBatch.Draw(_GUIGraphics, drawBox, new Rectangle(0, 0, 9, 10), Color.White);

            //Draws the option boxes
            foreach (Rectangle r in _optionsRectangle)
            {
                spriteBatch.Draw(_GUIGraphics, r, new Rectangle(15, 5, 1, 1), Color.White);
            }

            //Goes through all the text and draws it centered
            for (int i = 0; i < _text.Length; i++)
            {
                String s = _text[i];
                float stringHeight = (_font.MeasureString(s).Y / 2) * _text.Length * -1;
                stringHeight += _font.MeasureString(s).Y * i;

                spriteBatch.DrawString(_font, s,
                    new Vector2(
                        (1024 / 2) - (_font.MeasureString(s).X / 2),
                        (768 / 2) - 50 + stringHeight),
                    Color.Black);
            }

            //Draws all the text on the textboxes
            for (int i = 0; i < _options.Length; i++)
            {
                Vector2 stringPosition = new Vector2(
                    (_optionsRectangle[i].X + _optionsRectangle[i].Width / 2) - (_font.MeasureString(_options[i]).X / 2),
                    (_optionsRectangle[i].Y + _optionsRectangle[i].Height / 2) - (_font.MeasureString(_options[i]).Y / 2));

                spriteBatch.DrawString(
                    _font,
                    _options[i],
                    stringPosition,
                    Color.Black
                    );
            }
        }

        /// <summary>
        /// Used to set the graphics that the SelectionBox will use
        /// </summary>
        /// <param name="guiImg">The image.</param>
        public static void SetGraphics(Texture2D guiImg, SpriteFont font)
        {
            _GUIGraphics = guiImg;
            _font = font;
        }
    }
}
