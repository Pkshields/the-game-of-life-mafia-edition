﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameOfLifeMafia
{
    [Serializable]
    /// <summary>
    /// This is the player of the game.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// The position of the player.
        /// </summary>
        private Vector2 _position;
        public Vector2 Position
        {
            get { return _position; }
        }

        /// <summary>
        /// The graphic used to draw the Image.
        /// </summary>
        private Texture2D _playerImage;

        /// <summary>
        /// The current square that the player is occuping.
        /// </summary>
        private Square _currentSquare;

        /// <summary>
        /// The amount of money the player has.
        /// </summary>
        private int _money = 0;
        public int Money
        {
            get { return _money; }
            set { _money = value; }
        }

        /// <summary>
        /// CTF Mode - used to count the score for the end of the game
        /// </summary>
        private int ctfScore = 0;
        public int CTFScore
        {
            get { return ctfScore; }
            set { ctfScore = value; }
        }

        /// <summary>
        /// The color of the player
        /// </summary>
        private Color _color;
        public Color Color
        {
            get { return _color; }
            set { _color = value; }
        }

        /// <summary>
        /// The amount of loans the player owes to give back to the bank
        /// </summary>
        private int loansDue;

        /// <summary>
        /// How fast the player moves from square to square.
        /// </summary>
        private static int _movementSpeed = 1;

        /// <summary>
        /// Players associated Career
        /// </summary>
        private Career career;

        /// <summary>
        /// Return if the career is mafia nor not
        /// </summary>
        public bool? IsMafia
        {
            get
            {
                if (career != null) { return (career.CareerType == CareerType.Mafia); }
                else { return null; }               //Use something like if (IfMafia.HasValue && IfMafia) to check for Mafia == true, similar for false
            }
        }

        private List<Family> _family = new List<Family>();
        private List<House> _houses = new List<House>();

        /// <summary>
        /// Boolean to indicate whether the player has a spouse or not.
        /// </summary>
        private bool _spouse;

        /// <summary>
        /// The number assigned to the player when spin to win is activated.
        /// </summary>
        public int _SpinToWinNo;

        /// <summary>
        /// Method to get the value of spouse
        /// </summary>
        public bool HasSpouse
        {
            get { return _spouse; }
        }

        public bool[] stamps = new bool[(int)IslandID.Max];

        /// <summary>
        /// contains a reference to the players current money
        /// </summary>
        /// <returns>value of money</returns>
        public Career Career
        {
            get { return career; }
        }

        public Square CurrentSquare
        {
            get { return _currentSquare; }
            set { _currentSquare = value; }
        }

        public int HouseCount
        {
            get { return _houses.Count; }
        }

        public int MoneyCount
        { get { return this._money; } }

        /// <summary>
        /// Check if the player has all the island stamps using a simple loop
        /// </summary>
        public bool GotAllStamps
        {
            get 
            {
                foreach (var stamp in stamps)
                    if (!stamp)
                        return false;
                return true;
            }
        }

        //Needed to make the class serializable for Houses XML list
        public Player()
        { }

        /// <summary>
        /// Constructor method for the player.
        /// </summary>
        /// <param name="PlayerImage">The sprite to be used by the sprite.</param>
        /// <param name="StartingSquare">The starting Square for the player.</param>
        public Player(Texture2D PlayerImage, Square StartingSquare)
        {
            _position = StartingSquare.GetPosition();
            _playerImage = PlayerImage;
            _currentSquare = StartingSquare;

            loansDue = 0;
        }

        /// <summary>
        /// Draws the player sprite
        /// </summary>
        /// <param name="spriteBatch">The spritebatch to draw on.</param>
        public void DrawPlayer(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_playerImage, new Vector2(_position.X - 5, _position.Y - 5), Color.White);
        }

        /// <summary>
        /// Checks if the player if the players position is identical to the square he is occuping.
        /// </summary>
        /// <returns>Wheter the player is on his square</returns>
        public Boolean IsOnCurrentSquare()
        {
            return _position == _currentSquare.GetPosition();
        }

        public Boolean IsOnSquare(Square square)
        {
            return _position == square.GetPosition();
        }

        /// <summary>
        /// Moves the player towards the square he is occuping
        /// </summary>
        public void MoveTowardsCurrentSquare()
        {
            //Console.WriteLine(_currentSquare);
            //Gets the distance he is meant to travel of the square
            Vector2 CurrentSquarePosition = _currentSquare.GetPosition();

            //_position.X += Math.Sign(CurrentSquarePosition.X - _position.X) * _movementSpeed;
            //_position.Y += Math.Sign(CurrentSquarePosition.Y - _position.Y) * _movementSpeed;
            Vector2 test = (CurrentSquarePosition - _position);
            if (Math.Abs(test.X) < _movementSpeed && Math.Abs(test.Y) < _movementSpeed)
            {
                _position = CurrentSquarePosition;
            }
            else
            {
                test.Normalize();
                _position += test*_movementSpeed;
            }


            //System.Diagnostics.Debug.WriteLine(CurrentSquarePosition - _position);

        }

        public void MoveTowardsSquare(Square square)
        {
            Vector2 CurrentSquarePosition = square.GetPosition();

            _position.X += Math.Sign(CurrentSquarePosition.X - _position.X) * _movementSpeed;
            _position.Y += Math.Sign(CurrentSquarePosition.Y - _position.Y) * _movementSpeed;

            //System.Diagnostics.Debug.WriteLine(CurrentSquarePosition - _position);
        }

        /// <summary>
        /// This will set the next square for the player to go to.
        /// If the square doesn't have an alternative square it just sets the right square.
        /// Otherwise it will wait until the player has selected one of the squares
        /// </summary>
        /// <returns>Returns true if it is finished deciding on the next square</returns>
        public Boolean SetNextSquare()
        {
            //Checks if the current square is NOT a branching square
            if (_currentSquare.GetNextAlternativeSquare(0) == null)
            {
                _currentSquare = _currentSquare.GetNextSquare();
                return true;
            }
            else
            {
                //Gets the positions of the two squares
                Vector2 SquarePosition = _currentSquare.GetNextSquare().GetPosition();

                //Sets the hitboxes for the two squares
                Rectangle SquareHitbox = new Rectangle((int)SquarePosition.X - 7, (int)SquarePosition.Y - 7, 15, 15);

                //Checks if the player clicks on the next square and then sets the next square to the corresponding one
                if ((SquareHitbox.Contains((int)Input.MousePosition.X, (int)Input.MousePosition.Y)) && (Input.LeftMouseButtonReleased))
                {
                    _currentSquare = _currentSquare.GetNextSquare();
                    return true;
                }

                //Runs through the array and sees wheter any of the squares are being clicked
                for (int i = 0; i < _currentSquare.GetAlternativeArrayLength(); i++)
                {
                    //Gets the hit box
                    SquarePosition = _currentSquare.GetNextAlternativeSquare(i).GetPosition();
                    SquareHitbox = new Rectangle((int)SquarePosition.X - 7, (int)SquarePosition.Y - 7, 15, 15);

                    //Returns the square being clicked
                    if ((SquareHitbox.Contains((int)Input.MousePosition.X, (int)Input.MousePosition.Y)) && (Input.LeftMouseButtonReleased))
                    {
                        _currentSquare = _currentSquare.GetNextAlternativeSquare(i);
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// This Draws the pulse for the selected squares
        /// </summary>
        /// <param name="spriteBatch">The spritebatch to which it will be drawn too.</param>
        /// <param name="animation">How far along the animation is.</param>
        public void DrawSelectionSquare(SpriteBatch spriteBatch, int animation)
        {
            //Only draws the selection effect if there are alternative Squares
            if (_currentSquare.GetAlternativeArrayLength() != 0)
            {
                //Sets the animation scale based on the animation variable
                float AnimationScale = (float)(animation / 30.0) * 2;
                Vector2 SquarePosition = _currentSquare.GetNextSquare().GetPosition();

                //Sets the Square Draw box.
                Rectangle SquareDrawbox = new Rectangle(
                    (int)(SquarePosition.X - (AnimationScale * 7.0)),
                    (int)(SquarePosition.Y - (AnimationScale * 7.0)),
                    (int)(AnimationScale * 15.0),
                    (int)(AnimationScale * 15.0));

                //Draws the squares with transperancy based on the Animationprocess.
                spriteBatch.Draw(_playerImage, SquareDrawbox, Color.White * (2 - AnimationScale));

                //Does the same for all the alternative squares
                for (int i = 0; i < _currentSquare.GetAlternativeArrayLength(); i++)
                {
                    //Sets the animation scale based on the animation variable
                    AnimationScale = (float)(animation / 30.0) * 2;
                    SquarePosition = _currentSquare.GetNextAlternativeSquare(i).GetPosition();

                    //Sets the Square Draw box.
                    SquareDrawbox = new Rectangle(
                        (int)(SquarePosition.X - (AnimationScale * 7.0)),
                        (int)(SquarePosition.Y - (AnimationScale * 7.0)),
                        (int)(AnimationScale * 15.0),
                        (int)(AnimationScale * 15.0));

                    //Draws the squares with transperancy based on the Animationprocess.
                    spriteBatch.Draw(_playerImage, SquareDrawbox, Color.White * (2 - AnimationScale));
                }
            }
        }

        /**********************************************************************************************/

        public bool HouseSold()
        {
            for (int i = 0; i < _houses.Count; i++)
            {
                //Sets the hitbox for this house square
                Vector2 squarePosition = _houses[i].HouseSquareID.GetPosition();
                Rectangle squareHitbox = new Rectangle((int)squarePosition.X - 7, (int)squarePosition.Y - 7, 15, 15);

                //Check if the user has clicked on this, allowign the hosue to be sold
                if ((squareHitbox.Contains((int)Input.MousePosition.X, (int)Input.MousePosition.Y)) && (Input.LeftMouseButtonReleased))
                {
                    //Remove ownership of the house from the player and give them their money back
                    _money += _houses[i].HouseValue;
                    _houses[i].PlayerID = null;
                    _houses.Remove(_houses[i]);
                    return true;
                }
            }

            //No house sold yet, try again next time
            return false;
        }

        public void DrawSellHouseAnimation(SpriteBatch spriteBatch, Texture2D spriteFont, int animation)
        {
            for (int i = 0; i < _houses.Count; i++)
            {
                //Sets the animation scale based on the animation variable
                float AnimationScale = (float)(animation / 30.0) * 2;
                Vector2 SquarePosition = _houses[i].HouseSquareID.GetPosition();

                //Sets the Square Draw box.
                Rectangle SquareDrawbox = new Rectangle(
                    (int)(SquarePosition.X - (AnimationScale * 7.0)),
                    (int)(SquarePosition.Y - (AnimationScale * 7.0)),
                    (int)(AnimationScale * 15.0),
                    (int)(AnimationScale * 15.0));

                //Draws the squares with transperancy based on the Animationprocess.
                spriteBatch.Draw(_playerImage, SquareDrawbox, Color.Blue * (2 - AnimationScale));

                //If the mouse is currently hovered over this square
                //NOTE: Needs optimised
                if (_houses[i].HouseSquareID.Rect.Intersects(new Rectangle((int)Input.MousePosition.X, (int)Input.MousePosition.Y, 1, 1)))
                    SpriteText.drawSpriteText("Sell House: " + _houses[i].HouseName, spriteFont, new Vector2(Input.MousePosition.X - 8, Input.MousePosition.Y), new Vector2(8, 8), 1, align.right, spriteBatch, Color.Crimson);
            }
        }

        /**********************************************************************************************/


        public bool EnterSquareEvent()
        {
            return _currentSquare.EnterSquareEvent(this);
        }

        public bool LandOnSquareEvent()
        {
            return _currentSquare.LandOnSquareEvent(this);
        }

        public void ChangeMoney(int amount)
        {
            _money += amount;
            System.Diagnostics.Debug.WriteLine("The player currently has £" + _money + ".");
        }

        public void AssignSpinToWin(int num)
        {
            _SpinToWinNo = num;
        }

        public void AddFamilyMember(Family member)
        {
            if (_family.Count < 6)
            {
                if (!_spouse && member == Family.Spouse)
                {
                    _family.Add(member);
                    Console.WriteLine("The player gained " + member + " as a family member!");
                    _spouse = true;
                }

                if (member != Family.Spouse)
                {
                    _family.Add(member);
                    Console.WriteLine("The player gained " + member + " as a family member!");
                }
            }
            else
                Console.WriteLine("The player has a full family!");
        }

        public void RemoveFamilyMember(Family? member)
        {
            if (_family.Count > 0)
            {
                if (member == null)
                    _family.RemoveAt(_family.Count - 1);
                else
                {
                    Family deceased = (Family)member;

                    foreach (Family i in _family)
                    {
                        if (deceased == i)
                        {
                            _family.Remove(i);
                            break;
                        }
                    }
                }
            }
        }

        public void GetLoan()
        {
            int loan = 100000;
            _money += loan;
            loansDue += ((loan / 20) + loan);
            System.Diagnostics.Debug.WriteLine("The player currently has £" + _money + ".");
        }

        public void GetPaid()
        {
            if (career != null)
            {
                Console.WriteLine("Get Paid");
                _money += career.Salary();
            }
        }

        public void AddHouse(House newHouse)
        {
            //Add a house to the players arsenal
            _houses.Add(newHouse);
        }

        public void RemoveHouse(House newHouse)
        {
            //Remove a house from the players arsenal
            _houses.Remove(newHouse);
            Console.WriteLine("House worth " + newHouse.HouseValue + " has been removed from player");
        }

        public void AddCareer(Career career)
        {
            this.career = career;
        }

        public void SetCurrentSquare(Square square)
        {
            _position = square.GetPosition();
            _currentSquare = square;
        }

        public void AddStamp(IslandID id)
        {
            if (id != IslandID.Prision)
                stamps[(int) id] = true;
        }
    }
}
