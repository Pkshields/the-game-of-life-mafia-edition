﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameOfLifeMafia
{
    /// <summary>
    /// This class assigns the current player with random numbers using RandomNumber() and spinToWin()
    /// </summary>
    class Spinner
    {
        public static int min = 1; //smallest number on spinner
        public static int max = 11; //largest number on spinner

        /// <summary>
        /// This method generates a random number for the player calling it, between min and max values(1 & 10).
        /// </summary>
        /// <returns>Random number between bounds</returns>
        public static int getNumber()
        {
            Random random = new Random();
            return random.Next(min, max); //assign value
        }


        /// <summary>
        /// Assigns either a red(1) or black(2) value depending on value being modulus 2
        /// </summary>
        /// <returns>colour</returns>
        public static int getColour(int colour)
        {
            if (colour % 2 == 0)
                return 1; //red
            else
                return 2; //black              
        }

        /// <summary>
        /// Returns the winning number of the SpinToWinSquare class
        /// </summary>
        /// <returns>Winning number</returns>
        public static int getSpinToWin(int low, int high)
        {
            Random random = new Random();
            return random.Next(low, high); //assign value
        }
    }
}