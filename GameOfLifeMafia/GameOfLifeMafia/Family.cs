﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameOfLifeMafia
{
    /// <summary>
    /// Current representation of family members
    /// </summary>
    public enum Family
    {
        Spouse = 0,
        Child = 1,
        Pet = 2
    };
}
