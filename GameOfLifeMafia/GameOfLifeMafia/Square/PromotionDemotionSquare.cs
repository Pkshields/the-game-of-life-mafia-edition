﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameOfLifeMafia
{
    class PromotionDemotionSquare : Square
    {
        public PromotionDemotionSquare(BoardManager boardManager, Vector2 position, int nextSquare, Texture2D squareImage, Career careerID)
        {
            //Initialise all the default Square values
            _squareImage = squareImage;
            _position = position;
            this.boardManager = boardManager;
            _nextSquare = nextSquare;
            _alternativeSquare = null;

        }

        public PromotionDemotionSquare(BoardManager boardManager, Vector2 position, int nextSquare, int[] alternativeSquare, Texture2D squareImage, House houseID)
        {
            //Initialise all the default Square values
            _squareImage = squareImage;
            _position = position;
            this.boardManager = boardManager;
            _nextSquare = nextSquare;
            _alternativeSquare = alternativeSquare;

        }

        public override bool EnterSquareEvent(Player player)
        {
            int arbitraryNumber = 3;
            if (arbitraryNumber % 3 == 0)
                player.Career.ChangeSalary(CareerLevel.Promote);
            else
                player.Career.ChangeSalary(CareerLevel.Demote);
            return true;
        }
    }
}
