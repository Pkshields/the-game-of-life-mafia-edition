﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MuScreenManager;

namespace GameOfLifeMafia
{
    class FixedEventSquare : Square
    {
        /// <summary>
        /// Amount of money to add/remove from the player on land
        /// </summary>
        private int moneyChange;

        /// <summary>
        /// Family member to add upon landing of square
        /// </summary>
        private Family? familyChange;

        /// <summary>
        /// Text to display to the player upon landing on the square
        /// </summary>
        private string flavorText;

        /// <summary>
        /// Open a screen, allowing the pla`yer to change their career
        /// </summary>
        private bool allowCareerChange;
        private List<Career> careerList;
        private CareerManager careerManager;
        private ScreenManager screenManager;

        /// <summary>
        /// Create an instance of the Fixed Event Square
        /// </summary>
        /// <param name="position">Position to place the square</param>
        /// <param name="nextSquare">Square to move the player to upon next move</param>
        /// <param name="squareImage">Visual image of the square</param>
        /// <param name="moneyChange">Amount of money to add/remove upon landing on the square</param>
        /// <param name="familyChange">Family member to add upon landing on the square</param>
        /// <param name="forcePlayertoStop">If TRUE, player will stop moving when they move over this square</param>
        /// <param name="flavourText">Text to show the player upon land</param>
        public FixedEventSquare(BoardManager boardManager, Vector2 position, int nextSquare, Texture2D squareImage,
            int? moneyChange, Family? familyChange, bool allowCareerChange, List<Career> careerList, bool forcePlayertoStop, string flavourText, CareerManager careerManager, ScreenManager screenManager)
        {
            //Initialise all the default Square values
            _squareImage = squareImage;
            _position = position;
            this.boardManager = boardManager;
            _nextSquare = nextSquare;
            _alternativeSquare = null;

            //Initialize the values that FixedEventSquare needs
            if (moneyChange != null) this.moneyChange = (int)moneyChange;
            else this.moneyChange = 0;
            this.familyChange = familyChange;
            this.allowCareerChange = allowCareerChange;
            this.careerList = careerList;
            this.forceStopPlayer = forcePlayertoStop;
            this.flavorText = flavourText;
            this.screenManager = screenManager;
            this.careerManager = careerManager;
        }

        /// <summary>
        /// Create an instance of the Fixed Event Square
        /// </summary>
        /// <param name="position">Position to place the square</param>
        /// <param name="nextSquare">Square to move the player to upon next move</param>
        /// <param name="alternativeSquare">Alternative square to move the player to upon next move</param>
        /// <param name="squareImage">Visual image of the square</param>
        /// <param name="moneyChange">Amount of money to add/remove upon landing on the square</param>
        /// <param name="familyChange">Family member to add upon landing on the square</param>
        /// <param name="forcePlayertoStop">If TRUE, player will stop moving when they move over this square</param>
        /// <param name="flavourText">Text to show the player upon land</param>
        public FixedEventSquare(BoardManager boardManager, Vector2 position, int nextSquare, int[] alternativeSquare, Texture2D squareImage,
            int? moneyChange, Family? familyChange, bool allowCareerChange, List<Career> careerList, bool forcePlayertoStop, string flavourText, CareerManager careerManager, ScreenManager screenManager)
        {
            //Initialise all the default Square values
            _squareImage = squareImage;
            _position = position;
            this.boardManager = boardManager;
            _nextSquare = nextSquare;
            _alternativeSquare = alternativeSquare;

            //Initialize the values that FixedEventSquare needs
            if (moneyChange != null) this.moneyChange = (int)moneyChange;
            else this.moneyChange = 0;
            this.familyChange = familyChange; 
            this.allowCareerChange = allowCareerChange;
            this.careerList = careerList;
            this.forceStopPlayer = forcePlayertoStop;
            this.flavorText = flavourText;
            this.screenManager = screenManager;
            this.careerManager = careerManager;
        }

        /// <summary>
        /// Method to run upon player landing on square
        /// </summary>
        /// <param name="player">Curret active player</param>
        /// <returns>UI - Is the game ready to mvoe to the next stage?</returns>
        public override bool LandOnSquareEvent(Player player)
        {
            //Print out the flavour text to the console
            Console.WriteLine("Event!: " + flavorText);

            //Apply money value changes
            player.ChangeMoney(moneyChange);

            //Apply family changes (if there is any)
            if (familyChange != null)
                player.AddFamilyMember((Family)familyChange);

            //Load up the career screen if the player wants to change careers on this square and the option is available
            if (allowCareerChange)
            {
                screenManager.ChangeAllScreenStates(ScreenState.Paused);
                screenManager.AddScreen(new SetCareerMenuScreen(player, careerManager, 0, player.Career.CareerType));
            }

            //Square is completed!
            return true;
        }
    }
}
