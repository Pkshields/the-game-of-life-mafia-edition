﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameOfLifeMafia
{
    public class UniSquare : Square
    {
        /// <summary>
        /// Store a reference stating if the player has passed the exam or not
        /// </summary>
        private bool examPassed = false;

        /// <summary>
        /// Initialise a copy of UniSquare
        /// </summary>
        /// <param name="boardManager">Manager managing all the squares of the board</param>
        /// <param name="Position">Position to place the square</param>
        /// <param name="NextSquare">Square to move the player to upon next move</param>
        /// <param name="SquareImage">Visual image of the square</param>
        public UniSquare(BoardManager boardManager, Vector2 Position, int NextSquare, Texture2D SquareImage)
        {
            _position = Position;
            _nextSquare = NextSquare;
            this.boardManager = boardManager;
            _alternativeSquare = null;
            _squareImage = SquareImage;
        }

        /// <summary>
        /// Initialise a copy of UniSquare
        /// </summary>
        /// <param name="boardManager">Manager managing all the squares of the board</param>
        /// <param name="Position">Position to place the square</param>
        /// <param name="NextSquare">Square to move the player to upon next move</param>
        /// <param name="AlternativeSquare">Alternative square to move the player to upon next move</param>
        /// <param name="SquareImage">Visual image of the square</param>
        public UniSquare(BoardManager boardManager, Vector2 Position, int NextSquare, int[] AlternativeSquare, Texture2D SquareImage)
        {
            _position = Position;
            _nextSquare = NextSquare;
            this.boardManager = boardManager;
            _alternativeSquare = AlternativeSquare;
            _squareImage = SquareImage;
        }

        /// <summary>
        /// Let the player take the exam
        /// </summary>
        /// <param name="player">Player takign the exam</param>
        /// <returns>Is the square done?</returns>
        public override bool EnterSquareEvent(Player player)
        {
            //Take the exam!
            int result = Spinner.getNumber();

            //Check exam results
            if (result <= 3)
            {
                //FAILED
                examPassed = false;
                forceStopPlayer = true;
                Console.WriteLine("EXAM FAILED, YOU FAILURE. STAY RIGHT HERE.");
            }
            else
            {
                //PASS
                examPassed = true;
                Console.WriteLine("EXAM PASSED, UGH.");
            }

            return true;
        }

        /// <summary>
        /// Override the GetNexSquare so that if the exam wasn't passed it loops aound to this square and takes it again
        /// </summary>
        /// <returns>Square to move towards</returns>
        public override Square GetNextSquare()
        {
            if (examPassed)
                return base.GetNextSquare();
            else
                return this;
        }
    }
}
