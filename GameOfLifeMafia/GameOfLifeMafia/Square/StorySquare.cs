﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameOfLifeMafia
{
    /// <summary>
    /// This is a square on which nothing unusual happens
    /// </summary>
    public class StorySquare : Square
    {
        private StoryEvents _cardDeck;

        public StorySquare()
        {
            _position = new Vector2(50, 50);
            _nextSquare = 0;
            this.boardManager = null;
            _alternativeSquare = null;
            _squareImage = null;

            _testDelay = 120;
        }

        public StorySquare(BoardManager boardManager, Vector2 Position, StoryEvents cardDeck, int NextSquare, Texture2D SquareImage)
        {
            _position = Position;
            _cardDeck = cardDeck;
            _nextSquare = NextSquare;
            this.boardManager = boardManager;
            _alternativeSquare = null;
            _squareImage = SquareImage;

            _testDelay = 120;
        }

        public StorySquare(BoardManager boardManager, Vector2 Position, StoryEvents cardDeck, int NextSquare, int[] AlternativeSquare, Texture2D SquareImage)
        {
            _position = Position;
            _cardDeck = cardDeck;
            _nextSquare = NextSquare;
            this.boardManager = boardManager;
            _alternativeSquare = AlternativeSquare;
            _squareImage = SquareImage;

            _testDelay = 120;
        }

        public override bool LandOnSquareEvent(Player player)
        {
            if (_testDelay > 0)
            {
                _testDelay--;
                return false;
            }
            else
            {

                _cardDeck.ReadEvent(player);

                _testDelay = 120;
                return true;
            }
        }

        public override void DrawSquare(SpriteBatch spriteBatch, Color? color)
        {
            base.DrawSquare(spriteBatch, Color.HotPink);
        }
    }
}
