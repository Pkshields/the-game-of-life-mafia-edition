﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameOfLifeMafia
{
    class TravelSquare : Square
    {
        /// <summary>
        /// This is the square you are trying to move the player too.
        /// </summary>
        private Square _targetSqaure = null;

        /// <summary>
        /// This is the message box to ask the player if he wishes to travel
        /// </summary>
        private SelectionBox _selectionBox;

        /// <summary>
        /// This is ticked if the player should travel
        /// </summary>
        private bool _shouldTravel = false;


        /// <summary>
        /// This is ticked if the player should travel
        /// </summary>
        private bool _shouldTravelToNextSquare = false;

        /// <summary>
        /// This is ticked if the player should travel
        /// </summary>
        private bool _shouldDraw = false;

        private IslandID id;

        public TravelSquare()
        {
            _position = new Vector2(50, 50);
            _nextSquare = 0;
            this.boardManager = null;
            _alternativeSquare = null;
            _squareImage = null;
            _selectionBox = new SelectionBox("Select where you want to travel.", new String[] { "Confirm" });
        }

        public TravelSquare(BoardManager boardManager, Vector2 Position, int NextSquare, Texture2D SquareImage, IslandID id)
        {
            _position = Position;
            _nextSquare = NextSquare;
            this.boardManager = boardManager;
            _alternativeSquare = null;
            _squareImage = SquareImage;

            this.id = id;
            _selectionBox = new SelectionBox("Select where you want to travel.", new String[] { "Confirm" });
        }

        public TravelSquare(BoardManager boardManager, Vector2 Position, int NextSquare, int[] AlternativeSquare, Texture2D SquareImage, IslandID id)
        {
            _position = Position;
            _nextSquare = NextSquare;
            this.boardManager = boardManager;
            _alternativeSquare = AlternativeSquare;
            _squareImage = SquareImage;

            this.id = id;
            _selectionBox = new SelectionBox("Select where you want to travel.", new String[] { "Confirm" });
        }

        public override bool EnterSquareEvent(Player player)
        {
            _shouldDraw = true;

            //Gets the positions of the two squares
            Vector2 SquarePosition = boardManager.GetNextSquare(_nextSquare).GetPosition();

            //Sets the hitboxes for the two squares
            Rectangle SquareHitbox = new Rectangle((int)SquarePosition.X - 7, (int)SquarePosition.Y - 7, 15, 15);

            if (_shouldTravel)
                if (!_shouldTravelToNextSquare)
                {
                    //Checks if the player clicks on the next square and then sets the next square to the corresponding one
                    if ((SquareHitbox.Contains((int)Input.MousePosition.X, (int)Input.MousePosition.Y)) &&
                        (Input.LeftMouseButtonReleased))
                    {
                        _targetSqaure = GetNextSquare();
                    }
                    //Runs through the array and sees wheter any of the squares are being clicked
                    for (int i = 0; i < GetAlternativeArrayLength(); i++)
                    {
                        //Gets the hit box
                        SquarePosition = GetNextAlternativeSquare(i).GetPosition();
                        SquareHitbox = new Rectangle((int)SquarePosition.X - 7, (int)SquarePosition.Y - 7, 15, 15);

                        //Returns the square being clicked
                        if ((SquareHitbox.Contains((int)Input.MousePosition.X, (int)Input.MousePosition.Y)) &&
                            (Input.LeftMouseButtonReleased))
                        {
                            _targetSqaure = GetNextAlternativeSquare(i);
                        }
                    }
                }
                else
                {
                    //player.MoveTowardsSquare(_nextSquare);
                    if (player.IsOnSquare(_targetSqaure))
                    {
                        player.CurrentSquare = _targetSqaure;
                        _targetSqaure = null;
                        _shouldTravel = false;
                        _shouldDraw = false;
                        return true;
                    }
                }
            else
            {
                int box = _selectionBox.Update();

                if (box != 0)
                    _shouldTravel = true;

                if (box == 1)
                {
                    _shouldTravel = false;
                    _shouldDraw = false;
                    return true;
                }
            }
            return false;
        }

        public override void DrawSquare(SpriteBatch spriteBatch, Color? color)
        {
            base.DrawSquare(spriteBatch, color);

            if (!_shouldTravel && _shouldDraw)
                _selectionBox.Draw(spriteBatch);

            _shouldDraw = false;
        }

        public override void DrawSelectionBox(SpriteBatch spriteBatch)
        {
            if (!_shouldTravel && _shouldDraw)
                _selectionBox.Draw(spriteBatch);

            _shouldDraw = false;
        }
    }
}
