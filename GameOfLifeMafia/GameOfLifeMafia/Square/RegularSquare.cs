﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameOfLifeMafia
{
    /// <summary>
    /// This is a square on which nothing unusual happens
    /// </summary>
    public class RegularSquare : Square
    {
        public RegularSquare()
        {
            _position = new Vector2(50, 50);
            _nextSquare = 0;
            _alternativeSquare = null;
            _squareImage = null;

            _testDelay = 120;
        }

        public RegularSquare(BoardManager boardManager, Vector2 Position, int NextSquare, Texture2D SquareImage)
        {
            _position = Position;
            _nextSquare = NextSquare;
            this.boardManager = boardManager;
            _alternativeSquare = null;
            _squareImage = SquareImage;

            _testDelay = 120;
        }

        public RegularSquare(BoardManager boardManager, Vector2 Position, int NextSquare, int[] AlternativeSquare, Texture2D SquareImage)
        {
            _position = Position;
            _nextSquare = NextSquare;
            this.boardManager = boardManager;
            _alternativeSquare = AlternativeSquare;
            _squareImage = SquareImage;

            _testDelay = 120;
        }

        public override bool EnterSquareEvent(Player player)
        {
            return true;
        }

        public override bool LandOnSquareEvent(Player player)
        {
            if (_testDelay > 0)
            {
                _testDelay--;
                return false;
            }
            else
            {
                _testDelay = 120;
                return true;
            }
        }
    }
}
