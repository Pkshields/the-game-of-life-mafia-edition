﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameOfLifeMafia
{
    /// <summary>
    /// This class will be the sample class for all the types of squares
    /// </summary>
    abstract public class Square
    {
        /// <summary>
        /// The texture for the current sprite.
        /// Should be removed in the final version as the squares won't have sprites
        /// </summary>
        protected Texture2D _squareImage;

        /// <summary>
        /// The position of the square on the board
        /// </summary>
        protected Vector2 _position;

        /// <summary>
        /// Current BoardManager managign this square as well as all other squares
        /// </summary>
        protected BoardManager boardManager;

        /// <summary>
        /// The Square that follows this one.
        /// </summary>
        protected int _nextSquare;

        /// <summary>
        /// This is used to store an array of branching paths
        /// </summary>
        protected int[] _alternativeSquare;

        protected bool forceStopPlayer = false;

        /// <summary>
        /// CTF mode: Determines if this square is a flag square
        /// </summary>
        protected bool isFlag;
        public bool IsFlag
        {
            get { return isFlag; }
            set { isFlag = value; }
        }

        virtual public void DrawSelectionBox(SpriteBatch spriteBatch) { }

        protected int _testDelay;

        virtual public bool EnterSquareEvent(Player player) { return true; }
        virtual public bool LandOnSquareEvent(Player player) { return true; }
        virtual public void DrawSquare(SpriteBatch spriteBatch, Color? color)
        {
            //spriteBatch.Draw(_squareImage, new Vector2(_position.X - 5, _position.Y - 5), (color == null ? Color.Pink : (Color)color));
        }

        virtual public Square GetNextSquare()
        {
            return boardManager.GetNextSquare(_nextSquare);
        }

        /// <summary>
        /// Returns the alternative square.
        /// Will return null if there is no alternative square or the index is out of bounds
        /// </summary>
        /// <param name="index">The index of the array</param>
        /// <returns>The Square at the arrays index</returns>
        virtual public Square GetNextAlternativeSquare(int index)
        {
            //Sets the index
            int i = index;

            //Returns null if there is no alternative square value
            if (_alternativeSquare == null)
                return null;

            //Checks that the index is not out of bounds
            else if ((i < 0) || (i >= _alternativeSquare.Length))
                return null;

            //Returns the Square
            else
                return boardManager.GetNextSquare(_alternativeSquare[index]);
        }

        /// <summary>
        /// Gets the length of the alternative array length
        /// </summary>
        /// <returns></returns>
        virtual public int GetAlternativeArrayLength()
        {
            //Returns zero if there is no array
            if (_alternativeSquare == null)
                return 0;
            return _alternativeSquare.Length;
        }

        public Vector2 GetPosition() { return _position; }
        public bool IsForceStop { get { return forceStopPlayer; } }

        public void SetNextSquare(int NextSquare) { _nextSquare = NextSquare; }
    }
}
