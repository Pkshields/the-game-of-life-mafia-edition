﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameOfLifeMafia
{
    /// <summary>
    /// This calls the method getSpinToWin from Spinner class and assigns player money if they win
    /// </summary>
    class SpinToWinSquare : Square
    {

        private int PrizeMoney = 50000; //Amount of money to add to the player who wins
        private string flavorText;      //Text to display to the player on land
        private Player[] eachPlayer;    //array for all players
        private int WinningNumber;      //variable to hold winning number
        //private bool hasWon = false;    //bool to tell loop if someone has won
        private int secondNum;          //the second number for the player who landed on the square

        public SpinToWinSquare(BoardManager boardManager, Vector2 position, int nextSquare, Texture2D squareImage,
            int? moneyChange, string flavourText, Player[] players)
        {
            //Initialise all the default Square value
            _squareImage = squareImage;
            _position = position;
            this.boardManager = boardManager;
            _nextSquare = nextSquare;
            _alternativeSquare = null;
            eachPlayer = players;

            //Initialize the values that FixedEventSquare needs
            this.flavorText = flavourText;
        }

        public SpinToWinSquare(BoardManager boardManager, Vector2 position, int nextSquare, int[] alternativeSquare, Texture2D squareImage,
            int? moneyChange, string flavourText, Player[] players)
        {
            //Initialise all the default Square values
            _squareImage = squareImage;
            _position = position;
            this.boardManager = boardManager;
            _nextSquare = nextSquare;
            _alternativeSquare = alternativeSquare;
            eachPlayer = players;

            //Initialize the values that FixedEventSquare needs
            this.flavorText = flavourText;
        }

        /// <summary>
        /// Assign each player in array a number from 1 to array length
        /// </summary>
        /// <param name="player">pass in current player</param>
        /// <returns></returns>
        public override bool LandOnSquareEvent(Player player)
        {
            //Print out the flavour text to the console
            System.Diagnostics.Debug.WriteLine("Event!: " + flavorText);

            secondNum = 1; //assign player who landed on square an extra number
            System.Diagnostics.Debug.WriteLine("The current player's number is " + secondNum + ".");

            for (int i = 0; i < eachPlayer.Length; i++) //for each player...
            {
                eachPlayer[i].AssignSpinToWin(i + 2); //...assign a number
                System.Diagnostics.Debug.WriteLine("Player " + (i + 1) + "'s number is " + eachPlayer[i]._SpinToWinNo + ".");
            }

            WinningNumber = Spinner.getSpinToWin(1, eachPlayer.Length); //SPIN TO WIN NUMBER
            System.Diagnostics.Debug.WriteLine("Winning number is: " + WinningNumber); //print winning number

            //  while (!hasWon)
            //  { 
            for (int i = 0; i < eachPlayer.Length; i++) //for each player...
            {
                if (eachPlayer[i]._SpinToWinNo == WinningNumber) //...compare their number to the winning number
                {
                    System.Diagnostics.Debug.WriteLine("Player " + (i + 1) + " has won " + flavorText);
                    eachPlayer[i].ChangeMoney(PrizeMoney); //...if its the same then they win. assign money   
                    //hasWon = true;
                    break;
                }
                else if (secondNum == WinningNumber)
                {
                    System.Diagnostics.Debug.WriteLine("The current player has won " + flavorText);
                    player.ChangeMoney(PrizeMoney); //...if its the same then they win. assign money   
                    //hasWon = true;
                    break;
                }
            }
            //   }
            //Square is completed!
            return true;
        }
    }
}
