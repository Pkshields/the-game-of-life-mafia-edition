﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameOfLifeMafia
{
    class PayDaySquare : Square
    {
        public PayDaySquare(BoardManager boardManager, Vector2 position, int nextSquare, Texture2D squareImage)
        {
            //Initialise all the default Square values
            _squareImage = squareImage;
            _position = position;
            this.boardManager = boardManager;
            _nextSquare = nextSquare;
            _alternativeSquare = null;

        }

        public PayDaySquare(BoardManager boardManager, Vector2 position, int nextSquare, int[] alternativeSquare, Texture2D squareImage)
        {
            //Initialise all the default Square values
            _squareImage = squareImage;
            _position = position;
            this.boardManager = boardManager;
            _nextSquare = nextSquare;
            _alternativeSquare = alternativeSquare;

        }

        public override bool EnterSquareEvent(Player player)
        {
            player.GetPaid();
            return true;
        }
    }
}
