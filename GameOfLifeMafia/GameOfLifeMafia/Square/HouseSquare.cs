﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameOfLifeMafia
{
    public class HouseSquare : Square
    {
        /// <summary>
        /// House assigned to this square
        /// </summary>
        private House houseID;

        /// <summary>
        /// Current state of input the square is in
        /// </summary>
        private bool currentState = true;

        /// <summary>
        /// Menu items for buying houses
        /// </summary>
        private MenuItem[] buyMenu = new MenuItem[2];

        /// <summary>
        /// (TEMP?) Font for printing strings
        /// </summary>
        private SpriteFont font;

        /// <summary>
        /// Return the visible rectangle dimensions and position for the square 
        /// Needs optimised...
        /// </summary>
        public Rectangle Rect
        {
            get { return new Rectangle((int)(_position.X - _squareImage.Width / 2), (int)(_position.Y - _squareImage.Height / 2), (_squareImage.Width), (_squareImage.Height)); }
        }

        /// <summary>
        /// Create an instance of the Fixed Event Square (XML only!)
        /// </summary>
        public HouseSquare()
        { }

        /// <summary>
        /// Create an instance of the Fixed Event Square
        /// </summary>
        /// <param name="position">Position to place the square</param>
        /// <param name="nextSquare">Square to move the player to upon next move</param>
        /// <param name="squareImage">Visual image of the square</param>
        /// <param name="houseID">House to assign to this square</param>
        public HouseSquare(BoardManager boardManager, Vector2 position, int nextSquare, Texture2D squareImage, House houseID, Texture2D blankButton, SpriteFont font)
        {
            //Initialise all the default Square values
            _squareImage = squareImage;
            _position = position;
            this.boardManager = boardManager;
            _nextSquare = nextSquare;
            _alternativeSquare = null;

            //Set the house ID of this square
            this.houseID = houseID;

            //Tell the House that it is assigned this square
            houseID.HouseSquareID = this;

            //Generate two (blank) buttons for selling the house
            buyMenu[0] = new MenuItem(blankButton, new Vector2(600, 195), 0);
            buyMenu[1] = new MenuItem(blankButton, new Vector2(600, 295), 0);
            this.font = font;
        }

        /// <summary>
        /// Create an instance of the Fixed Event Square
        /// </summary>
        /// <param name="position">Position to place the square</param>
        /// <param name="nextSquare">Square to move the player to upon next move</param>
        /// <param name="alternativeSquare">Alternative square to move the player to upon next move</param>
        /// <param name="squareImage">Visual image of the square</param>
        /// <param name="houseID">House to assign to this square</param>
        public HouseSquare(BoardManager boardManager, Vector2 position, int nextSquare, int[] alternativeSquare, Texture2D squareImage, House houseID, Texture2D blankButton, SpriteFont font)
        {
            //Initialise all the default Square values
            _squareImage = squareImage;
            _position = position;
            this.boardManager = boardManager;
            _nextSquare = nextSquare;
            _alternativeSquare = alternativeSquare;

            //Set the house ID of this square
            this.houseID = houseID;

            //Generate two (blank) buttons for selling the house
            buyMenu[0] = new MenuItem(blankButton, new Vector2(600, 195), 0);
            buyMenu[1] = new MenuItem(blankButton, new Vector2(600, 295), 0);
            this.font = font;
        }

        /// <summary>
        /// Method to run upon player landing on square
        /// </summary>
        /// <param name="player">Current active player</param>
        /// <returns>UI - Is the game ready to move to the next stage?</returns>
        public override bool EnterSquareEvent(Player player)
        {
            //If the house is not already bought
            if (houseID.PlayerID == null)
            {
                //Check to see what state the square is in
                //State "square just entered, ask user do they want to buy the house"
                if (currentState)
                {
                    //Ask user do they want to buy the house
                    Console.WriteLine("Do you want to buy this house for " + houseID.HouseValue + "SSDs? (y for yes, n for no)");

                    //Reset the two buttons
                    buyMenu[0].ResetButtonClicked();
                    buyMenu[1].ResetButtonClicked();

                    //Set the state to wait for user input on answer
                    currentState = false;
                }
                //State "Waiting for the user to grace us with their response"
                else
                {
                    //If yes, then sell them the house, taking the price from their cash pile
                    if (buyMenu[0].UpdateMenuItem())
                    {
                        //Take the money from the player
                        player.ChangeMoney(-houseID.HouseValue);

                        //Give them ownership of the house
                        houseID.PlayerID = player;

                        //Give the house to player too
                        player.AddHouse(houseID);

                        //Set the game to continue on as normal
                        currentState = true;

                        //House is bought, force stop on this square
                        forceStopPlayer = true;

                        //Print it out to console that the house has been bought
                        Console.WriteLine("Player has bought house... Something! It's worth " + houseID.HouseValue + " Solid Shiny Doubloons (SSDs)!");
                    }
                    else if (buyMenu[1].UpdateMenuItem())
                    {
                        //Tell the player they didn't buy the house
                        Console.WriteLine("You did not buy this house!");

                        //Set the game to continue on as normal
                        currentState = true;
                    }
                }

                //Tell the game what state we are currently in
                return currentState;
            }

            //No action is to be taken, tell the game to continue
            currentState = true;
            forceStopPlayer = false;
            return currentState;
        }

        /// <summary>
        /// Draw both the square itself as well as the UI elements for buying houses
        /// </summary>
        /// <param name="spriteBatch"></param>
        override public void DrawSquare(SpriteBatch spriteBatch, Color? color)
        {
            //Draw the square itself
            //spriteBatch.Draw(_squareImage, new Vector2(_position.X - 5, _position.Y - 5), (color == null ? Color.Pink : (Color)color));

            //If we are on buying house stage
            if (!currentState)
            {
                //Draw the two blank MenuItems
                foreach (var menuItem in buyMenu)
                {
                    menuItem.DrawMenuItem(spriteBatch);
                }

                //Draw the string elements for the buttons
                spriteBatch.DrawString(font, ParseText("Buy House " + houseID.HouseName + "?", 180), buyMenu[0].Position + new Vector2(10, 10), Color.Red);
                spriteBatch.DrawString(font, ParseText("No.", 180), buyMenu[1].Position + new Vector2(10, 10), Color.Red);
            }
        }

        /*****************************************************************************************************/
        /*                          Taken from what I have used before, probably temp                        */

        /// <summary>
        /// Parses the text. If the text if greater than the given width, add a line break
        /// Code taken and adapted from: http://danieltian.wordpress.com/2008/12/24/xna-tutorial-typewriter-text-box-with-proper-word-wrapping-part-2/
        /// </summary>
        /// <param name="text">String of text to be parsed</param>
        /// <returns></returns>
        private String ParseText(String text, float width)
        {
            String line = String.Empty;
            String returnString = String.Empty;
            String[] wordArray = text.Split(' ');

            foreach (String word in wordArray)
            {
                if (font.MeasureString(line + word).Length() > width)
                {
                    returnString = returnString + line + '\n';
                    line = String.Empty;
                }

                line = line + word + ' ';
            }

            return returnString + line;
        }

        /*****************************************************************************************************/
    }
}
