using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using System.Xml.Serialization;
using System.IO;
using MuScreenManager;
using  System.Xml.Linq;

namespace GameOfLifeMafia
{
    /// <summary>
    /// An Enum Variable to decide what stage of the game it is.
    /// </summary>
    public enum GameStage
    {
        SetUp = 0,
        PlayerSpin = 1,
        PlayerSelectSquare = 2,
        PlayerMove = 3,
        PlayerEnterSquare = 4,
        PlayerLandOnSquare = 5,
        NextPlayer = 6,
        SellHouse = 7
    };

    /// <summary>
    /// An Enum variable to decide what the current colour is
    /// </summary>
    public enum Colour
    {
        Red = 1,
        Black = 2
    };

    /// <summary>
    /// Variable indicating then game mode for this game
    /// </summary>
    public enum GameMode
    {
        Classic = 0, 
        LandJump = 1, 
        CaptureTheFlag = 2
    };

    /// <summary>
    /// This is the main type for your game
    /// </summary>
    [XmlRootAttribute("PlayersStats", Namespace = "", IsNullable = false)]
    public class BoardGame : Screen
    {
        Texture2D spriteFont;
        private Texture2D spinnerImage;

        private BoardManager boardManager;
        private CareerManager careerManager;
        Texture2D boardTexture, TimeLine, EndMarker, CurrentMarker, colourBox, colourBoxColour, numberBox;
        //int screenWidth;
        //int screenHeight;

        Player[] ListOfPlayers;
        int CurrentPlayer = 0;
        int startingMoney;
        GameMode gameMode;

        /// <summary>
        /// Current and End Year information
        /// </summary>
        int currentYear = 0;
        int endYear;

        /// <summary>
        /// EndGameScreen Data
        /// </summary>
        int i;
        List<EndGameMenuScreen> endGameScreen;
        int j;
        //int savenum;

        MenuItem[] MenuItems = new MenuItem[2];

        MenuItem[] GetLoanButtons = new MenuItem[1];

        private List<Career> _careerList
        {
            get { return careerManager.CareerList; }
        }

        private StoryEvents cardDeck;

        /// <summary>
        /// CTF Variables
        /// </summary>
        private int playerWithFlag = -1;
        private int squareWithFlag = -1;
        private int homeSquare = -1;
        private Texture2D flagTexture;

        //MOVEMENT
        GameStage CurrentStage = GameStage.SetUp;       //TEMP: Should call something that allows for "End Turn" check
                                                        //and HouseCount check for obvious reasons
                                                        //REMEMBER: Used to be "NextPlayer" by default
        public static int NumberOfMoves = Spinner.getNumber(); //calls getNumber method from Spinner Class
        int MovesTotal;

        //COLOURS
        int colourNum;          //holds value when spinner class is called
        //string colourStr;       //holds text value of current colour value
        Colour currentColour;   //create instance of enum Colour

        //If we are on "selling house", keep the current GameStage so we can go back to it after the selling is done
        GameStage storedStage;

        /// <summary>
        /// Goes between 0 and 120. To show at which point of the animation it is.
        /// </summary>
        //Texture2D PlayerImage;

        PlayerHUD[] playerHuds;

        int SelectionDrawState = 1;

        /// <summary>
        /// List of the players stats
        /// </summary>
        /// <param name="numOfPlayer"></param>
        /// <param name="yearEnd"></param>
        [XmlArray("playersStatistics"),XmlArrayItem("stat", typeof(int))]
        List<PlayerStats> PlayerStats;

        Music theSong;
        
        public BoardGame(GameMode gameMode, int numOfPlayer, int yearEnd, int startingMoney)
        {
            //Initialize the objects in need of settings from last screen
            this.gameMode = gameMode;
            ListOfPlayers = new Player[numOfPlayer];
            playerHuds = new PlayerHUD[numOfPlayer];
            endYear = yearEnd;
            MovesTotal = NumberOfMoves;
            colourNum = Spinner.getColour(MovesTotal);
            currentColour = (Colour)colourNum;
            this.startingMoney = startingMoney;
            cardDeck = new StoryEvents(ListOfPlayers, currentColour, CurrentPlayer, boardManager);
            //savenum = 0;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        public override void LoadContent()
        {
            //Play music!
            theSong = screenManager.Audio.GetMusic("Music", "TheSong");
            theSong.Play();

            boardTexture = content.Load<Texture2D>("MSM/board2");
            spinnerImage = content.Load<Texture2D>("MSM/spinner");
            Texture2D BoardImage = content.Load<Texture2D>("Images/square");
            Texture2D PlayerImage = content.Load<Texture2D>("Images/peg");
            //buttons
            Texture2D EndTurnImage = content.Load<Texture2D>("Images/end-turn-button");
            Texture2D SellHouseImage = content.Load<Texture2D>("Images/sellhouse-button");
            Texture2D GetLoanImage = content.Load<Texture2D>("Images/getloan-button");
            Texture2D BlankButton = content.Load<Texture2D>("Images/b_blank");
            Texture2D playHUD = content.Load<Texture2D>("Images/PlayerHUD");

            //timeline items
            TimeLine = content.Load<Texture2D>("Images/timeline");
            EndMarker = content.Load<Texture2D>("Images/end-marker");
            CurrentMarker = content.Load<Texture2D>("Images/current-marker");
            //fonts
            SpriteFont font = content.Load<SpriteFont>("HellaFont");
            spriteFont = content.Load<Texture2D>("Images/testFont"); //load Font
            GlobalVariables.spriteFont = spriteFont;


            Texture2D GUI = content.Load<Texture2D>("Images/GUI");
            SelectionBox.SetGraphics(GUI, font);

            //colour and number
            colourBox = content.Load<Texture2D>("Images/colourBox");
            colourBoxColour = content.Load<Texture2D>("Images/colourBox");
            numberBox = content.Load<Texture2D>("Images/numberBox");
            //CTF
            flagTexture = content.Load<Texture2D>("Images/money-bag");

            //Generate the list of houses for use in the house square
            //NOTE TO SELF: Switch this for Content parsing with XNA XML file
            House[] listOHouses = House.LoadHouses();

            careerManager = new CareerManager(screenManager);

            boardManager = new BoardManager(BoardImage, cardDeck, listOHouses, ListOfPlayers, careerManager, BlankButton, font, screenManager);

            /*
            for (int i = 0; i < ListOfSquares.Length; i++)
            {
                if (i == 0)
                    ListOfSquares[i] = new RegularSquare(new Vector2(0, 0), null, BoardImage);
                else
                    ListOfSquares[i] = new RegularSquare(new Vector2(50 * i, 50 * i), ListOfSquares[i-1], BoardImage);
            }
            ListOfSquares[0].SetNextSquare(ListOfSquares[9]);
            */

            //creating new menuitem object with image and position and menuitem enum type (click,hover etc)
            MenuItems[0] = new MenuItem(EndTurnImage, new Vector2(880, 690), 0);
            MenuItems[1] = new MenuItem(SellHouseImage, new Vector2(845, 5), 0);

            //creating new get Loan buttons according to player number
            for (int i = 0; i < GetLoanButtons.Length; i++)
            { 
                GetLoanButtons[i] = new MenuItem(GetLoanImage, new Vector2(930 + ((i) * GetLoanImage.Width ), 5), 0);            
            }


            CurrentPlayer = 0;

            for (int i = 0; i < ListOfPlayers.Length; i++)
            {
                ListOfPlayers[i] = new Player(PlayerImage, boardManager.GetNextSquare(0));
                ListOfPlayers[i].ChangeMoney(startingMoney);
            }

            //initialises a new list of player stats, which can be used for loading saving later on
            PlayerStats = new List<PlayerStats>();
            //used to cycle through endgame menu screens
            j = ListOfPlayers.Length-1;

            //this.screenState = ScreenState.Paused;
            //screenManager.AddScreen(new CareerMenuScreen(ListOfPlayers, _careerList, 0, this));

            for (int i = 0; i < ListOfPlayers.Length; i++)
                playerHuds[i] = new PlayerHUD(new Vector2(512 - (125 * (ListOfPlayers.Length - 1)) + (250 * i), 0), playHUD, ListOfPlayers[i]);

            for (int i = 0; i < ListOfPlayers.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        ListOfPlayers[i].Color = new Color(255, 255, 125);
                        break;
                    case 1:
                        ListOfPlayers[i].Color = new Color(255, 125, 255);
                        break;
                    case 2:
                        ListOfPlayers[i].Color = new Color(255, 125, 125);
                        break;
                    case 3:
                        ListOfPlayers[i].Color = new Color(125, 255, 255);
                        break;
                }
            }
        }

        public void AfterCareerSetup(Player[] ListOfPlayers)
        {
            this.ListOfPlayers = ListOfPlayers;
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            //Updates the Input class
            Input.Update();

            //TEMP: temp pause screen, using the slightly different Input from MSM for keyboard control
            if (screenManager.Input.IsKeyPressed(Keys.P))
            {
                ConfirmMessageScreen confirmScreen = new ConfirmMessageScreen("Pause");
                confirmScreen.OnConfirm += new EventHandler<EventArgs>(confirmScreen_OnConfirm);
                confirmScreen.OnSave += new EventHandler<EventArgs>(confirmScreen_OnSave);
                screenManager.AddScreen(confirmScreen);
            }

            //TAKE OUT LOAN
            //allows the player to take out a loan
            foreach (var loanitem in GetLoanButtons)
            {
                loanitem.UpdateMenuItem();
                if ((loanitem.UpdateMenuItem()) && (CurrentStage != GameStage.PlayerSelectSquare))
                {
                    ListOfPlayers[CurrentPlayer].GetLoan();
                    //will give current player one loan and disable button
                    loanitem.ResetButtonClicked();
                    loanitem.IsEnabled = false;
                }                  
            }

            //SELL HOUSE
            //Check if the player wants to sell a house
            if (MenuItems[1].UpdateMenuItem())
            {
                if (CurrentStage != GameStage.SellHouse)
                {
                    //Set the game to sell house time
                    storedStage = CurrentStage;
                    CurrentStage = GameStage.SellHouse;
                }
                else
                {
                    //Selling of house cancelled, return to gameplay
                    CurrentStage = storedStage;
                }

                //Nomatter what, button has been clicked, reset it
                MenuItems[1].ResetButtonClicked();
            }

            if (CurrentStage == GameStage.SetUp)
            {
                if (careerManager.Update(ref ListOfPlayers))
                {
                    CurrentStage = GameStage.NextPlayer;

                    foreach (var player in ListOfPlayers)
                        if (player.Career == null)
                            player.SetCurrentSquare(boardManager.GetNextSquare(13)); //set position of good guy

                    //Set up the CTF mode
                    //Add the flag to a square within the 4 main islands
                    if (gameMode == GameMode.CaptureTheFlag)
                    {
                        squareWithFlag = 25;//new Random().Next(firstSquare, lastSquare);
                        boardManager.GetNextSquare(squareWithFlag).IsFlag = true;
                    }
                }
            }
            else if (CurrentStage == GameStage.PlayerSelectSquare)
            {
                if (ListOfPlayers[CurrentPlayer].SetNextSquare())
                    CurrentStage = GameStage.PlayerMove;
            }
            else if (CurrentStage == GameStage.PlayerMove)
            {
                if (ListOfPlayers[CurrentPlayer].IsOnCurrentSquare())
                {
                    CurrentStage = GameStage.PlayerEnterSquare;
                    NumberOfMoves--;
                }
                else
                {
                    //only move the player if button boolean is true
                    ListOfPlayers[CurrentPlayer].MoveTowardsCurrentSquare();
                }
            }
            else if (CurrentStage == GameStage.PlayerEnterSquare)
            {
                if (ListOfPlayers[CurrentPlayer].EnterSquareEvent())
                {
                    if (NumberOfMoves == 0)
                        CurrentStage = GameStage.PlayerLandOnSquare;
                    else
                    {
                        if (ListOfPlayers[CurrentPlayer].CurrentSquare.IsForceStop)
                        {
                            NumberOfMoves = 0;
                            CurrentStage = GameStage.PlayerLandOnSquare;
                        }
                        else
                        {
                            CurrentStage = GameStage.PlayerSelectSquare;
                            //ListOfPlayers[CurrentPlayer].SetNextSquare();
                        }
                    }

                    //CTF mode - check if the player has landed on the flag square
                    if (gameMode == GameMode.CaptureTheFlag)
                    {
                        //If the current player is on the square with the flag, give them it
                        if (ListOfPlayers[CurrentPlayer].CurrentSquare.IsFlag)
                        {
                            ListOfPlayers[CurrentPlayer].CurrentSquare.IsFlag = false;
                            playerWithFlag = CurrentPlayer;
                            squareWithFlag = -1;

                            int firstSquare = 23, lastSquare = 122;
                            homeSquare = new Random().Next(firstSquare, lastSquare);
                        }
                        //If a player has the flag and the current player just overtook the player with the flag, give the current player the flag
                        else if (playerWithFlag != -1 && ListOfPlayers[playerWithFlag].CurrentSquare == ListOfPlayers[CurrentPlayer].CurrentSquare)
                        {
                            playerWithFlag = CurrentPlayer;
                        }
                        //If the current player (the player with the flag) hits the square with the home logo, give them a point and reset the game
                        if (CurrentPlayer == playerWithFlag && homeSquare != -1 && boardManager.GetNextSquare(homeSquare) == ListOfPlayers[CurrentPlayer].CurrentSquare)
                        {
                            //Add point!
                            ListOfPlayers[CurrentPlayer].CTFScore++;

                            //Reset game!
                            playerWithFlag = -1;
                            homeSquare = -1;

                            int firstSquare = 23, lastSquare = 122;
                            squareWithFlag = new Random().Next(firstSquare, lastSquare);
                            boardManager.GetNextSquare(squareWithFlag).IsFlag = true;
                        }
                    }
                }
            }
            else if (CurrentStage == GameStage.PlayerLandOnSquare)
            {
                if (ListOfPlayers[CurrentPlayer].LandOnSquareEvent())
                {
                    CurrentStage = GameStage.NextPlayer;

                    //when a player lands on their destination square reset all loan buttons to stop taking out loans all the time
                    foreach (var loanitem in GetLoanButtons)
                    {
                        loanitem.IsEnabled = true;
                    }
                    //ListOfPlayers[CurrentPlayer].SetNextSquare();
                }
            }
            else if (CurrentStage == GameStage.NextPlayer)
            {
                //Resets End Turn button
                MenuItems[0].ResetButtonClicked();

                //Check if the end turn button has been pressed
                //Moved to here so the player has a chance to sell houses etc. before the end of their turn
                //Before player was changed at the end of movement, before the end turn button had been hit
                if (MenuItems[0].UpdateMenuItem())
                {
                    //Check if the game has to end because of the player having all the land stamps
                    if (gameMode == GameMode.LandJump)
                    {
                        //If the player has all the stamps in this mode, then end the game
                        if (ListOfPlayers[CurrentPlayer].GotAllStamps)
                            RunEndGame();
                    }
                    else if (gameMode == GameMode.CaptureTheFlag)
                    {
                        //If the player has collected the flag and returned it home 3 or more times then end the game
                        if (ListOfPlayers[CurrentPlayer].CTFScore >= 3)
                            RunEndGame();
                    }

                    //FROM PAUL: Moved this to NextPlayer because it seemed to make more sense to me. EndGame should probably go here too. 
                    //If theres a problem with this come scream at me, let me know.
                    NumberOfMoves = Spinner.getNumber(); //when player lands on sqaure assign next value
                    MovesTotal = NumberOfMoves;

                    colourNum = Spinner.getColour(MovesTotal); //get colour based on number obtained
                    if (colourNum == 1) //if value returned is 1 it is red
                    {
                        currentColour = Colour.Red;
                        //colourStr = "Red";
                    }
                    else //if value returned is 2 it is black
                    {
                        currentColour = Colour.Black;
                        //colourStr = "Black";
                    }

                    //Check if the year needs to advance
                    if (NumberOfMoves == 10)
                    {
                        //Advance years!
                        currentYear += 10;

                        //Check if the game now needs to end and creates an endmenu screen object with all the players stats
                        if (currentYear == endYear && gameMode == GameMode.Classic)
                        {
                            //Moved the EndGame code to a separate method so it doesn't need to be duplicated above
                            RunEndGame();
                        }
                    }
                    //Check if the board needs to rotate
                    else if (NumberOfMoves == 1)
                    {
                        //Tell the BoardManager to rotate the board
                        boardManager.RotateBoard();

                        //VOLCANO
                        //Give hella money to the player that spun the 1
                        //TODO: UI needed for this
                        ListOfPlayers[CurrentPlayer].ChangeMoney(Spinner.getNumber() * 10);
                    }

                    CurrentPlayer++;
                    if (CurrentPlayer >= ListOfPlayers.Length)
                        CurrentPlayer = 0;

                    cardDeck.Update(currentColour, CurrentPlayer);

                    CurrentStage = GameStage.PlayerSelectSquare;

                    //If the player doesn't own any houses, disable the "Sell Houses" button
                    if (ListOfPlayers[CurrentPlayer].HouseCount == 0)
                        MenuItems[1].IsEnabled = false;
                    else
                        MenuItems[1].IsEnabled = true;
                }
            }
            //Sell house time!
            else if (CurrentStage == GameStage.SellHouse)
            {
                //If a house is sold, go back to previous stage & reset button
                if (ListOfPlayers[CurrentPlayer].HouseSold())
                {
                    CurrentStage = storedStage;
                    MenuItems[1].ResetButtonClicked();

                    //Check if we should disable the sell house button or not
                    if (ListOfPlayers[CurrentPlayer].HouseCount == 0)
                        MenuItems[1].IsEnabled = false;
                    else
                        MenuItems[1].IsEnabled = true;
                }
            }

            base.Update(gameTime);
        }

      //  public void DrawMenu(SpriteBatch spriteBatch)
    //    {
        //  spriteBatch.Draw(_playerImage, new Vector2(_position.X - 5, _position.Y - 5), Color.White);
    //    }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            //draw background board
            Rectangle screen = new Rectangle(0, 0, 1024, 768);
            spriteBatch.Draw(boardTexture, screen, Color.White);

            ListOfPlayers[CurrentPlayer].DrawSelectionSquare(spriteBatch, SelectionDrawState);


            //Draws all the players);
            for (int i = 0; i < ListOfPlayers.Length; i++)
                ListOfPlayers[i].DrawPlayer(spriteBatch);

            //Draws all the squares
            boardManager.Draw(spriteBatch);

            //Draws the glowing selection for the Squares
            SelectionDrawState++;
            if (SelectionDrawState == 30)
                SelectionDrawState = 1;

            //timeline
            spriteBatch.Draw(TimeLine, new Rectangle(215, 681, 655, 81), Color.Gray);
            if (gameMode == GameMode.Classic)
            {
                spriteBatch.Draw(CurrentMarker, new Rectangle((246 + ((currentYear / 10) * 56)), 695, 30, 40), Color.White);
                spriteBatch.Draw(EndMarker, new Rectangle((246 + ((endYear / 10) * 56)), 695, 30, 40), Color.White);
            }

            //colourBox);
            if(colourNum == 2)
                spriteBatch.Draw(colourBoxColour, new Rectangle(120, 681, 80, 85), Color.Black);
            else if(colourNum == 1)
                spriteBatch.Draw(colourBoxColour, new Rectangle(120, 681, 80, 85), Color.Red);
            spriteBatch.Draw(colourBox, new Rectangle(120, 681, 80, 85), Color.Gray);
           
            //number box
            spriteBatch.Draw(numberBox, new Rectangle(13, 681, 120, 85), Color.Gray);
            SpriteText.drawSpriteText("" + NumberOfMoves, spriteFont, new Vector2(105, 690), new Vector2(8, 8), 8, align.right, spriteBatch, Color.YellowGreen); //draws number of moves left

            // Draws num of houses
            //SpriteText.drawSpriteText("House Num: " + ListOfPlayers[CurrentPlayer].HouseCount, spriteFont, new Vector2(775, 5), new Vector2(8, 8), 1, align.right, spriteBatch, Color.PowderBlue);

            if (CurrentStage == GameStage.PlayerSelectSquare)
            {
                ListOfPlayers[CurrentPlayer].DrawSelectionSquare(spriteBatch, SelectionDrawState);
            }
            else if (CurrentStage == GameStage.SellHouse)
            {
                ListOfPlayers[CurrentPlayer].DrawSellHouseAnimation(spriteBatch, spriteFont, SelectionDrawState);
            }

            foreach (var item in MenuItems)
                item.DrawMenuItem(spriteBatch);

            foreach (var loanitem in GetLoanButtons)
                loanitem.DrawMenuItem(spriteBatch);

            if (gameMode == GameMode.CaptureTheFlag)
            {
                if (squareWithFlag != -1)
                    spriteBatch.Draw(flagTexture, boardManager.GetNextSquare(squareWithFlag).GetPosition() - new Vector2(5, 5), Color.White);
                if (playerWithFlag != -1)
                    spriteBatch.Draw(flagTexture, ListOfPlayers[playerWithFlag].Position + new Vector2(0, 20), Color.White);
                if (homeSquare != -1)
                    spriteBatch.Draw(flagTexture, boardManager.GetNextSquare(homeSquare).GetPosition() - new Vector2(5, 5), Color.White);
            }

            
            drawHUD(spriteBatch);

            //Draws all the squares
            //boardManager.Draw(spriteBatch);
        }

        private void drawHUD(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < playerHuds.Length; i++)
                playerHuds[i].Draw(spriteBatch, i);
        }

        public void RunEndGame()
        {
            //create a new list of endmenuscreens which will be navigated at the end of the game
            endGameScreen = new List<EndGameMenuScreen>();

            for (i = 0; i < ListOfPlayers.Length; i++)
            {
                //create a new Playerstats object, will also be used for saving the game
                PlayerStats.Add(new PlayerStats(new List<int> { endYear, ListOfPlayers[i].HouseCount, ListOfPlayers[i].MoneyCount }));

                //adds DrawableString text to stats for drawing in the EndMenuScreens
                endGameScreen.Add(new EndGameMenuScreen("Game Over Player " + (i + 1), PlayerStats.ElementAt(i).addDrawableStringText()));

                //adds eventhandlers to the 3 menuitems
                endGameScreen.ElementAt(i).OnConfirm_PreviousPlayersStats += new EventHandler<EventArgs>(OnConfirm_PreviousPlayersStats);
                endGameScreen.ElementAt(i).OnConfirm_NextPlayersStats += new EventHandler<EventArgs>(OnConfirm_NextPlayersStats);
                endGameScreen.ElementAt(i).OnConfirm += new EventHandler<EventArgs>(confirmScreen_OnConfirm);

            }
            //add first screen to display
            screenManager.AddScreen(endGameScreen.ElementAt(i - 1));
        }

        /// <summary>
        /// Event handler for the previous screen of player stats at the end of the game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnConfirm_PreviousPlayersStats(object sender, EventArgs e)
        {
            //as long we haven't reached the lower bound of the list of endmenuscreens remove current and add previous
            if (j != 0)
            {
                screenManager.RemoveScreen(endGameScreen.ElementAt(j));
                screenManager.AddScreen(endGameScreen.ElementAt(j -= 1));
            }
            //else bring the player back to the main menu
            else
            {
                theSong.Stop();

                screenManager.RemoveAllScreens();
                screenManager.AddScreen(new BackgroundScreen("MSM/title", true));
                screenManager.AddScreen(new MainMenu());
            }
        }
        /// <summary>
        /// Event handler for the next screen of player stats at the end of the game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void OnConfirm_NextPlayersStats(object sender, EventArgs e)
        {
            //as long we haven't reached the upper bound of the list of endmenuscreens remove current and add previous
            if (j != ListOfPlayers.Length - 1)
            {
                screenManager.RemoveScreen(endGameScreen.ElementAt(j));
                screenManager.AddScreen(endGameScreen.ElementAt(j += 1));
            }
            //else bring the player back to the main menu
            else
            {
                theSong.Stop();

                screenManager.RemoveAllScreens();
                screenManager.AddScreen(new BackgroundScreen("MSM/title", true));
                screenManager.AddScreen(new MainMenu());
            }
        }
        /// <summary>
        /// Event handler for the pause screen. Remove this screen and add the main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void confirmScreen_OnConfirm(object sender, EventArgs e)
        {
            theSong.Stop();

            screenManager.RemoveScreen(this);
            screenManager.AddScreen(new BackgroundScreen("MSM/title", true));
            screenManager.AddScreen(new MainMenu());
        }
        /// <summary>
        /// Saves the players game stats into a xml file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void confirmScreen_OnSave(object sender, EventArgs e)
        {
            for (int j = 0; j < ListOfPlayers.Length; j++)
            {
                //create a new Playerstats object, will also be used for saving the game
                PlayerStats.Add(new PlayerStats(new List<int> { endYear, ListOfPlayers[j].HouseCount, ListOfPlayers[j].MoneyCount }));
            }

              XElement root = new XElement("Root",
                new XElement("Child", PlayerStats)

             );
            root.Save("Root.xml");
            string str = File.ReadAllText("Root.xml");
            Console.WriteLine(str);

            int whiteSpaceNodes;
            XElement xmlTree2 = XElement.Load("Root.xml",
                LoadOptions.None);
            whiteSpaceNodes = xmlTree2
                .DescendantNodesAndSelf()
                .OfType<XText>()
                .Where(tNode => tNode.ToString().Trim().Length == 0)
                .Count();
            Console.WriteLine("Count of white space nodes (not preserving whitespace): {0}", whiteSpaceNodes);
            
        }
    }
}
