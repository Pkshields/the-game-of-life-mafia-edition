﻿#region FILE DESCRIPTION
//-----------------------------------------------------------------------------
// MuScreenManager
// 
// EndGameMenuScreen.cs
//-----------------------------------------------------------------------------
#endregion

#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion USING

namespace MuScreenManager
{
    /// <summary>
    /// Class to show a confirmation message using the existing MenuScreen, with a few extra tweaks
    /// </summary>
    public class LoadGameMenuScreen : MenuScreen
    {
        #region PROPERTIES

        /// <summary>
        /// Provides a white pixel we can manipulate and draw from
        /// </summary>
        Texture2D whitePixel;

        /// <summary>
        /// Size of the rect to draw for th ebackground of this screen
        /// </summary>
        Rectangle drawRect;

        /// <summary>
        /// Holds a list of strings passed in to create drawable strings
        /// </summary>
        List<string> _saveGames;


        #endregion PROPERTIES

        #region EVENTS

        /// <summary>
        /// Event to run upon confirming screen
        /// </summary>
        public event EventHandler<EventArgs> OnConfirm;

        /// <summary>
        /// Event to load stuff from xml files
        /// </summary>
        //public event EventHandler<EventArgs> OnLoad;

        #endregion EVENTS

        #region METHODS


        /// overided constructor allows a list of savegames to be drawn onscreen
        /// </summary>
        /// <param name="confirmText"></param>
        /// <param name="stat"></param>
        public LoadGameMenuScreen(string TitleText, List<string> saveGames)
            : base(TitleText)
        {
            //initialise stats list
            _saveGames = new List<string>();
            _saveGames = saveGames;
        }

        /// <summary>
        /// returns a list of savegamenames and places dummy ones there if there are no save files
        /// </summary>
        /// <returns></returns>
        public void FillSaveGameNames()
        {
            for (int i = 0; i < 3; i++)
            {                
                if (_saveGames.Count() != 3)
                    _saveGames.Add("Empty Save Slot");
            }
        }
        /// <summary>
        /// Initialize the menu components of screen and pause all other screens
        /// </summary>
        public override void Initialize()
        {
            //Pause all screens other than this
            screenManager.ChangeAllScreenStates(ScreenState.Paused, this);

            //Get the size of the screen and store as rectangle
            drawRect = new Rectangle(0, 0, (int)screenManager.ScreenDimensions.X, (int)screenManager.ScreenDimensions.Y);

            FillSaveGameNames();
            //Create the MenuItems to navigate back and forth
            MenuItem item1 = new MenuItem(_saveGames.ElementAt(0), new Vector2((screenManager.ScreenDimensions.X / 2), 275), screenManager);
            MenuItem item2 = new MenuItem(_saveGames.ElementAt(1), new Vector2((screenManager.ScreenDimensions.X / 2), 325), screenManager);
            MenuItem item3 = new MenuItem(_saveGames.ElementAt(2), new Vector2((screenManager.ScreenDimensions.X / 2), 375), screenManager);
            MenuItem item4 = new MenuItem("Return to Main Menu", new Vector2((screenManager.ScreenDimensions.X * 0.5f), 650), screenManager);
            
            //Add the event handlers
            item1.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(game_OnLoad);
            item2.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(game_OnLoad);
            item3.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(game_OnLoad);
            item4.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(OnConfirm);

            //Add to screen
            AddItem(item1);
            AddItem(item2);
            AddItem(item3);
            AddItem(item4);
        }

        /// <summary>
        /// Load the 1 pixel sprite
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            whitePixel = new Texture2D(screenManager.GraphicsDevice, 1, 1);
            whitePixel.SetData(new[] { Color.White });
        }

        /// <summary>
        /// Update MenuScreen items as well as check for additional cancel inputs
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        public override void Update(GameTime gameTime)
        {
            //Update the MenuScreen first
            base.Update(gameTime);

            //Check all gamepads for calcennation buttons
            for (int i = 0; i < screenManager.Input.NumOfGamepad; i++)
            {
                if (screenManager.Input.IsButtonPressed(Buttons.B, (PlayerIndex)i))
                    CloseScreen();
            }

            //Check the keyboard for cancellation buttons
            if (screenManager.Input.IsKeyPressed(Keys.Escape) || screenManager.Input.IsKeyPressed(Keys.P))
                CloseScreen();
        }

        /// <summary>
        /// Draw all menu items as well as additional background
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw these objects on screen</param>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            //Draw the translucent background
            spriteBatch.Draw(whitePixel, drawRect, Color.Black * 0.6f);

            //Draw the rest of MenuItem
            base.Draw(spriteBatch, gameTime);
        }

        /// <summary>
        /// Method to unpause all screens and remove this one
        /// </summary>
        private void CloseScreen()
        {
            screenManager.ChangeAllScreenStates(ScreenState.Active, this);
            screenManager.RemoveScreen(this);
        }

        /// <summary>
        /// Event method to run confirmation event code
        /// </summary>
        /// <param name="sender">Caller of this event</param>
        /// <param name="e">Event params</param>
        private void game_OnLoad(object sender, MenuObjectEventArgs e)
        {
            if (OnConfirm != null)
            {
                screenManager.ChangeAllScreenStates(ScreenState.Active, this);
                OnConfirm(this, new EventArgs());
                screenManager.RemoveScreen(this);
            }
        }

        /// <summary>
        /// Event method to run confirmation event code
        /// </summary>
        /// <param name="sender">Caller of this event</param>
        /// <param name="e">Event params</param>
        private void item3_OnButtonSelect(object sender, MenuObjectEventArgs e)
        {
            if (OnConfirm != null)
            {
                screenManager.ChangeAllScreenStates(ScreenState.Active, this);
                OnConfirm(this, new EventArgs());
                screenManager.RemoveScreen(this);
            }
        }

        #endregion METHODS

    }
}


