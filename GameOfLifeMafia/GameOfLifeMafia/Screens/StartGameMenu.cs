﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MuScreenManager;

namespace GameOfLifeMafia
{
    class StartGameMenu : MenuScreen
    {
        List<MenuOption> tempOptions = new List<MenuOption>();
        Music menuMusic;

        public StartGameMenu(Music menuMusic)
        {
            this.menuMusic = menuMusic;
        }

        /// <summary>
        /// Initialise the MenuItems components of the menu
        /// </summary>
        public override void Initialize()
        {
            //Initialize the MenuItems using the MenuItem from MSM
            MenuOption option1 = new MenuOption("Game Mode", new Vector2(screenManager.ScreenDimensions.X / 2, 275), new string[] { "Classic", "Land Jump", "Capture The Flag" }, new Vector2(700, 275), screenManager);
            MenuOption option2 = new MenuOption("No. Of Players", new Vector2(screenManager.ScreenDimensions.X / 2, 325), new string[] { "2", "3", "4" }, new Vector2(700, 325), screenManager);
            MenuOption option3 = new MenuOption("Year of End", new Vector2(screenManager.ScreenDimensions.X / 2, 375), new string[] { "40", "50", "60", "70", "80", "10", "20", "30"}, new Vector2(700, 375), screenManager);
            MenuOption option4 = new MenuOption("Starting Money", new Vector2(screenManager.ScreenDimensions.X / 2, 425), new string[] { "0k", "10k", "20k", "30k", "40k", "50k", "60k", "70k", "80k", "90k", "100k", }, new Vector2(700, 425), screenManager);
            MuScreenManager.MenuItem item3 = new MuScreenManager.MenuItem("Go!", new Vector2(screenManager.ScreenDimensions.X / 2, 475), screenManager);

            //Add consequences to hitting the buttons
            item3.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(item3_OnButtonSelect);

            //Add items to screen
            AddItem(option1);
            AddItem(option2);
            AddItem(option3);
            AddItem(option4);
            AddItem(item3);
            tempOptions.Add(option1);
            tempOptions.Add(option2);
            tempOptions.Add(option3);
            tempOptions.Add(option4);
        }

        /// <summary>
        /// Consequence for hitting the go button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item3_OnButtonSelect(object sender, MenuObjectEventArgs e)
        {
            Enum.Parse(typeof(GameMode), tempOptions[0].SelectedValue.Replace(" ", ""));
            screenManager.RemoveAllScreens();
            screenManager.AddScreen(new BoardGame((GameMode)Enum.Parse(typeof(GameMode), tempOptions[0].SelectedValue.Replace(" ", "")), 
                                    Convert.ToInt32(tempOptions[1].SelectedValue), 
                                    Convert.ToInt32(tempOptions[2].SelectedValue), 
                                    Convert.ToInt32(tempOptions[3].SelectedValue.Substring(0, tempOptions[2].SelectedValue.Length - 1))));

            menuMusic.Stop();
        }
    }
}
