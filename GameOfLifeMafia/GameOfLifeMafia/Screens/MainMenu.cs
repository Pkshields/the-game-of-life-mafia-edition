﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using System.Xml.Serialization;
using System.IO;
using MuScreenManager;
using System.Xml.Linq;

namespace GameOfLifeMafia
{
    public class MainMenu : MenuScreen
    {
        public List<string> _sgames;

        private Music menuMusic;

        //constructor to pass through savegames to loadgamemenuscreen
        public MainMenu(List<string> sgames)
        {
            _sgames = sgames;
        }
        //empty constructor
        public MainMenu()
        { }   
        /// <summary>
        /// Initialise the MenuItems components of the menu
        /// </summary>
        /// 


        public override void Initialize()
        {
            //Initialize the MenuItems using the MenuItem from MSM
            MuScreenManager.MenuItem item1 = new MuScreenManager.MenuItem("Begin!", new Vector2(screenManager.ScreenDimensions.X / 2, 275), screenManager);
            MuScreenManager.MenuItem item2 = new MuScreenManager.MenuItem("Exit", new Vector2(screenManager.ScreenDimensions.X / 2, 325), screenManager);
            MuScreenManager.MenuItem item3 = new MuScreenManager.MenuItem("Load", new Vector2(screenManager.ScreenDimensions.X / 2, 375), screenManager);

            //Add consequences to hitting the buttons
            item1.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(item1_OnButtonSelect);
            item2.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(item2_OnButtonSelect);
            item3.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(item3_OnButtonSelect);
            //Add items to screen
            AddItem(item1);
            AddItem(item2);
            AddItem(item3);

            //Create the menu music and play
            menuMusic = screenManager.Audio.GetMusic("Music", "MenuSong");
            menuMusic.Play();

            LoadGameMenuScreen ls = new LoadGameMenuScreen("", _sgames);
     //       ls.OnLoad += new EventHandler<EventArgs>(ls_OnLoad);
        }

        /// <summary>
        /// Consequence for hitting the go button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item1_OnButtonSelect(object sender, MenuObjectEventArgs e)
        {
            screenManager.RemoveScreen(this);
            screenManager.AddScreen(new StartGameMenu(menuMusic));
        }

        /// <summary>
        /// Consequence for hitting the quit button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item2_OnButtonSelect(object sender, MenuObjectEventArgs e)
        {
            ConfirmMessageScreen confirmScreen = new ConfirmMessageScreen("Close the game?");
            confirmScreen.OnConfirm += new EventHandler<EventArgs>(conirmScreen_OnConfirm);
            screenManager.AddScreen(confirmScreen);
        }

        /// <summary>
        /// Consequence for hitting the Load button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item3_OnButtonSelect(object sender, MenuObjectEventArgs e)
        {
            LoadGameMenuScreen lGameScreen = new LoadGameMenuScreen("Load Game", _sgames);
            screenManager.RemoveScreen(this);
            lGameScreen.OnConfirm+=new EventHandler<EventArgs>(lGameScreen_OnConfirm);           
            screenManager.AddScreen(lGameScreen);

        }
        //started loading game
        private void lGameScreen_OnLoad(object sender, MenuObjectEventArgs e)
        {
            int whiteSpaceNodes;
            //load from file root.xml
            XElement xmlTree2 = XElement.Load("Root.xml",
                LoadOptions.None);
            whiteSpaceNodes = xmlTree2
                .DescendantNodesAndSelf()
                .OfType<XText>()
                .Where(tNode => tNode.ToString().Trim().Length == 0)
                .Count();
            Console.WriteLine("Count of white space nodes (not preserving whitespace): {0}", whiteSpaceNodes);
            screenManager.RemoveScreen(this);
            screenManager.AddScreen(new BackgroundScreen("MSM/title", true));
            screenManager.AddScreen(new MainMenu());
        }

        /// <summary>
        /// Consequence for hitting the Yes! Quit! button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void conirmScreen_OnConfirm(object sender, EventArgs e)
        {
            screenManager.Game.Exit();
        }
        private void lGameScreen_OnConfirm(object sender, EventArgs e)
        {
            screenManager.RemoveScreen(this);
            screenManager.AddScreen(new BackgroundScreen("MSM/title", true));
            screenManager.AddScreen(new MainMenu());
            int whiteSpaceNodes;
            XElement xmlTree2 = XElement.Load("Root.xml",
                LoadOptions.None);
            whiteSpaceNodes = xmlTree2
                .DescendantNodesAndSelf()
                .OfType<XText>()
                .Where(tNode => tNode.ToString().Trim().Length == 0)
                .Count();
            Console.WriteLine("Count of white space nodes (not preserving whitespace): {0}", whiteSpaceNodes);
            screenManager.RemoveScreen(this);
            screenManager.AddScreen(new BackgroundScreen("MSM/title", true));
            screenManager.AddScreen(new MainMenu());
        }
    }
}
