﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MuScreenManager;

namespace GameOfLifeMafia
{
    class SetCareerMenuScreen : MenuScreen
    {
        /// <summary>
        /// Player we are assigning a career
        /// </summary>
        private Player player;

        /// <summary>
        /// List of careers we can use for this careeer type
        /// </summary>
        private List<Career> careerList = new List<Career>();
        private CareerManager careerManager;

        /// <summary>
        /// BG settings
        /// </summary>
        private Texture2D whitePixel;
        private Rectangle drawRect;

        /// <summary>
        /// Career centric options to draw and update
        /// </summary>
        private MenuOption careerOption;
        private DrawableString itemInfo;

        /// <summary>
        /// Store the current career we are on for the player
        /// </summary>
        private int currentCareer = 0;

        /// <summary>
        /// Event to run on screen finish
        /// </summary>
        public event EventHandler<UpdatePlayerEventArgs> OnFinish;

        /// <summary>
        /// Initialise an instance of a StartGameMenu using MenuScreen
        /// </summary>
        public SetCareerMenuScreen(Player player, CareerManager careerManager, int currentPlayer, CareerType type)
            : base("Select Career - Player " + (currentPlayer + 1))
        {
            //Store player
            this.player = player;
            this.careerManager = careerManager;
            
            //Store a list of all careers that are applicable for this career type
            foreach (var career in careerManager.CareerList)
                if (career.CareerType == type)
                    careerList.Add(career);
        }

        /// <summary>
        /// Initialise the MenuItems components of the menu
        /// </summary>
        public override void Initialize()
        {
            //Get a string array of all the careers we can select
            string[] careerOptions = new string[careerList.Count];
            for (int i = 0; i < careerOptions.Length; i++)
                careerOptions[i] = careerList[i].Title;

            //Find the largest string in the list so we can draw this at the centre of the screen
            Vector2 largestString = Vector2.Zero, tempVector;
            foreach (var careerText in careerOptions)
            {
                //Get the size of the string the compare it to the current largest
                tempVector = font.MeasureString(careerText);
                if (tempVector.X > largestString.X)
                    largestString = tempVector;
            }

            //Setup the MenuOptions, MenuItems and the String to draw on screen
            careerOption = new MenuOption("", Vector2.Zero, careerOptions, new Vector2((screenManager.ScreenDimensions.X / 2) - (largestString.X / 2), 100), screenManager);
            itemInfo = new DrawableString("Salary Info: Level 0 - " + careerList[currentCareer].AllSalaries()[0] + "\n" +
                                          "\t     Level 1 - " + careerList[currentCareer].AllSalaries()[1] + "\n" +
                                          "\t     Level 2 - " + careerList[currentCareer].AllSalaries()[2] + "\n" +
                                          "\t     Level 3 - " + careerList[currentCareer].AllSalaries()[3],
                                          new Vector2(500, 250), screenManager.Font);
            MuScreenManager.MenuItem item1 = new MuScreenManager.MenuItem("Continue", new Vector2((screenManager.ScreenDimensions.X / 2), 400), screenManager);
            item1.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(item1_OnButtonSelect);

            //Add the items to the item list for updating and drawing
            AddItem(careerOption);
            AddItem(item1);
        }

        /// <summary>
        /// Load content to the graphics card
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            //Draw the background for the entire screen
            drawRect = new Rectangle(0, 0, (int)screenManager.ScreenDimensions.X, (int)screenManager.ScreenDimensions.Y);

            whitePixel = new Texture2D(screenManager.GraphicsDevice, 1, 1);
            whitePixel.SetData(new[] { Color.White });
        }

        public override void Update(GameTime gameTime)
        {
            //Set the current item to the CareersOption for career selecting
            currentItem = 1;

            //Update the CareerOption manually to allow for upodating at the same time as the continue button
            careerOption.Update(gameTime, true);

            //Update base class     //Updated manually using code taken from base.Update() to allow for both buttons updating at once
            //base.Update(gameTime);

            //PART 2: Check if a menu item has been selected
            //I.E. Clicked on, etc.
            //Check all input devices at once);
            if (screenManager.Input.IsButtonReleased(Buttons.A, PlayerIndex.One) ||
                screenManager.Input.IsButtonReleased(Buttons.Start, PlayerIndex.One) ||
                screenManager.Input.IsKeyReleased(Keys.Enter) ||
                (screenManager.Input.IsLeftMouseReleased() && screenManager.Input.IsMouseIntersect(menuItem[currentItem].Hitbox)))
                menuItem[1].Update(gameTime, true);
            else
                menuItem[1].Update(gameTime, false);

            //Update the string showing the salary information if the career has changed
            if (currentCareer != careerOption.SelectedID)
            {
                //Career has changed, update the salary text
                currentCareer = careerOption.SelectedID;

                itemInfo.Text = "Salary Info: Level 0 - " + careerList[currentCareer].AllSalaries()[0] + "\n" +
                                "\t     Level 1 - " + careerList[currentCareer].AllSalaries()[1] + "\n" +
                                "\t     Level 2 - " + careerList[currentCareer].AllSalaries()[2] + "\n" +
                                "\t     Level 3 - " + careerList[currentCareer].AllSalaries()[3];
            }
        }

        /// <summary>
        /// Draw the menu options as well as the BG
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw these objects on screen</param>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            //Draw the translucent background
            spriteBatch.Draw(whitePixel, drawRect, Color.Black * 0.6f);

            //Draw the info string
            itemInfo.Draw(spriteBatch);

            //Draw the rest of MenuItem
            base.Draw(spriteBatch, gameTime);
        }

        /// <summary>
        /// Give the player the career they selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void item1_OnButtonSelect(object sender, MenuObjectEventArgs e)
        {
            //For all careers in the list
            for (int i = 0; i < careerList.Count; i++)
            {
                //If the title of that career is the same as the text on the button
                if (careerOption.SelectedValue.Equals(careerList[i].Title))
                {
                    //If player has a career already, release that career to be available
                    if (player.Career != null)
                        careerManager.Add(player.Career);

                    //Add that career to the player and finish the screen
                    player.AddCareer(careerList[i]);
                    careerManager.Remove(careerList[i]);
                    screenManager.RemoveScreen(this);
                    screenManager.ChangeAllScreenStates(ScreenState.Active);
                    
                    //If their is an event to call and send back player, call it
                    if (OnFinish != null)
                        OnFinish(this, new UpdatePlayerEventArgs(player));
                }
            }
        }
    }
}