﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MuScreenManager;

namespace GameOfLifeMafia
{
    class CareerTypeMenuScreen : MenuScreen
    {
        /// <summary>
        /// Manager managing the careers in this game
        /// </summary>
        private CareerManager careerManager;

        /// <summary>
        /// BG settings
        /// </summary>
        private Texture2D whitePixel;
        private Rectangle drawRect;

        /// <summary>
        /// Initialise an instance of a StartGameMenu using MenuScreen
        /// </summary>
        public CareerTypeMenuScreen(int currentPlayer, CareerManager careerManager)
            : base("Select Career - Player " + (currentPlayer + 1))
        {
            this.careerManager = careerManager;
        }

        /// <summary>
        /// Initialise the MenuItems components of the menu
        /// </summary>
        public override void Initialize()
        {
            MuScreenManager.MenuItem item1 = new MuScreenManager.MenuItem("Mafia Type", new Vector2(screenManager.ScreenDimensions.X / 2, 100), screenManager);
            MuScreenManager.MenuItem item2 = new MuScreenManager.MenuItem("Good Guy Type", new Vector2(screenManager.ScreenDimensions.X / 2, 200), screenManager);

            item1.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(item1_OnButtonSelect);
            item2.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(item2_OnButtonSelect);

            AddItem(item1);
            AddItem(item2);
        }

        /// <summary>
        /// Load content to the graphics card
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            //Draw the background for the entire screen
            drawRect = new Rectangle(0, 0, (int)screenManager.ScreenDimensions.X, (int)screenManager.ScreenDimensions.Y);

            whitePixel = new Texture2D(screenManager.GraphicsDevice, 1, 1);
            whitePixel.SetData(new[] { Color.White });
        }

        /// <summary>
        /// Draw the menu options as well as the BG
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw these objects on screen</param>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            //Draw the translucent background
            spriteBatch.Draw(whitePixel, drawRect, Color.Black * 0.6f);

            //Draw the rest of MenuItem
            base.Draw(spriteBatch, gameTime);
        }

        /// <summary>
        /// For Mafia choice
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item1_OnButtonSelect(object sender, MenuObjectEventArgs e)
        {
            //Mafia, remove this screen and set new career immediately
            careerManager.Stage = CareerManagerStage.SetMafiaCareer;
            screenManager.ChangeAllScreenStates(ScreenState.Active);
            screenManager.RemoveScreen(this);
        }

        /// <summary>
        /// For the non mafia choice
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item2_OnButtonSelect(object sender, MenuObjectEventArgs e)
        {
            //Good guy, remove this screen and don't initialize career yet
            screenManager.ChangeAllScreenStates(ScreenState.Active);
            screenManager.RemoveScreen(this);
        }
    }
}