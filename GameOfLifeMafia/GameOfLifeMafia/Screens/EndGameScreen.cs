﻿#region FILE DESCRIPTION
//-----------------------------------------------------------------------------
// MuScreenManager
// 
// EndGameMenuScreen.cs
//-----------------------------------------------------------------------------
#endregion

#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion USING

namespace MuScreenManager
{
    /// <summary>
    /// Class to show a confirmation message using the existing MenuScreen, with a few extra tweaks
    /// </summary>
    public class EndGameMenuScreen : MenuScreen
    {
        #region PROPERTIES

        /// <summary>
        /// Provides a white pixel we can manipulate and draw from
        /// </summary>
        Texture2D whitePixel;

        /// <summary>
        /// Size of the rect to draw for th ebackground of this screen
        /// </summary>
        Rectangle drawRect;

        /// <summary>
        /// Holds a list of strings passed in to create drawable strings
        /// </summary>
        List <string> _stats;
        List <DrawableString> DrawableStats;


        #endregion PROPERTIES

        #region EVENTS

        /// <summary>
        /// Event to run upon confirming screen
        /// </summary>
        public event EventHandler<EventArgs> OnConfirm;
        /// <summary>
        /// Event to run showing the previous players stats screen
        /// </summary>
        public event EventHandler<EventArgs> OnConfirm_PreviousPlayersStats;
        /// <summary>
        /// Event to run showing the next players stats screen
        /// </summary>
        public event EventHandler<EventArgs> OnConfirm_NextPlayersStats;

        #endregion EVENTS

        #region METHODS


        /// overided constructor allows a list of stats to be drawn onscreen
        /// </summary>
        /// <param name="confirmText"></param>
        /// <param name="stat"></param>
        public EndGameMenuScreen(string TitleText, List<string> stat)
            : base(TitleText)
        { 
             //initialise stats list
            _stats = new List<string>();
            _stats = stat;       
        }
        /// <summary>
        /// Initialize the menu components of screen and pause all other screens
        /// </summary>
        public override void Initialize()
        {
            //Pause all screens other than this
            screenManager.ChangeAllScreenStates(ScreenState.Paused, this);

            //Get the size of the screen and store as rectangle
            drawRect = new Rectangle(0, 0, (int)screenManager.ScreenDimensions.X, (int)screenManager.ScreenDimensions.Y);

            //Create the MenuItems to navigate back and forth
            MenuItem item1 = new MenuItem("Previous Player", new Vector2((screenManager.ScreenDimensions.X * 0.25f), 600), screenManager);
            MenuItem item2 = new MenuItem("Next Player", new Vector2((screenManager.ScreenDimensions.X * 0.75f), 600), screenManager);
            MenuItem item3 = new MenuItem("Return to Main Menu", new Vector2((screenManager.ScreenDimensions.X * 0.5f), 650), screenManager);
            //intialise list of drawablestats and start position for drawable stats.Y
            DrawableStats = new List<DrawableString>();
            int startpos = 100;

            //for all of the strings create a new temp drawable string and add to DrawableStats List
            foreach (var stringItem in _stats)
            { 
                DrawableString titem = new DrawableString(""+stringItem,new Vector2(screenManager.ScreenDimensions.X / 2,startpos += 50),screenManager.Font);
                DrawableStats.Add(titem);
            }

            //Add the event handlers
            item1.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(item1_PreviousPlayer);
            item2.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(item2_NextPlayer);
            item3.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(item3_OnButtonSelect);
            //Add to screen
            AddItem(item1);
            AddItem(item2);
            AddItem(item3);
        }

        /// <summary>
        /// Load the 1 pixel sprite
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            whitePixel = new Texture2D(screenManager.GraphicsDevice, 1, 1);
            whitePixel.SetData(new[] { Color.White });
        }

        /// <summary>
        /// Update MenuScreen items as well as check for additional cancel inputs
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        public override void Update(GameTime gameTime)
        {
            //Update the MenuScreen first
            base.Update(gameTime);

            //Check all gamepads for calcennation buttons
            for (int i = 0; i < screenManager.Input.NumOfGamepad; i++)
            {
                if (screenManager.Input.IsButtonPressed(Buttons.B, (PlayerIndex)i))
                    CloseScreen();
            }

            //Check the keyboard for cancellation buttons
            if (screenManager.Input.IsKeyPressed(Keys.Escape) || screenManager.Input.IsKeyPressed(Keys.P))
                CloseScreen();
        }

        /// <summary>
        /// Draw all menu items as well as additional background
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw these objects on screen</param>
        /// <param name="gameTime">Provides a snapshot of the game time</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            //Draw the translucent background
            spriteBatch.Draw(whitePixel, drawRect, Color.Black * 0.6f);
            //draw all the drawablestrings
            foreach (var DrawableStringItem in DrawableStats)
            {
                DrawableStringItem.Draw(spriteBatch);
            }
            //Draw the rest of MenuItem
            base.Draw(spriteBatch, gameTime);
        }

        /// <summary>
        /// Method to unpause all screens and remove this one
        /// </summary>
        private void CloseScreen()
        {
            screenManager.ChangeAllScreenStates(ScreenState.Active, this);
            screenManager.RemoveScreen(this);
        }

        /// <summary>
        /// Event method to run confirmation event code
        /// </summary>
        /// <param name="sender">Caller of this event</param>
        /// <param name="e">Event params</param>
        private void item3_OnButtonSelect(object sender, MenuObjectEventArgs e)
        {
            if (OnConfirm != null)
            {
                screenManager.ChangeAllScreenStates(ScreenState.Active, this);
                OnConfirm(this, new EventArgs());
                screenManager.RemoveScreen(this);
            }
        }
        private void item1_PreviousPlayer(object sender, MenuObjectEventArgs e)
        {
            if (OnConfirm_PreviousPlayersStats != null)
            {
                screenManager.ChangeAllScreenStates(ScreenState.Active, this);
                OnConfirm_PreviousPlayersStats(this, new EventArgs());
                screenManager.RemoveScreen(this);
            }
        }
        private void item2_NextPlayer(object sender, MenuObjectEventArgs e)
        {
            if (OnConfirm_NextPlayersStats != null)
            {
                screenManager.ChangeAllScreenStates(ScreenState.Active, this);
                OnConfirm_NextPlayersStats(this, new EventArgs());
                screenManager.RemoveScreen(this);
            }
        }
        #endregion METHODS

    }
}

