﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameOfLifeMafia
{
    /// <summary>
    /// Playerstats stores all the data of each player into one object in a list of ints
    /// will be used for endgamemenu screens and for loading and saving games
    /// </summary>
    public class PlayerStats
    {
        List <int> _stats;
        int playernum;
        public PlayerStats(List <int> stats)
        {
            playernum = 0;
            _stats = stats;
        }

        //overrided constructor to work with xml serialiser
        public PlayerStats()
        {
            List<int> s = _stats;
        }

        //returns a list of strings for to be used by DrawableString
        public List<string> addDrawableStringText()
        {
            //cheap workaround, please don't judge me :)
            playernum++;
            //returns list with string details added
            return new List<string> { "Year Ended: " + _stats.ElementAt(0), "Player" + (playernum + 1) + " House Count: " + _stats.ElementAt(1), "Player" + (playernum + 1) + " Money: $" + _stats.ElementAt(2) };
        }
    }
}
