﻿// Note: Code for randomising using RNGCryptoServiceProvider and range checking is modified code from
// http://msdn.microsoft.com/en-us/library/system.security.cryptography.rngcryptoserviceprovider.aspx

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Security.Cryptography;
using System.Xml.Serialization;

namespace GameOfLifeMafia
{
    /// <summary>
    /// Class to handle the events triggered by landing on a <c>StorySquare</c>
    /// </summary>
    public class StoryEvents
    {
        // Queue is for the 'deck' of 'Cards' the game, List for the used cards
        // to be placed, and the last three variables are for references to events
        // putside the class
        private Queue<Card> _storyMechanics = new Queue<Card>();
        private List<Card> _discardPile = new List<Card>();
        private Colour _currentColour;
        private Player[] _players;
        private int _currentPlayer;
        private BoardManager _boardManager;

        /// <summary>
        /// Struct for the different events a <c>Card</c> can impose 
        /// on the player. Each <c>Card</c> has 2 <c>Conditions</c>
        /// </summary>
        public struct Condition
        {
            public StoryMethod method;  // 'method' stores the 'StoryMethod' for the 'Condition'
            public int amount1;         // 'amount1' stores the amount of money the 'Condition' affects
            public int amount2;         // 'amount2' is used only when the 'Gamble' StoryEvent is used
            public int noOfOdds;        // 'noOfOdds' stores the denominator of the fraction used for gambling
            public int winOdds;         // 'winOdd' stores the numerator of the fraction used for gambling
            public string description;  // 'description stores a randomised description of what happens to the player associated with the 'StoryMethod'
        }

        /// <summary>
        /// Struct for the a <c>Card</c> that is drawn by the player when
        /// they land on a <c>StorySquare</c>
        /// </summary>
        public struct Card
        {
            public Condition red;
            public Condition black;
        }

        ///<summary>
        /// Enum Variable to determine the type of event that is triggered, 
        /// with XMLEnum elements for each value to help the XML reader determine
        /// what value to assign to every <c>Condition</c>
        /// </summary>
        public enum StoryMethod
        {
            // Indicates the current player will gain money
            [XmlEnum("Addition")]
            Addition = 0,
            // Indicates the current player will lose money
            [XmlEnum("Subtraction")]
            Subtraction = 1,
            // Indicates the current player will gain money from the next player
            [XmlEnum("GainLeft")]
            GainLeft = 2,
            // Indicates the current player will give money to the next player
            [XmlEnum("LoseLeft")]
            LoseLeft = 3,
            // Gives the player a promotion in their current carrer
            [XmlEnum("Promotion")]
            Promotion = 4,
            // Gives the player a promotion in their current carrer
            [XmlEnum("Demotion")]
            Demotion = 5,
            // Causes the player to either lose or gain money depending on the win/lose condition
            [XmlEnum("GambleOne")]
            GambleOne = 6,
            // Causes any players with Mafia careers to go to prison, and rewards the players with Good careers
            [XmlEnum("PrisonMafia")]
            PrisonMafia = 7,
            // Causes any players with Mafia careers to go to prison, and rewards the players with Good careers
            [XmlEnum("MafiaPayday")]
            MafiaPayday = 8,
            [XmlEnum("GainSpouse")]
            GainSpouse = 9,
            [XmlEnum("GainChild")]
            GainChild = 10,
            [XmlEnum("GainPet")]
            GainPet = 11,
            [XmlEnum("LoseSpouse")]
            LoseSpouse = 12,
            [XmlEnum("LoseChild")]
            LoseChild = 13,
            [XmlEnum("LosePet")]
            LosePet = 14,
        }

        /// <summary>
        /// Create an instance of StoryEvents
        /// </summary>
        /// <param name="players">Reference to the list of players<param>
        /// <param name="colour">Reference to current colour of the <c>Spinner</c>c></param>
        /// <param name="currentPlayer">Reference to the current player</param>
        /// <param name="boardManager">Reference to the BoardManager</param>
        public StoryEvents(Player[] players, Colour colour, int currentPlayer, BoardManager boardManager)
        {
            // Assigning the references to their corresponding class variables
            _players = players;
            _currentColour = colour;
            _currentPlayer = currentPlayer;
            _boardManager = boardManager;

            // Reading the XML documents containing the list of 'Cards' and list of 'string' arrays
            _discardPile = loadCards();
            List<string[]> stringTemp = parseDescriptions();

            // for loop assigns each  of the 2 'Conditions' of a 'Card' a description based on their 'StoryEvent'
            for (int i = 0; i < _discardPile.Count; i++)
            {
                Card temp = _discardPile[0];                        // Assigns the current card to a 'temp' Card for editing
                int currentArray = (int)temp.red.method;            // Gets the array associated with the 'StoryEvent' of the red 'Condition'
                int arraySize = stringTemp[currentArray].Length;    // Gets the size of the currently selected array for randomisation

                // Assigns a random string to the description for the red 'Condition' of 'temp' using the 'Randomiser' method
                temp.red.description = (string)stringTemp[currentArray].GetValue(Randomiser((byte)arraySize));

                currentArray = (int)temp.black.method;          // Gets the array associated with the 'StoryEvent' of the black 'Condition'
                arraySize = stringTemp[currentArray].Length;    // Gets the size of the currently selected array for randomisation

                // Assigns a random string to the description for the black 'Condition' of 'temp' using the 'Randomiser' method
                temp.black.description = (string)stringTemp[currentArray].GetValue(Randomiser((byte)arraySize));

                // Removes the current 'Card' from the start of the '_discardPile' List and adds the modified copy of it , 'temp', to the end of the '_discardPile' List
                _discardPile.RemoveAt(0);
                _discardPile.Add(temp);
            }

            // Calls the 'ShuffleDeck' method, used to empty the '_discardPile' List and fill the '_storyMechanics' Queue
            ShuffleDeck();
        }

        /// <summary>
        /// Method to ensure the class Stays up to date
        /// </summary>
        /// <param name="colour">Colour used to determine which <c>Condition</c> to read</param>
        /// <param name="currentPlayer">Int to indicate the current player</param>
        public void Update(Colour colour, int currentPlayer)
        {
            _currentColour = colour;
            _currentPlayer = currentPlayer;
        }

        /// <summary>
        /// Reads the event of the most current <c>Card</c> based on the current <c>Colour</c>
        /// and then carries out the actions associated with the event
        /// </summary>
        public void ReadEvent(Player player)
        {
            Card card = _storyMechanics.Dequeue();  // Most current 'Card' from the 'deck'
            _discardPile.Add(card);                 // Add the card to the discard pile
            Condition con;                          // Declaring a 'Condition' to hold the relevant 'Condition' from the 'Card'

            // Switch statement that checks '_currentColour' and assigns the relevant 'Condition'
            // of the 'Card' based on the case. Defaults to the red 'Condition' as a precaution despite
            // the fact that both 'Conditions' should be assign to the 'Card'
            switch (_currentColour)
            {
                case Colour.Red:
                    con = card.red;
                    break;
                case Colour.Black:
                    con = card.black;
                    break;
                default:
                    con = card.red;
                    break;
            }

            //Switch statement that carries out different actions depending on the 'StoryEvent'
            switch (con.method)
            {
                // Adds 'amount1' to the '_currentPlayer's' money, and notifies the player
                case StoryMethod.Addition:
                    player.ChangeMoney(con.amount1);
                    System.Diagnostics.Debug.WriteLine(con.description, con.amount1);
                    break;
                // Multiplies 'amount1' by -1 and then adds it to the '_currentPlayer's' 
                // money, and notifies the player. Done to prevent the notification to the 
                // player from being slightly off, e.g. "you have lost -10 SSDs"
                case StoryMethod.Subtraction:
                    player.ChangeMoney(con.amount1 * -1);
                    System.Diagnostics.Debug.WriteLine(con.description, con.amount1);
                    break;
                // Adds 'amount1' to the '_currentPlayer's' money, multiplies 'amount1' 
                // by -1 and then adds it to the next players money and notifies the player.
                // Done again to prevent the notification from being slightly off.
                case StoryMethod.GainLeft:
                    player.ChangeMoney(con.amount1);
                    _players[(_currentPlayer + 1) % _players.Length].ChangeMoney(con.amount1 * -1);
                    System.Diagnostics.Debug.WriteLine(con.description, con.amount1);
                    break;
                // Multiplies 'amount1' by -1 and then adds it to the '_currentPlayer's' money, 
                // adds 'amount1' to the next players money and notifies the player. Done again
                // to prevent the notification from being slightly off.
                case StoryMethod.LoseLeft:
                    player.ChangeMoney(con.amount1 * -1);
                    _players[(_currentPlayer + 1) % _players.Length].ChangeMoney(con.amount1);
                    System.Diagnostics.Debug.WriteLine(con.description, con.amount1);
                    break;
                // Promotes the '_currentPlayer' to the next pay grade of their career, if they have
                // one, if not then it draws a new card.
                case StoryMethod.Promotion:
                    // Check to see if the player has a career initalised
                    if (_players[_currentPlayer].Career != null)
                    {
                        _players[_currentPlayer].Career.ChangeSalary(CareerLevel.Promote);
                        System.Diagnostics.Debug.WriteLine(con.description);
                    }
                    else
                        ReadEvent(player);
                    break;
                // Promotes the '_currentPlayer' to the next pay grade of their career, if they have
                // one, if not then it draws a new card.
                case StoryMethod.Demotion:
                    // Check to see if the player has a career initalised
                    if (_players[_currentPlayer].Career != null)
                    {
                        _players[_currentPlayer].Career.ChangeSalary(CareerLevel.Demote);
                        System.Diagnostics.Debug.WriteLine(con.description);
                    }
                    else
                        ReadEvent(player);
                    break;
                // Notifies the player of what will win, or lose, depending on the odds dictated by
                // the 'Condition', obtains a random number based on 'noOfOdds' and then compares it
                // to 'winOdds', if it is less then 'winOdds' it counts as a win, else it counts as a
                // loss. It then notifies the player of whether they won or lost
                case StoryMethod.GambleOne:
                    System.Diagnostics.Debug.WriteLine(con.description, con.amount1, con.amount2);
                    int i = Randomiser((byte)con.noOfOdds);

                    if (i < con.winOdds)
                    {
                        _players[_currentPlayer].ChangeMoney(con.amount1);
                        System.Diagnostics.Debug.WriteLine("Win! You gain {0}SSDs", con.amount1);
                    }
                    else
                    {
                        _players[_currentPlayer].ChangeMoney(con.amount2 * -1);
                        System.Diagnostics.Debug.WriteLine("Loss! You lose {0}SSDs", con.amount2);
                    }
                    break;
                // Notifies the player of what will win, or lose, depending on the odds dictated by
                // the 'Condition', obtains a random number based on 'noOfOdds' and then compares it
                // to 'winOdds', if it is less then 'winOdds' it counts as a win, else it counts as a
                // loss. It then notifies the player of whether they won or lost
                case StoryMethod.PrisonMafia:
                    if (player.IsMafia.HasValue && (bool)player.IsMafia)
                        _players[_currentPlayer].SetCurrentSquare(_boardManager.GetNextSquare(123));
                    else
                        _players[_currentPlayer].ChangeMoney(con.amount1);

                    System.Diagnostics.Debug.WriteLine(con.description, con.amount1);
                    break;
                // Notifies the player of what will win, or lose, depending on the odds dictated by
                // the 'Condition', obtains a random number based on 'noOfOdds' and then compares it
                // to 'winOdds', if it is less then 'winOdds' it counts as a win, else it counts as a
                // loss. It then notifies the player of whether they won or lost
                case StoryMethod.MafiaPayday:
                    if (player.IsMafia.HasValue && (bool)player.IsMafia)
                        player.ChangeMoney(con.amount1);
                    else
                        player.ChangeMoney(con.amount2 * -1);

                    System.Diagnostics.Debug.WriteLine(con.description, con.amount1, con.amount2);
                    break;
                // Notifies the player that they have potentially gained a spouse, and of the amount
                // of money they will gain from the wedding and the ammount they gain for the anniversary
                // if they already have one. Then it adds a "Spouse" Family member to the player's family
                // if it can and gives the player the money dependent on whether they have a spouse or not
                case StoryMethod.GainSpouse:
                    if (!player.HasSpouse)
                    {
                        player.AddFamilyMember(Family.Spouse);
                        player.ChangeMoney(con.amount1);
                    }
                    else
                        player.ChangeMoney(con.amount2);

                    System.Diagnostics.Debug.WriteLine(con.description, con.amount1, con.amount2);
                    break;
                // Notifies the player that they have gained a child and then the amount of money the player
                // gains for gaining the child. It then adds the "Child" Family member to the player's family
                // if it can and gives the player the money dependent on whether they have a spouse or not
                case StoryMethod.GainChild:
                    player.AddFamilyMember(Family.Child);
                    player.ChangeMoney(con.amount1);

                    System.Diagnostics.Debug.WriteLine(con.description, con.amount1);
                    break;
                // Notifies the player that they have gained a pet and then the amount of money the player
                // gains for gaining the pet. It then adds the "Pet" Family member to the player's family
                // if it can and gives the player the money dependent on whether they have a spouse or not
                case StoryMethod.GainPet:
                    player.AddFamilyMember(Family.Pet);
                    player.ChangeMoney(con.amount1);

                    System.Diagnostics.Debug.WriteLine(con.description, con.amount1);
                    break;
                // Notifies the player that they have potentially lost a spouse, and of the amount
                // of money they will gain from the loss and the ammount they gain for the mistaken death
                // if they don't have one. Then it removes a "Spouse" Family member to the player's family
                // if it can and gives the player the money dependent on whether they had a spouse or not
                case StoryMethod.LoseSpouse:
                    if (player.HasSpouse)
                    {
                        player.RemoveFamilyMember(Family.Spouse);
                        player.ChangeMoney(con.amount1);
                    }
                    else
                        player.ChangeMoney(con.amount2);

                    System.Diagnostics.Debug.WriteLine(con.description, con.amount1, con.amount2);
                    break;
                // Notifies the player that they have potentially lost a child, and of the amount of money
                // they will gain from the potential loss. Then it removes a "Child" Family member to the
                // player's family if it can and gives the player the money regardless of whether they had
                // a child or not
                case StoryMethod.LoseChild:
                    player.RemoveFamilyMember(Family.Child);
                    player.ChangeMoney(con.amount1);

                    System.Diagnostics.Debug.WriteLine(con.description);
                    break;
                // Notifies the player that they have potentially lost a pet, and of the amount of money
                // they will gain from the potential loss. Then it removes a "Pet" Family member to the
                // player's family if it can and gives the player the money regardless of whether they had
                // a pet or not
                case StoryMethod.LosePet:
                    player.RemoveFamilyMember(Family.Pet);
                    System.Diagnostics.Debug.WriteLine(con.description);
                    break;
            }
        }

        /// <summary>
        /// Construct a List of <c>Cards</c> from the XML file that contains the List of
        /// <c>Cards</c>, and then returns the List
        /// </summary>
        /// <returns>Returns the constructed <c>Card</c> List</returns>
        private static List<Card> loadCards()
        {
            // List we are generating being initalised
            List<Card> newList = newList = new List<Card>();

            // String for the specific XML file
            string xmlFile = @"Content/CardList.xml";

            // Deserialize the XML file into the Card List
            XmlSerializer deserializer;
            try
            {
                deserializer = new XmlSerializer(typeof(List<Card>));
            }
            // Catch for the exception that can be thrown by the XMLSerializer
            catch (Exception ex)
            {
                throw ex.InnerException.InnerException.InnerException;
            }

            // Loads the XML file into the TextReader
            TextReader textReader = new StreamReader(xmlFile);
            
            // Assigns the 'Card' List read from the XML file
            newList = (List<Card>)deserializer.Deserialize(textReader);
            
            // Closes the TextReader
            textReader.Close();

            // Send the 'Card' List back to caller
            return newList;
        }

        /// <summary>
        /// Construct a List of Arrays of <c>Strings</c> from the XML file that contains the List of
        /// Arrays of <c>String</c>, and then returns the List
        /// </summary>
        /// <returns>Returns the List of Arrays of <c>String</c></returns>
        private static List<string[]> parseDescriptions()
        {
            // Get the XML file that contains the List of Arrays of 'String' associated with the 'Cards'
            string xmlFile = @"Content/CardDescriptionList.xml";

            // Deserialize the XML file into the Array of String List
            XmlSerializer deserializer;
            try
            {
                deserializer = new XmlSerializer(typeof(List<string[]>));
            }
            // Catch for the exception that can be thrown by the XMLSerializer
            catch (Exception ex)
            {
                throw ex.InnerException.InnerException.InnerException;
            }

            // Loads the XML file into the TextReader
            TextReader textReader = new StreamReader(xmlFile);

            // Assigns the Array of 'String' List read from the XML file
            List<string[]> newList = (List<string[]>)deserializer.Deserialize(textReader);

            // Closes the TextReader
            textReader.Close();

            //Send the Array of 'String' List back to caller
            return newList;
        }

        /// <summary>
        /// Selects a random <c>Card</c> from <c>_discardPile</c> then adds it to
        /// <c>_storyMechanics</c>, then repeats the process until the <c>_discardPile</c> is empty
        /// </summary>
        public void ShuffleDeck()
        {
            // Declares and intialises the int used for selecting the random 'Card' from '_discardPile'
            int selector = 0;

            // Adds any remaining 'Card' in '_storyMechanics' to the '_discardPile'
            // to insure the entire 'deck' of 'Cards' is randomised
            foreach (Card i in _storyMechanics)
            {
                _discardPile.Add(i);
                _storyMechanics.Dequeue();
            }

            // Declares and intialises the int used for the 'for' loop condition
            int shuffleLimit = _discardPile.Count;

            // Loops while 'i' is less than 'shuffleLimit'
            for (int i = 0; i < shuffleLimit; i++)
            {
                // Generates the random number based on the current number of 'Cards' of '_discardPile'
                selector = Randomiser((byte)_discardPile.Count);

                // Adds the randomly selected 'Card' to the '_storyMechanics' and removes it from the '_discardPile'
                _storyMechanics.Enqueue(_discardPile[selector]);
                _discardPile.RemoveAt(selector);
            }
        }

        /// <summary>
        /// Randomly generates a number between the range of 0 - (size - 1)
        /// passed into the method
        /// </summary>
        /// <param name="size">Used to determine the range</param>
        /// <returns>The randomly generated byte</returns>
        private byte Randomiser(byte size)
        {
            // Checks to make sure 'size' is greater than zero
            if (size <= 0)
                throw new ArgumentOutOfRangeException("size");

            // RNGCryptoServiceProvider and the array of byte it uses is declared
            RNGCryptoServiceProvider randomiser = new RNGCryptoServiceProvider();
            byte[] randomNum = new byte[1];

            // do while loop that continues while 'RangeChecker' returns false
            do
            {
                // fills 'randomNum' with a random number
                randomiser.GetBytes(randomNum);
            }
            while (!RangeChecker(randomNum[0], size));

            // Returns the random number % size
            return (byte)(randomNum[0] % size);
        }

        /// <summary>
        /// Determines whether number is within any multiplication of size
        /// </summary>
        /// <param name="number">Random number between 0 - 'Byte.MaxValue'</param>
        /// <param name="size">Used to determine the number of fullsets of 'size' that exist in 'Byte.MaxValue'</param>
        /// <returns>Returns true or false depending on whether the number is wihin one of te full sets</returns>
        private static bool RangeChecker(byte number, byte size)
        {
            // Divides 'Byte.MaxValue' by 'size' to determine the number of full sets of size
            // discard any remainder, e.g. if 'Byte.MaxValue' equals 255, then if 'size' is 6
            // it produces 42 full sets, discarding the remainder
            int fullSet = Byte.MaxValue / size;

            // Returns true or false based on whether 'number' is within the maximum range of 'size'
            return number < size * fullSet;
        }
    }
}