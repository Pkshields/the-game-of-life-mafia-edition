﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MuScreenManager;

namespace GameOfLifeMafia
{
    public class CareerManager
    {
        /// <summary>
        /// Store the screenmanager for use for generating screens
        /// </summary>
        private ScreenManager screenManager;

        /// <summary>
        /// List of possible careers
        /// </summary>
        private List<Career> careerList = new List<Career>();
        public List<Career> CareerList
        {
            get { return careerList; }
        }

        /// <summary>
        /// Current player we are assigning careers
        /// </summary>
        private int currentPlayer = -1;

        /// <summary>
        /// Player to move back to player list
        /// </summary>
        private Player updatePlayer;

        /// <summary>
        /// Current stage this manager is at
        /// </summary>
        private CareerManagerStage stage = CareerManagerStage.SelectPlayer;
        public CareerManagerStage Stage
        {
            set { stage = value; }
        }

        /// <summary>
        /// Is the CareerManager finished the initial update?
        /// </summary>
        private bool isFinished = false;
        public bool IsFinished
        {
            get { return isFinished; }
        }

        /// <summary>
        /// Initialize a copy of CareerManager
        /// </summary>
        /// <param name="screenManager">Manager managing the screens in this game</param>
        public CareerManager(ScreenManager screenManager)
        {
            this.screenManager = screenManager;

            careerList.Add(new Career("Games Developer", CareerType.Civilian, new int[] { 500, 600, 700, 800 }));
            careerList.Add(new Career("Accountant", CareerType.Civilian, new int[] { 800, 1000, 1200, 1400 }));
            careerList.Add(new Career("Toast Engineer", CareerType.Civilian, new int[] { 100, 200, 300, 400 }));
            careerList.Add(new Career("FBI", CareerType.Civilian, new int[] { 600, 700, 900, 1500 }));
            careerList.Add(new Career("Rogue Games Developer", CareerType.Mafia, new int[] { 500, 600, 700, 800 }));
            careerList.Add(new Career("Rogue Accountant", CareerType.Mafia, new int[] { 800, 1000, 1200, 1400 }));
            careerList.Add(new Career("Rogue Toast Engineer", CareerType.Mafia, new int[] { 100, 200, 300, 400 }));
            careerList.Add(new Career("Rogue FBI", CareerType.Mafia, new int[] { 600, 700, 900, 1500 }));
        }

        /// <summary>
        /// Assign careers depending on users settings from UI screens
        /// </summary>
        /// <param name="playerList">List of players to assign</param>
        /// <returns>If update is finished</returns>
        public bool Update(ref Player[] playerList)
        {
            //If we are on the player selecting page
            if (stage == CareerManagerStage.SelectPlayer)
            {
                //If updatePlayer is not void, update player in array 
                if (updatePlayer != null)
                {
                    playerList[currentPlayer] = updatePlayer;
                    updatePlayer = null;
                }

                //Next player
                currentPlayer++;

                //If player is less than total player, start another one
                if (currentPlayer < playerList.Length)
                {
                    //Create MafiaNonmafia check screen
                    screenManager.ChangeAllScreenStates(ScreenState.Paused);
                    screenManager.AddScreen(new CareerTypeMenuScreen(currentPlayer, this));
                }
                else
                {
                    //Update is finished, tell game to stop
                    isFinished = true;
                }
            }
            //If we are on the player selecting page
            else if (stage == CareerManagerStage.SetMafiaCareer)
            {
                //Generate a choose career screen to let the user choose a mafia screen
                screenManager.ChangeAllScreenStates(ScreenState.Paused);
                SetCareerMenuScreen screen = new SetCareerMenuScreen(playerList[currentPlayer], this, currentPlayer, CareerType.Mafia);
                screen.OnFinish += new EventHandler<UpdatePlayerEventArgs>(screen_OnFinish);
                screenManager.AddScreen(screen);
            }

            //Return if we are finished or not
            return isFinished;
        }

        /// <summary>
        /// Add a career to the List
        /// </summary>
        /// <param name="career">Players career</param>
        public void Add(Career career)
        {
            careerList.Add(career);
        }

        /// <summary>
        /// Remove a career from the list
        /// </summary>
        /// <param name="career">Remove chosen career</param>
        public void Remove(Career career)
        {
            careerList.Remove(career);
        }

        /// <summary>
        /// Update the player in the array and move to the next player
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void screen_OnFinish(object sender, UpdatePlayerEventArgs e)
        {
            updatePlayer = e.player;
            stage = CareerManagerStage.SelectPlayer;
        }

    }

    /// <summary>
    /// Stage we are at with the CareerManager update
    /// </summary>
    public enum CareerManagerStage
    {
        SelectPlayer = 0, 
        SetMafiaCareer = 1
    }

    /// <summary>
    /// Update Player event settings
    /// </summary>
    public class UpdatePlayerEventArgs : EventArgs
    {
        public Player player;

        public UpdatePlayerEventArgs(Player player)
        {
            this.player = player;
        }
    }


}
