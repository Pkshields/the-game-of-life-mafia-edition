﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MuScreenManager;

namespace GameOfLifeMafia
{
    class StartGameMenu : MenuScreen
    {
        List<MenuOption> tempOptions = new List<MenuOption>();

        /// <summary>
        /// Initialise an instance of a StartGameMenu using MenuScreen
        /// </summary>
        public StartGameMenu()
            : base("Start Game!")
        { }

        /// <summary>
        /// Initialise the MenuItems components of the menu
        /// </summary>
        public override void Initialize()
        {
            //Initialize the MenuItems using the MenuItem from MSM
            MenuOption option1 = new MenuOption("No. Of Players", new Vector2(screenManager.ScreenDimensions.X / 2, 100), new string[] { "2", "3", "4" }, new Vector2(700, 100), screenManager);
            MenuOption option2 = new MenuOption("Year of End", new Vector2(screenManager.ScreenDimensions.X / 2, 200), new string[] { "10", "20", "30", "40", "50", "60", "70", "80" }, new Vector2(700, 200), screenManager);
            MuScreenManager.MenuItem item3 = new MuScreenManager.MenuItem("Go!", new Vector2(screenManager.ScreenDimensions.X / 2, 300), screenManager);

            //Add consequences to hitting the buttons
            item3.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(item3_OnButtonSelect);

            //Add items to screen
            AddItem(option1);
            AddItem(option2);
            AddItem(item3);
            tempOptions.Add(option1);
            tempOptions.Add(option2);
        }

        /// <summary>
        /// Consequence for hitting the go button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item3_OnButtonSelect(object sender, MenuObjectEventArgs e)
        {
            screenManager.RemoveAllScreens();
            screenManager.AddScreen(new BoardGame(Convert.ToInt32(tempOptions[0].SelectedValue), Convert.ToInt32(tempOptions[1].SelectedValue)));
        }
    }
}
