using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using MuScreenManager;

namespace GameOfLifeMafia
{
    /// <summary>
    /// This is the main entry point got the Game of Life
    /// </summary>
    public class Game : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        List<string> savegamesNames;
        ScreenManager screenManager;

        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;

            this.IsMouseVisible = true;
        }
        /// <summary>
        /// returns a list of savegamenames and places dummy ones there if there are no save files
        /// </summary>
        /// <returns></returns>
        public void FillSaveGameNames()
        {
            for(int i = 0; i < 3; i++)
            {
                if (savegamesNames.Count() != 3)
                    savegamesNames.Add("Empty Save Slot");
            }         
        }
        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //inititalise savegamesnames list
            savegamesNames = new List<string>();

            //Initialize the music settings
            AudioSettings musicSettings = new AudioSettings(@"Content/Music", @"ExampleXACT");
            musicSettings.AddBank("Music", "Music");
            musicSettings.AddCategory("Music");

            //Create the ScreenManager
            screenManager = new ScreenManager(this, GraphicsDevice, "Content", Content.Load<SpriteFont>(@"HellaFont"), musicSettings);
            
            //Add the first mnu screen to the manager to get the ball rolling
            screenManager.AddScreen(new BackgroundScreen("MSM/title", true));

            //Directory Data
            string dir = @"C:\Users\Michael\Desktop\Game of Life 1.95\GameOfLifeMafia\GameOfLifeMafiaContent\";
            string save = "";
            string ext = ".xml";

            //checks if there are any previous save files
            for (int i = 1; i <= 3; i++)
            {
                save = "savegame" + i;
                string fullSaveName = dir + save + ext;
                if (File.Exists(fullSaveName))
                {
                    savegamesNames.Add(save);
                }
            }

            //Fills in any empty savegame slots with placeholder text
            FillSaveGameNames();
            MainMenu loadMenu = new MainMenu(savegamesNames);
            screenManager.AddScreen(loadMenu);
            //Get the ScreenManager to update and draw
            Components.Add(screenManager);

            //Initialize everything else
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            FillSaveGameNames();
            //Update MuScreenManager
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //Clears current graphics off screen
            GraphicsDevice.Clear(Color.CornflowerBlue);

            //Draws everything within ScreenManager
            base.Draw(gameTime);
        }
    }

#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (Game game = new Game())
            {
                game.Run();
            }
        }
    }
#endif
}