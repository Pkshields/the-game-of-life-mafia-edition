﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MuScreenManager;

namespace GameOfLifeMafia
{
    public class MainMenu : MenuScreen
    {
        /// <summary>
        /// Initialise an instance of a MainMenu using MenuScreen
        /// </summary>
        public MainMenu()
            : base("Game of Life!")
        { }

        /// <summary>
        /// Initialise the MenuItems components of the menu
        /// </summary>
        public override void Initialize()
        {
            //Initialize the MenuItems using the MenuItem from MSM
            MuScreenManager.MenuItem item1 = new MuScreenManager.MenuItem("Begin!", new Vector2(screenManager.ScreenDimensions.X / 2, 100), screenManager);
            MuScreenManager.MenuItem item2 = new MuScreenManager.MenuItem("Exit", new Vector2(screenManager.ScreenDimensions.X / 2, 150), screenManager);

            //Add consequences to hitting the buttons
            item1.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(item1_OnButtonSelect);
            item2.OnButtonSelect += new EventHandler<MenuObjectEventArgs>(item2_OnButtonSelect);

            //Add items to screen
            AddItem(item1);
            AddItem(item2);
        }

        /// <summary>
        /// Consequence for hitting the go button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item1_OnButtonSelect(object sender, MenuObjectEventArgs e)
        {
            screenManager.RemoveScreen(this);
            screenManager.AddScreen(new StartGameMenu());
        }

        /// <summary>
        /// Consequence for hitting the quit button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item2_OnButtonSelect(object sender, MenuObjectEventArgs e)
        {
            ConfirmMessageScreen confirmScreen = new ConfirmMessageScreen("Close the game?");
            confirmScreen.OnConfirm += new EventHandler<EventArgs>(conirmScreen_OnConfirm);
            screenManager.AddScreen(confirmScreen);
        }

        /// <summary>
        /// Consequence for hitting the Yes! Quit! button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void conirmScreen_OnConfirm(object sender, EventArgs e)
        {
            screenManager.Game.Exit();
        }
    }
}
